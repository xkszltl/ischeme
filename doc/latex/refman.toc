\contentsline {chapter}{\numberline {1}Class Index}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Class List}{1}{section.1.1}
\contentsline {chapter}{\numberline {2}Class Documentation}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Build\discretionary {-}{}{}In Class Reference}{3}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Detailed Description}{3}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Member Function Documentation}{3}{subsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.2.1}run}{3}{subsubsection.2.1.2.1}
\contentsline {subsubsection}{\numberline {2.1.2.2}set\discretionary {-}{}{}Env}{3}{subsubsection.2.1.2.2}
\contentsline {section}{\numberline {2.2}Complex Class Reference}{4}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Detailed Description}{5}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Constructor \& Destructor Documentation}{5}{subsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.2.1}Complex}{5}{subsubsection.2.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2.2}Complex}{5}{subsubsection.2.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.2.3}Complex}{5}{subsubsection.2.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.2.4}Complex}{5}{subsubsection.2.2.2.4}
\contentsline {subsubsection}{\numberline {2.2.2.5}Complex}{5}{subsubsection.2.2.2.5}
\contentsline {section}{\numberline {2.3}Empty Class Reference}{6}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Detailed Description}{6}{subsection.2.3.1}
\contentsline {section}{\numberline {2.4}Empty\discretionary {-}{}{}List Class Reference}{6}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Detailed Description}{6}{subsection.2.4.1}
\contentsline {section}{\numberline {2.5}Environment Class Reference}{6}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Detailed Description}{7}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}Constructor \& Destructor Documentation}{7}{subsection.2.5.2}
\contentsline {subsubsection}{\numberline {2.5.2.1}Environment}{7}{subsubsection.2.5.2.1}
\contentsline {subsubsection}{\numberline {2.5.2.2}Environment}{7}{subsubsection.2.5.2.2}
\contentsline {subsubsection}{\numberline {2.5.2.3}Environment}{7}{subsubsection.2.5.2.3}
\contentsline {subsection}{\numberline {2.5.3}Member Function Documentation}{7}{subsection.2.5.3}
\contentsline {subsubsection}{\numberline {2.5.3.1}add\discretionary {-}{}{}Var}{7}{subsubsection.2.5.3.1}
\contentsline {subsubsection}{\numberline {2.5.3.2}get\discretionary {-}{}{}Var}{7}{subsubsection.2.5.3.2}
\contentsline {subsubsection}{\numberline {2.5.3.3}operator+}{7}{subsubsection.2.5.3.3}
\contentsline {subsubsection}{\numberline {2.5.3.4}operator=}{8}{subsubsection.2.5.3.4}
\contentsline {subsubsection}{\numberline {2.5.3.5}set\discretionary {-}{}{}Var}{8}{subsubsection.2.5.3.5}
\contentsline {section}{\numberline {2.6}Exception Class Reference}{8}{section.2.6}
\contentsline {subsection}{\numberline {2.6.1}Detailed Description}{9}{subsection.2.6.1}
\contentsline {subsection}{\numberline {2.6.2}Constructor \& Destructor Documentation}{9}{subsection.2.6.2}
\contentsline {subsubsection}{\numberline {2.6.2.1}Exception}{9}{subsubsection.2.6.2.1}
\contentsline {subsubsection}{\numberline {2.6.2.2}Exception}{9}{subsubsection.2.6.2.2}
\contentsline {subsection}{\numberline {2.6.3}Member Function Documentation}{9}{subsection.2.6.3}
\contentsline {subsubsection}{\numberline {2.6.3.1}error\discretionary {-}{}{}Arg\discretionary {-}{}{}Num\discretionary {-}{}{}Lower}{9}{subsubsection.2.6.3.1}
\contentsline {subsubsection}{\numberline {2.6.3.2}error\discretionary {-}{}{}Arg\discretionary {-}{}{}Num\discretionary {-}{}{}Upper}{10}{subsubsection.2.6.3.2}
\contentsline {subsubsection}{\numberline {2.6.3.3}error\discretionary {-}{}{}Arg\discretionary {-}{}{}Type}{10}{subsubsection.2.6.3.3}
\contentsline {subsubsection}{\numberline {2.6.3.4}error\discretionary {-}{}{}Arg\discretionary {-}{}{}Value}{10}{subsubsection.2.6.3.4}
\contentsline {subsubsection}{\numberline {2.6.3.5}error\discretionary {-}{}{}Base}{10}{subsubsection.2.6.3.5}
\contentsline {subsubsection}{\numberline {2.6.3.6}error\discretionary {-}{}{}Call\discretionary {-}{}{}Without\discretionary {-}{}{}Proc}{10}{subsubsection.2.6.3.6}
\contentsline {subsubsection}{\numberline {2.6.3.7}error\discretionary {-}{}{}Convert}{10}{subsubsection.2.6.3.7}
\contentsline {subsubsection}{\numberline {2.6.3.8}error\discretionary {-}{}{}Divide\discretionary {-}{}{}By\discretionary {-}{}{}Zero}{10}{subsubsection.2.6.3.8}
\contentsline {subsubsection}{\numberline {2.6.3.9}error\discretionary {-}{}{}Index}{10}{subsubsection.2.6.3.9}
\contentsline {subsubsection}{\numberline {2.6.3.10}error\discretionary {-}{}{}Invaild\discretionary {-}{}{}Call}{10}{subsubsection.2.6.3.10}
\contentsline {subsubsection}{\numberline {2.6.3.11}error\discretionary {-}{}{}Operator}{11}{subsubsection.2.6.3.11}
\contentsline {subsubsection}{\numberline {2.6.3.12}error\discretionary {-}{}{}Operator}{11}{subsubsection.2.6.3.12}
\contentsline {subsubsection}{\numberline {2.6.3.13}error\discretionary {-}{}{}Set\discretionary {-}{}{}Unknown\discretionary {-}{}{}Id}{11}{subsubsection.2.6.3.13}
\contentsline {subsubsection}{\numberline {2.6.3.14}error\discretionary {-}{}{}Unknown\discretionary {-}{}{}Id}{11}{subsubsection.2.6.3.14}
\contentsline {section}{\numberline {2.7}Interactive Class Reference}{11}{section.2.7}
\contentsline {subsection}{\numberline {2.7.1}Detailed Description}{11}{subsection.2.7.1}
\contentsline {subsection}{\numberline {2.7.2}Member Function Documentation}{11}{subsection.2.7.2}
\contentsline {subsubsection}{\numberline {2.7.2.1}entry}{11}{subsubsection.2.7.2.1}
\contentsline {section}{\numberline {2.8}Interpreter Class Reference}{12}{section.2.8}
\contentsline {subsection}{\numberline {2.8.1}Detailed Description}{16}{subsection.2.8.1}
\contentsline {subsection}{\numberline {2.8.2}Constructor \& Destructor Documentation}{16}{subsection.2.8.2}
\contentsline {subsubsection}{\numberline {2.8.2.1}Interpreter}{16}{subsubsection.2.8.2.1}
\contentsline {subsection}{\numberline {2.8.3}Member Function Documentation}{16}{subsection.2.8.3}
\contentsline {subsubsection}{\numberline {2.8.3.1}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Abbreviation}{16}{subsubsection.2.8.3.1}
\contentsline {subsubsection}{\numberline {2.8.3.2}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Abbrev\discretionary {-}{}{}Prefix}{16}{subsubsection.2.8.3.2}
\contentsline {subsubsection}{\numberline {2.8.3.3}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Alternate}{16}{subsubsection.2.8.3.3}
\contentsline {subsubsection}{\numberline {2.8.3.4}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Assignment}{16}{subsubsection.2.8.3.4}
\contentsline {subsubsection}{\numberline {2.8.3.5}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Atmosphere}{17}{subsubsection.2.8.3.5}
\contentsline {subsubsection}{\numberline {2.8.3.6}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Binding\discretionary {-}{}{}Spec}{17}{subsubsection.2.8.3.6}
\contentsline {subsubsection}{\numberline {2.8.3.7}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Body}{17}{subsubsection.2.8.3.7}
\contentsline {subsubsection}{\numberline {2.8.3.8}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Boolean}{17}{subsubsection.2.8.3.8}
\contentsline {subsubsection}{\numberline {2.8.3.9}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Case\discretionary {-}{}{}Clause}{17}{subsubsection.2.8.3.9}
\contentsline {subsubsection}{\numberline {2.8.3.10}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Character}{17}{subsubsection.2.8.3.10}
\contentsline {subsubsection}{\numberline {2.8.3.11}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Character\discretionary {-}{}{}Name}{17}{subsubsection.2.8.3.11}
\contentsline {subsubsection}{\numberline {2.8.3.12}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Command}{17}{subsubsection.2.8.3.12}
\contentsline {subsubsection}{\numberline {2.8.3.13}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Command\discretionary {-}{}{}Or\discretionary {-}{}{}Definition}{18}{subsubsection.2.8.3.13}
\contentsline {subsubsection}{\numberline {2.8.3.14}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Comment}{18}{subsubsection.2.8.3.14}
\contentsline {subsubsection}{\numberline {2.8.3.15}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Complex\discretionary {-}{}{}R}{18}{subsubsection.2.8.3.15}
\contentsline {subsubsection}{\numberline {2.8.3.16}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Compound\discretionary {-}{}{}Datum}{18}{subsubsection.2.8.3.16}
\contentsline {subsubsection}{\numberline {2.8.3.17}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Cond\discretionary {-}{}{}Clause}{18}{subsubsection.2.8.3.17}
\contentsline {subsubsection}{\numberline {2.8.3.18}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Conditional}{18}{subsubsection.2.8.3.18}
\contentsline {subsubsection}{\numberline {2.8.3.19}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Consequent}{18}{subsubsection.2.8.3.19}
\contentsline {subsubsection}{\numberline {2.8.3.20}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Datum}{19}{subsubsection.2.8.3.20}
\contentsline {subsubsection}{\numberline {2.8.3.21}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Decimal10}{19}{subsubsection.2.8.3.21}
\contentsline {subsubsection}{\numberline {2.8.3.22}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Def\discretionary {-}{}{}Formals}{19}{subsubsection.2.8.3.22}
\contentsline {subsubsection}{\numberline {2.8.3.23}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Definition}{19}{subsubsection.2.8.3.23}
\contentsline {subsubsection}{\numberline {2.8.3.24}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Delimiter}{19}{subsubsection.2.8.3.24}
\contentsline {subsubsection}{\numberline {2.8.3.25}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Derived\discretionary {-}{}{}Expression}{19}{subsubsection.2.8.3.25}
\contentsline {subsubsection}{\numberline {2.8.3.26}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Digit\discretionary {-}{}{}R}{19}{subsubsection.2.8.3.26}
\contentsline {subsubsection}{\numberline {2.8.3.27}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Do\discretionary {-}{}{}Result}{19}{subsubsection.2.8.3.27}
\contentsline {subsubsection}{\numberline {2.8.3.28}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Exactness}{20}{subsubsection.2.8.3.28}
\contentsline {subsubsection}{\numberline {2.8.3.29}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Exponent\discretionary {-}{}{}Marker}{20}{subsubsection.2.8.3.29}
\contentsline {subsubsection}{\numberline {2.8.3.30}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Expression}{20}{subsubsection.2.8.3.30}
\contentsline {subsubsection}{\numberline {2.8.3.31}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Expression\discretionary {-}{}{}Keyword}{20}{subsubsection.2.8.3.31}
\contentsline {subsubsection}{\numberline {2.8.3.32}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Formals}{20}{subsubsection.2.8.3.32}
\contentsline {subsubsection}{\numberline {2.8.3.33}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Identifier}{20}{subsubsection.2.8.3.33}
\contentsline {subsubsection}{\numberline {2.8.3.34}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Init}{20}{subsubsection.2.8.3.34}
\contentsline {subsubsection}{\numberline {2.8.3.35}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Initial}{21}{subsubsection.2.8.3.35}
\contentsline {subsubsection}{\numberline {2.8.3.36}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Intertoken\discretionary {-}{}{}Space}{21}{subsubsection.2.8.3.36}
\contentsline {subsubsection}{\numberline {2.8.3.37}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Iteration\discretionary {-}{}{}Spec}{21}{subsubsection.2.8.3.37}
\contentsline {subsubsection}{\numberline {2.8.3.38}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Keyword}{21}{subsubsection.2.8.3.38}
\contentsline {subsubsection}{\numberline {2.8.3.39}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Lambda\discretionary {-}{}{}Expression}{21}{subsubsection.2.8.3.39}
\contentsline {subsubsection}{\numberline {2.8.3.40}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Letter}{21}{subsubsection.2.8.3.40}
\contentsline {subsubsection}{\numberline {2.8.3.41}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}List}{21}{subsubsection.2.8.3.41}
\contentsline {subsubsection}{\numberline {2.8.3.42}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Literal}{21}{subsubsection.2.8.3.42}
\contentsline {subsubsection}{\numberline {2.8.3.43}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Macro\discretionary {-}{}{}Block}{22}{subsubsection.2.8.3.43}
\contentsline {subsubsection}{\numberline {2.8.3.44}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Macro\discretionary {-}{}{}Use}{22}{subsubsection.2.8.3.44}
\contentsline {subsubsection}{\numberline {2.8.3.45}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Number}{22}{subsubsection.2.8.3.45}
\contentsline {subsubsection}{\numberline {2.8.3.46}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Num\discretionary {-}{}{}R}{22}{subsubsection.2.8.3.46}
\contentsline {subsubsection}{\numberline {2.8.3.47}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Operand}{22}{subsubsection.2.8.3.47}
\contentsline {subsubsection}{\numberline {2.8.3.48}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Operator}{22}{subsubsection.2.8.3.48}
\contentsline {subsubsection}{\numberline {2.8.3.49}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Peculiar\discretionary {-}{}{}Identifier}{22}{subsubsection.2.8.3.49}
\contentsline {subsubsection}{\numberline {2.8.3.50}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Prefix\discretionary {-}{}{}R}{23}{subsubsection.2.8.3.50}
\contentsline {subsubsection}{\numberline {2.8.3.51}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Procedure\discretionary {-}{}{}Call}{23}{subsubsection.2.8.3.51}
\contentsline {subsubsection}{\numberline {2.8.3.52}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Program}{23}{subsubsection.2.8.3.52}
\contentsline {subsubsection}{\numberline {2.8.3.53}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Program\discretionary {-}{}{}With\discretionary {-}{}{}Output}{23}{subsubsection.2.8.3.53}
\contentsline {subsubsection}{\numberline {2.8.3.54}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Quotation}{23}{subsubsection.2.8.3.54}
\contentsline {subsubsection}{\numberline {2.8.3.55}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Radix\discretionary {-}{}{}R}{23}{subsubsection.2.8.3.55}
\contentsline {subsubsection}{\numberline {2.8.3.56}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Real\discretionary {-}{}{}R}{23}{subsubsection.2.8.3.56}
\contentsline {subsubsection}{\numberline {2.8.3.57}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Recipient}{24}{subsubsection.2.8.3.57}
\contentsline {subsubsection}{\numberline {2.8.3.58}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Self\discretionary {-}{}{}Evaluating}{24}{subsubsection.2.8.3.58}
\contentsline {subsubsection}{\numberline {2.8.3.59}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Sequence}{24}{subsubsection.2.8.3.59}
\contentsline {subsubsection}{\numberline {2.8.3.60}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Sign}{24}{subsubsection.2.8.3.60}
\contentsline {subsubsection}{\numberline {2.8.3.61}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Simple\discretionary {-}{}{}Datum}{24}{subsubsection.2.8.3.61}
\contentsline {subsubsection}{\numberline {2.8.3.62}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Special\discretionary {-}{}{}Initial}{24}{subsubsection.2.8.3.62}
\contentsline {subsubsection}{\numberline {2.8.3.63}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Special\discretionary {-}{}{}Subsequent}{24}{subsubsection.2.8.3.63}
\contentsline {subsubsection}{\numberline {2.8.3.64}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Step}{24}{subsubsection.2.8.3.64}
\contentsline {subsubsection}{\numberline {2.8.3.65}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}String}{25}{subsubsection.2.8.3.65}
\contentsline {subsubsection}{\numberline {2.8.3.66}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}String\discretionary {-}{}{}Element}{25}{subsubsection.2.8.3.66}
\contentsline {subsubsection}{\numberline {2.8.3.67}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Subsequent}{25}{subsubsection.2.8.3.67}
\contentsline {subsubsection}{\numberline {2.8.3.68}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Suffix}{25}{subsubsection.2.8.3.68}
\contentsline {subsubsection}{\numberline {2.8.3.69}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Symbol}{25}{subsubsection.2.8.3.69}
\contentsline {subsubsection}{\numberline {2.8.3.70}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Syntantic\discretionary {-}{}{}Keyword}{25}{subsubsection.2.8.3.70}
\contentsline {subsubsection}{\numberline {2.8.3.71}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Syntax\discretionary {-}{}{}Definition}{25}{subsubsection.2.8.3.71}
\contentsline {subsubsection}{\numberline {2.8.3.72}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Syntax\discretionary {-}{}{}Spec}{25}{subsubsection.2.8.3.72}
\contentsline {subsubsection}{\numberline {2.8.3.73}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Test}{26}{subsubsection.2.8.3.73}
\contentsline {subsubsection}{\numberline {2.8.3.74}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Token}{26}{subsubsection.2.8.3.74}
\contentsline {subsubsection}{\numberline {2.8.3.75}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}U\discretionary {-}{}{}Integer\discretionary {-}{}{}R}{26}{subsubsection.2.8.3.75}
\contentsline {subsubsection}{\numberline {2.8.3.76}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}U\discretionary {-}{}{}Real\discretionary {-}{}{}R}{26}{subsubsection.2.8.3.76}
\contentsline {subsubsection}{\numberline {2.8.3.77}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Variable}{26}{subsubsection.2.8.3.77}
\contentsline {subsubsection}{\numberline {2.8.3.78}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Vector}{26}{subsubsection.2.8.3.78}
\contentsline {subsubsection}{\numberline {2.8.3.79}\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}Whitespace}{26}{subsubsection.2.8.3.79}
\contentsline {subsubsection}{\numberline {2.8.3.80}char\discretionary {-}{}{}O\discretionary {-}{}{}R}{26}{subsubsection.2.8.3.80}
\contentsline {subsubsection}{\numberline {2.8.3.81}entry}{27}{subsubsection.2.8.3.81}
\contentsline {subsubsection}{\numberline {2.8.3.82}run}{27}{subsubsection.2.8.3.82}
\contentsline {subsubsection}{\numberline {2.8.3.83}string\discretionary {-}{}{}O\discretionary {-}{}{}R}{27}{subsubsection.2.8.3.83}
\contentsline {section}{\numberline {2.9}Proc Class Reference}{27}{section.2.9}
\contentsline {subsection}{\numberline {2.9.1}Detailed Description}{28}{subsection.2.9.1}
\contentsline {subsection}{\numberline {2.9.2}Constructor \& Destructor Documentation}{28}{subsection.2.9.2}
\contentsline {subsubsection}{\numberline {2.9.2.1}Proc}{28}{subsubsection.2.9.2.1}
\contentsline {subsubsection}{\numberline {2.9.2.2}Proc}{28}{subsubsection.2.9.2.2}
\contentsline {subsubsection}{\numberline {2.9.2.3}Proc}{28}{subsubsection.2.9.2.3}
\contentsline {subsubsection}{\numberline {2.9.2.4}Proc}{28}{subsubsection.2.9.2.4}
\contentsline {section}{\numberline {2.10}ref\discretionary {-}{}{}Var Class Reference}{28}{section.2.10}
\contentsline {subsection}{\numberline {2.10.1}Detailed Description}{29}{subsection.2.10.1}
\contentsline {subsection}{\numberline {2.10.2}Constructor \& Destructor Documentation}{29}{subsection.2.10.2}
\contentsline {subsubsection}{\numberline {2.10.2.1}ref\discretionary {-}{}{}Var}{29}{subsubsection.2.10.2.1}
\contentsline {subsubsection}{\numberline {2.10.2.2}ref\discretionary {-}{}{}Var}{29}{subsubsection.2.10.2.2}
\contentsline {subsubsection}{\numberline {2.10.2.3}$\sim $ref\discretionary {-}{}{}Var}{29}{subsubsection.2.10.2.3}
\contentsline {subsection}{\numberline {2.10.3}Member Function Documentation}{29}{subsection.2.10.3}
\contentsline {subsubsection}{\numberline {2.10.3.1}operator=}{29}{subsubsection.2.10.3.1}
\contentsline {section}{\numberline {2.11}Rtl\discretionary {-}{}{}Complex Class Reference}{30}{section.2.11}
\contentsline {subsection}{\numberline {2.11.1}Detailed Description}{31}{subsection.2.11.1}
\contentsline {subsection}{\numberline {2.11.2}Constructor \& Destructor Documentation}{31}{subsection.2.11.2}
\contentsline {subsubsection}{\numberline {2.11.2.1}Rtl\discretionary {-}{}{}Complex}{31}{subsubsection.2.11.2.1}
\contentsline {subsubsection}{\numberline {2.11.2.2}Rtl\discretionary {-}{}{}Complex}{31}{subsubsection.2.11.2.2}
\contentsline {subsubsection}{\numberline {2.11.2.3}Rtl\discretionary {-}{}{}Complex}{31}{subsubsection.2.11.2.3}
\contentsline {section}{\numberline {2.12}Symbol Class Reference}{31}{section.2.12}
\contentsline {subsection}{\numberline {2.12.1}Detailed Description}{32}{subsection.2.12.1}
\contentsline {subsection}{\numberline {2.12.2}Constructor \& Destructor Documentation}{32}{subsection.2.12.2}
\contentsline {subsubsection}{\numberline {2.12.2.1}Symbol}{32}{subsubsection.2.12.2.1}
\contentsline {subsubsection}{\numberline {2.12.2.2}Symbol}{32}{subsubsection.2.12.2.2}
\contentsline {section}{\numberline {2.13}var Class Reference}{32}{section.2.13}
\contentsline {subsection}{\numberline {2.13.1}Detailed Description}{35}{subsection.2.13.1}
\contentsline {subsection}{\numberline {2.13.2}Constructor \& Destructor Documentation}{35}{subsection.2.13.2}
\contentsline {subsubsection}{\numberline {2.13.2.1}var}{35}{subsubsection.2.13.2.1}
\contentsline {subsubsection}{\numberline {2.13.2.2}var}{35}{subsubsection.2.13.2.2}
\contentsline {subsubsection}{\numberline {2.13.2.3}var}{35}{subsubsection.2.13.2.3}
\contentsline {subsubsection}{\numberline {2.13.2.4}var}{35}{subsubsection.2.13.2.4}
\contentsline {subsubsection}{\numberline {2.13.2.5}var}{35}{subsubsection.2.13.2.5}
\contentsline {subsubsection}{\numberline {2.13.2.6}var}{35}{subsubsection.2.13.2.6}
\contentsline {subsubsection}{\numberline {2.13.2.7}var}{36}{subsubsection.2.13.2.7}
\contentsline {subsubsection}{\numberline {2.13.2.8}var}{36}{subsubsection.2.13.2.8}
\contentsline {subsubsection}{\numberline {2.13.2.9}var}{36}{subsubsection.2.13.2.9}
\contentsline {subsubsection}{\numberline {2.13.2.10}var}{36}{subsubsection.2.13.2.10}
\contentsline {subsubsection}{\numberline {2.13.2.11}var}{36}{subsubsection.2.13.2.11}
\contentsline {subsubsection}{\numberline {2.13.2.12}var}{36}{subsubsection.2.13.2.12}
\contentsline {subsubsection}{\numberline {2.13.2.13}var}{36}{subsubsection.2.13.2.13}
\contentsline {subsubsection}{\numberline {2.13.2.14}var}{36}{subsubsection.2.13.2.14}
\contentsline {subsubsection}{\numberline {2.13.2.15}var}{36}{subsubsection.2.13.2.15}
\contentsline {subsubsection}{\numberline {2.13.2.16}$\sim $var}{37}{subsubsection.2.13.2.16}
\contentsline {subsection}{\numberline {2.13.3}Member Function Documentation}{37}{subsection.2.13.3}
\contentsline {subsubsection}{\numberline {2.13.3.1}add\discretionary {-}{}{}Arg}{37}{subsubsection.2.13.3.1}
\contentsline {subsubsection}{\numberline {2.13.3.2}append}{37}{subsubsection.2.13.3.2}
\contentsline {subsubsection}{\numberline {2.13.3.3}append\discretionary {-}{}{}List}{37}{subsubsection.2.13.3.3}
\contentsline {subsubsection}{\numberline {2.13.3.4}conjugate}{37}{subsubsection.2.13.3.4}
\contentsline {subsubsection}{\numberline {2.13.3.5}copy}{37}{subsubsection.2.13.3.5}
\contentsline {subsubsection}{\numberline {2.13.3.6}float\discretionary {-}{}{}Range}{37}{subsubsection.2.13.3.6}
\contentsline {subsubsection}{\numberline {2.13.3.7}get\discretionary {-}{}{}Imag}{37}{subsubsection.2.13.3.7}
\contentsline {subsubsection}{\numberline {2.13.3.8}get\discretionary {-}{}{}Real}{37}{subsubsection.2.13.3.8}
\contentsline {subsubsection}{\numberline {2.13.3.9}operator bool}{38}{subsubsection.2.13.3.9}
\contentsline {subsubsection}{\numberline {2.13.3.10}operator char}{38}{subsubsection.2.13.3.10}
\contentsline {subsubsection}{\numberline {2.13.3.11}operator Complex}{38}{subsubsection.2.13.3.11}
\contentsline {subsubsection}{\numberline {2.13.3.12}operator Empty}{38}{subsubsection.2.13.3.12}
\contentsline {subsubsection}{\numberline {2.13.3.13}operator Empty\discretionary {-}{}{}List}{38}{subsubsection.2.13.3.13}
\contentsline {subsubsection}{\numberline {2.13.3.14}operator mpf\discretionary {-}{}{}\_\discretionary {-}{}{}class}{38}{subsubsection.2.13.3.14}
\contentsline {subsubsection}{\numberline {2.13.3.15}operator mpq\discretionary {-}{}{}\_\discretionary {-}{}{}class}{38}{subsubsection.2.13.3.15}
\contentsline {subsubsection}{\numberline {2.13.3.16}operator Pair}{38}{subsubsection.2.13.3.16}
\contentsline {subsubsection}{\numberline {2.13.3.17}operator Proc}{38}{subsubsection.2.13.3.17}
\contentsline {subsubsection}{\numberline {2.13.3.18}operator Rtl\discretionary {-}{}{}Complex}{39}{subsubsection.2.13.3.18}
\contentsline {subsubsection}{\numberline {2.13.3.19}operator string}{39}{subsubsection.2.13.3.19}
\contentsline {subsubsection}{\numberline {2.13.3.20}operator Symbol}{39}{subsubsection.2.13.3.20}
\contentsline {subsubsection}{\numberline {2.13.3.21}operator Vector}{39}{subsubsection.2.13.3.21}
\contentsline {subsubsection}{\numberline {2.13.3.22}operator!=}{39}{subsubsection.2.13.3.22}
\contentsline {subsubsection}{\numberline {2.13.3.23}operator\%}{39}{subsubsection.2.13.3.23}
\contentsline {subsubsection}{\numberline {2.13.3.24}operator$<$}{39}{subsubsection.2.13.3.24}
\contentsline {subsubsection}{\numberline {2.13.3.25}operator$<$=}{39}{subsubsection.2.13.3.25}
\contentsline {subsubsection}{\numberline {2.13.3.26}operator=}{39}{subsubsection.2.13.3.26}
\contentsline {subsubsection}{\numberline {2.13.3.27}operator==}{40}{subsubsection.2.13.3.27}
\contentsline {subsubsection}{\numberline {2.13.3.28}operator$>$}{40}{subsubsection.2.13.3.28}
\contentsline {subsubsection}{\numberline {2.13.3.29}operator$>$=}{40}{subsubsection.2.13.3.29}
\contentsline {subsubsection}{\numberline {2.13.3.30}set\discretionary {-}{}{}Code}{40}{subsubsection.2.13.3.30}
\contentsline {subsubsection}{\numberline {2.13.3.31}set\discretionary {-}{}{}First}{40}{subsubsection.2.13.3.31}
\contentsline {subsubsection}{\numberline {2.13.3.32}set\discretionary {-}{}{}List\discretionary {-}{}{}Arg}{40}{subsubsection.2.13.3.32}
\contentsline {subsubsection}{\numberline {2.13.3.33}set\discretionary {-}{}{}Second}{40}{subsubsection.2.13.3.33}
\contentsline {subsubsection}{\numberline {2.13.3.34}vec\discretionary {-}{}{}Clear}{40}{subsubsection.2.13.3.34}
\contentsline {subsubsection}{\numberline {2.13.3.35}vec\discretionary {-}{}{}Get}{40}{subsubsection.2.13.3.35}
\contentsline {subsubsection}{\numberline {2.13.3.36}vec\discretionary {-}{}{}Push\discretionary {-}{}{}Back}{41}{subsubsection.2.13.3.36}
\contentsline {part}{Index}{42}{section*.26}
