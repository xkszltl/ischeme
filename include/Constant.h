#pragma once
#ifndef H_STATUSCONSTANT
#define H_STATUSCONSTANT

#define STATUS_NORMAL                       0

#define NORMAL_REPRESENT_STYLE              0
#define DISPLAY_REPRESENT_STYLE             1

#define DEFAULT_LIB                         "stdlib"

#define DEFAULT_FLOAT_PRECISION             1024

#endif

