#pragma once
#ifndef H_BUILDIN
#define H_BUILDIN

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <ctime>
#include <cassert>
#include <cctype>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <string>
#include <vector>
#include <bitset>
#include <set>
#include <map>
#include <queue>
#include <list>
#include <limits>

#include <gmp.h>
#include <gmpxx.h>
#include <mpfr.h>
#include <mpc.h>

#include "Constant.h"
#include "scmType.h"
#include "Environment.h"
#include "Interpreter.h"

#define addBuildIn(funcName)   \
    buildinEnv.addVar((funcName)); \
    buildinEnv.setVar((funcName), nowEval = var(Proc(1, (funcName), Environment())))

#define exceptionArgNumLower(lower, funcName) \
    if (arg.size() < lower)    \
        throw Exception::errorArgNumLower(arg.size(), funcName)

#define exceptionArgNum(lower, upper, funcName) \
    if (arg.size() < lower)    \
        throw Exception::errorArgNumLower(arg.size(), funcName);    \
    else if (arg.size() > upper)    \
        throw Exception::errorArgNumUpper(arg.size(), funcName)

using namespace std;

class var;
class refVar;
class Environment;

/// Class about build-in procedures.
class BuildIn
{
public:
    static Environment buildinEnv;      ///< Build-in environment.

public:
    static void setEnv();

    static int run(string, vector<var>, Environment &, var &);
};

#endif

