#pragma once
#ifndef H_INTERPRETER
#define H_INTERPRETER

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <ctime>
#include <cassert>
#include <cctype>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <string>
#include <vector>
#include <bitset>
#include <set>
#include <map>
#include <queue>
#include <list>

#include <gmp.h>
#include <gmpxx.h>
#include <mpfr.h>
#include <mpc.h>

#include "Constant.h"
#include "Exception.h"
#include "scmType.h"
#include "Environment.h"
#include "BuildIn.h"

using namespace std;

class var;
class refVar;
class Environment;
class BuildIn;

/// Class of parser.
class Interpreter
{
public:
    Interpreter();
    ~Interpreter();

    static int entry(istringstream &, Environment &);

public:
    static int run(var, Environment &, var &);

public:
    static const vector<string> digitSet2;          ///< Digit set of base 2.
    static const vector<string> digitSet8;          ///< Digit set of base 8.
    static const vector<string> digitSet10;         ///< Digit set of base 10.
    static const vector<string> digitSet16;         ///< Digit set of base 16.

public:
    static string lowercase(const string &);
    static int charOR(string &, string &, int (*)(int));
    static int stringOR(string &, string &, const vector<string> &);

    ///Lexical Structure
    static int _Token(string &, string &, var &);
    static int _Delimiter(string &, string &);
    static int _Whitespace(string &);
    static int _Comment(string &);
    static int _Atmosphere(string &);
    static int _IntertokenSpace(string &);
    static int _Identifier(string &, string &);
    static int _Initial(string &, string &);
    static int _Letter(string &, string &);
    static int _SpecialInitial(string &, string &);
    static int _Subsequent(string &, string &);
    static int _Digit(string &, string &, var &);
    static int _SpecialSubsequent(string &, string &);
    static int _PeculiarIdentifier(string &, string &);
    static int _SyntanticKeyword(string &, string &);
    static int _ExpressionKeyword(string &, string &);
    static int _Variable(string &, string &);
    static int _Boolean(string &, string &, var &);
    static int _Character(string &, string &, var &);
    static int _CharacterName(string &, string &);
    static int _String(string &, string &, var &);
    static int _StringElement(string &, string &);
    static int _Number(string &, string &, var &);
    static int _NumR(const int &, string &, string &, var &);
    static int _ComplexR(const int &, string &, string &, var &, const int &);
    static int _RealR(const int &, string &, string &, var &, const int &);
    static int _URealR(const int &, string &, string &, var &, const int &);
    static int _Decimal10(string &, string &, var &, const int &);
    static int _UIntegerR(const int &, string &, string &, var &);
    static int _PrefixR(const int &, string &, string &);
    static int _Suffix(string &, string &);
    static int _ExponentMarker(string &, string &);
    static int _Sign(string &, string &);
    static int _Exactness(string &, string &);
    static int _RadixR(int, string &, string &);
    static int _DigitR(int, string &, string &, var &);

    //External representations
    static int _Datum(string &, string &, var &, Environment &);
    static int _SimpleDatum(string &, string &, var &, Environment &);
    static int _Symbol(string &, string &, var &);
    static int _CompoundDatum(string &, string &, var &, Environment &);
    static int _List(string &, string &, var &, Environment &);
    static int _Abbreviation(string &, string &, var &, Environment &);
    static int _AbbrevPrefix(string &, string &);
    static int _Vector(string &, string &, var &, Environment &);

    //Expression
    static int _Expression(string &, string &, var &, Environment &, const int &);
    static int _Literal(string &, string &, var &, Environment &, const int &);
    static int _SelfEvaluating(string &, string &, var &);
    static int _Quotation(string &, string &, var &, Environment &, const int &);
    static int _ProcedureCall(string &, string &, var &, Environment &, const int &);
    static int _Operator(string &, string &, var &, Environment &, const int &);
    static int _Operand(string &, string &, var &, Environment &, const int &);
    static int _LambdaExpression(string &, string &, var &, Environment &, const int &);
    static int _Formals(string &, string &, var &, Environment &);
    static int _Body(string &, string &, var &, Environment &, const int &);
    static int _Sequence(string &, string &, var &, Environment &, const int &);
    static int _Command(string &, string &, var &, Environment &, const int &);
    static int _Conditional(string &, string &, var &, Environment &, const int &);
    static int _Test(string &, string &, var &, Environment &, const int &);
    static int _Consequent(string &, string &, var &, Environment &, const int &);
    static int _Alternate(string &, string &, var &, Environment &, const int &);
    static int _Assignment(string &, string &, var &, Environment &, const int &);
    static int _DerivedExpression(string &, string &, var &, Environment &, const int &);
    static int _CondClause(string &, string &, var &, Environment &, const int &);
    static int _Recipient(string &, string &, var &, Environment &, const int &);
    static int _CaseClause(string &, string &, var &, Environment &, var &, const int &);
    static int _BindingSpec(string &, string &, var &, Environment &, const int &);
    static int _IterationSpec(string &, string &, Environment &, string &, const int &);
    static int _Init(string &, string &, var &, Environment &, const int &);
    static int _Step(string &, string &, var &, Environment &, const int &);
    static int _DoResult(string &, string &, var &, Environment &, const int &);
    static int _MacroUse(string &, string &);
    static int _Keyword(string &, string &);
    static int _MacroBlock(string &, string &);
    static int _SyntaxSpec(string &, string &);

    //Quasiquotations

    //Transformers
    static int _TransformerSpec(string &, string &);
    static int _SyntaxRule(string &, string &);
    static int _Pattern(string &, string &);
    static int _PatternDatum(string &, string &);
    static int _Template(string &, string &);
    static int _TemplateElement(string &, string &);
    static int _TemplateDatum(string &, string &);
    static int _PatternIdentifier(string &, string &);
    static int _Ellipsis(string &, string &);

    //Programs and definitions
    static int _ProgramWithOutput(string &, string &, var &, Environment &, const int &);
    static int _Program(string &, string &, var &, Environment &, const int &);
    static int _CommandOrDefinition(string &, string &, var &, Environment &, const int &);
    static int _Definition(string &, string &, Environment &, const int &);
    static int _DefFormals(string &, string &, var &, Environment &, const int &);
    static int _SyntaxDefinition(string &, string &, var &, Environment &, const int &);
};

#endif

