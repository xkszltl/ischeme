#pragma once
#ifndef H_ENVIRONMENT
#define H_ENVIRONMENT

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <ctime>
#include <cassert>
#include <cctype>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <string>
#include <vector>
#include <bitset>
#include <set>
#include <map>
#include <queue>
#include <list>
#include <limits>

#include "Constant.h"
#include "scmType.h"
#include "Exception.h"
#include "BuildIn.h"

using namespace std;

class var;
class refVar;
class BuildIn;

/// Class of run-time environment.
class Environment
{
public:
    map<string, refVar> varList;        ///< Mapping from variable's name to the reference of weak type object.

public:
    Environment();
    Environment(const map<string, refVar> &_varList);
    Environment(const Environment &);

    Environment operator =(const Environment &);

    Environment operator +(const Environment &);

    int addVar(const string &);
    var getVar(const string &);
    int setVar(const string &, const var &);
};

#endif

