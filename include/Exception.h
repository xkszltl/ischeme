#pragma once
#ifndef H_EXCEPTION
#define H_EXCEPTION

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <ctime>
#include <cassert>
#include <cctype>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <string>
#include <vector>
#include <bitset>
#include <set>
#include <map>
#include <queue>
#include <list>
#include <limits>

using namespace std;

/// Class of exception.
class Exception
{
public:
    string msg;     ///< Message of exception.

public:
    static string internalError;        ///< Message of internal errors.

public:
    Exception();
    Exception(const string &);
    Exception(const Exception &);

public:
    static Exception errorArgType(const string &, const string &, const int &);
    static Exception errorArgValue(const string &, const string &, const int &);
    static Exception errorArgNumLower(const int &, const string &);
    static Exception errorArgNumUpper(const int &, const string &);
    static Exception errorIndex(const int &);
    static Exception errorUnknownId(const string &);
    static Exception errorInvaildCall(const string &);
    static Exception errorCallWithoutProc(const string &, const string &);
    static Exception errorBase(const int &);
    static Exception errorDivideByZero(const string &, const string &);

    static Exception errorConvert(const string &, const string &);
    static Exception errorOperator(const string &, const string &);
    static Exception errorOperator(const string &, const string &, const string &);
    static Exception errorSetUnknownId(const string &, const string &);
};

#endif

