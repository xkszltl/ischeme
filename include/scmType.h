#pragma once
#ifndef H_SCMTYPE
#define H_SCMTYPE

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <ctime>
#include <cassert>
#include <cctype>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <string>
#include <vector>
#include <bitset>
#include <set>
#include <map>
#include <queue>
#include <list>
#include <typeinfo>
#include <limits>

#include <gmp.h>
#include <gmpxx.h>
#include <mpfr.h>
#include <mpc.h>

#include "Constant.h"
#include "Exception.h"
#include "Environment.h"

#define typeMatch(A, B, C, op) \
    if (*tid == typeid(A).name() && *(b.tid) == typeid(B).name()) \
        return var((C)(((C)(operator A())) op ((C)(b.operator B()))), (*opt) | (*(b.opt)))

#define typeDelete(A)   \
    if (*tid == typeid(A).name())    \
        delete (A*)val

#define typeCopy(A) \
    if (*tid == typeid(A).name())   \
        return var(*(A *)val, *opt)

using namespace std;

class Environment;

class var;
class refVar;
class Empty;
class EmptyList;
class RtlComplex;
class Complex;

/// Class of pair.
typedef pair<var, var> Pair;
/// Class of vector.
typedef vector<var> Vector;

///Class of empty object.
class Empty
{
};

///Class of empty list.
class EmptyList
{
};

///Class of complex with rational members.
class RtlComplex
{
public:
    mpq_class re;   ///< Real part.
    mpq_class im;   ///< Imaginary part.

    RtlComplex();
    RtlComplex(const mpq_class &);
    RtlComplex(const mpq_class &, const mpq_class &);
    RtlComplex(const RtlComplex &);

    RtlComplex operator =(const RtlComplex &);

    RtlComplex operator +(const RtlComplex &) const;
    RtlComplex operator -(const RtlComplex &) const;
    RtlComplex operator *(const RtlComplex &) const;
    RtlComplex operator /(const RtlComplex &) const;

    RtlComplex conjugate() const;

    bool operator ==(const mpq_class &) const;
    bool operator ==(const mpf_class &) const;
    bool operator ==(const RtlComplex &) const;
    bool operator ==(const Complex &) const;
    bool operator !=(const mpq_class &) const;
    bool operator !=(const mpf_class &) const;
    bool operator !=(const RtlComplex &) const;
    bool operator !=(const Complex &) const;
    
    mpq_class norm() const;
    mpf_class abs() const;

    string getStr() const;
};

/// Class of complex with float members.
class Complex
{
public:
    mpf_class re;   ///< Real part.
    mpf_class im;   ///< Imaginary part.

    Complex();
    Complex(const mpq_class &);
    Complex(const mpf_class &);
    Complex(const mpf_class &, const mpf_class &);
    Complex(const RtlComplex &);
    Complex(const Complex &);

    Complex operator =(const Complex &);

    Complex operator +(const Complex &) const;
    Complex operator -(const Complex &) const;
    Complex operator *(const Complex &) const;
    Complex operator /(const Complex &) const;

    Complex conjugate() const;

    bool operator ==(const mpq_class &) const;
    bool operator ==(const mpf_class &) const;
    bool operator ==(const RtlComplex &) const;
    bool operator ==(const Complex &) const;
    bool operator !=(const mpq_class &) const;
    bool operator !=(const mpf_class &) const;
    bool operator !=(const RtlComplex &) const;
    bool operator !=(const Complex &) const;
    
    mpf_class norm() const;
    mpf_class abs() const;

    string getStr() const;
};

/// Class of symbol.
class Symbol
{
public:
    string code;    ///< Code of symbol.

public:
    Symbol();
    Symbol(const string &);
    Symbol(const Symbol &);

    Symbol operator =(const Symbol &);
};

/// Class of procedure.
class Proc
{
public:
    int buildin;            ///< Tag of build-in procedure.
    vector<string> arg;     ///< Arguments list.
    string listArg;         ///< List-argument for unlimited number of argumnents.
    string code;            ///< Code of procedure.
    Environment env;        ///< Environment of procedure.

public:
    Proc();
    Proc(const Environment &);
    Proc(const string &, const Environment &);
    Proc(const int &, const string &, const Environment &);
    Proc(const Proc &);

    Proc operator =(const Proc &);

public:
    void addArg(const string &);
    void setListArg(const string &);
};

/// Class of weak type object.
class var
{
public:
    void *val;          ///< Pointer of strong type value.
    int *opt;           ///< Pointer of tag.
    string *tid;        ///< Pointer of the type name from typeid().
    int *ref;           ///< Pointer of reference counter.

public:
    var();

    var(const Empty &, const int & = 0);
    var(const EmptyList &, const int & = 0);
    var(const bool &, const int & = 0);
    var(const mpq_class &, const int & = 0);
    var(const mpf_class &, const int & = 0);
    var(const RtlComplex &, const int & = 0);
    var(const Complex &, const int & = 0);
    var(const char &, const int & = 0);
    var(const string &, const int & = 0);
    var(const Pair &, const int & = 0);
    var(const Vector &, const int & = 0);
    var(const Symbol &, const int & = 0);
    var(const Proc &, const int & = 0);

    var(const var &);

    var & operator =(const var &);

    ~var();

    var copy() const;

public:
    operator Empty() const;
    operator EmptyList() const;
    operator bool() const;
    operator mpq_class() const;
    operator mpf_class() const;
    operator RtlComplex() const;
    operator Complex() const;
    operator char() const;
    operator string() const;
    operator Pair() const;
    operator Vector() const;
    operator Symbol() const;
    operator Proc() const;

    string getStr(const int & = NORMAL_REPRESENT_STYLE) const;
    string typeName() const;

public:
    var operator +(const var &);
    var operator -(const var &);
    var operator *(const var &);
    var operator /(const var &);
    var operator %(const var &);

    var conjugate() const;

    bool operator <(const var &) const;
    bool operator >(const var &) const;
    bool operator <=(const var &) const;
    bool operator >=(const var &) const;
    bool operator ==(const var &) const;
    bool operator !=(const var &) const;

    var getReal() const;
    var getImag() const;

    void addArg(const string &);
    void setListArg(const string &);
    void setCode(const string &);

    void vecClear();
    var vecGet(const int &);
    void vecPushBack(const var &);

    void setFirst(const var &);
    void setSecond(const var &);

    void append(const var &);
    void appendList(const var &);

    bool floatRange() const;
};

var sin(const var &);
var cos(const var &);
var tan(const var &);
var asin(const var &);
var acos(const var &);
var atan(const var &);
var atan2(const var &, const var &);
var floor(const var &);
var ceil(const var &);
var trunc(const var &);
var round(const var &);
var sqrt(const var &);
var exp(const var &);
var log(const var &);
var pow(const var &, const var &);
var abs(const var &);

/// Class of the reference of weak type object.
class refVar
{
public:
    var *ptr;       ///< Pointer of the referenced object

public:
    refVar();
    refVar(const var &);
    refVar(const refVar &);

    ~refVar();

    refVar operator =(const var &);
    refVar operator =(const refVar &);
};

#endif

