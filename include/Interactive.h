#pragma once
#ifndef H_INTERACTIVE
#define H_INTERACTIVE

#include "Constant.h"
#include "scmType.h"
#include "Environment.h"
#include "Exception.h"
#include "Interpreter.h"

/// Class of interactive interface.
class Interactive
{
public:
    Interactive();
    ~Interactive();

    int entry(Environment &);
};

#endif

