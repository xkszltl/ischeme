#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <ctime>
#include <cassert>
#include <cctype>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <string>
#include <vector>
#include <bitset>
#include <set>
#include <map>
#include <queue>
#include <list>
#include <limits>

#include "Constant.h"
#include "scmType.h"
#include "Exception.h"
#include "BuildIn.h"
#include "Environment.h"

using namespace std;

///@brief Default constructor.

/// Construct to an empty environment.
Environment::Environment()
{
    varList.clear();
}

///@brief Constructor.

/// Construct from a map mapping variable's name to refVar.
Environment::Environment(const map<string, refVar> &_varList):varList(_varList) {}

///@brief Copy constructor.

///The variables in the new environment will be related to the old one.
Environment::Environment(const Environment &b):varList(b.varList) {}

///@brief Assignment.

/// The same as the copy constructor.
Environment Environment::operator =(const Environment &b)
{
    varList.clear();
    varList = b.varList;
    return *this;
}

///@brief Combination

/// Combine two environment to a new environment.
/// If there are some variable with the same name in these two environments, the one from the latter will be kept.
/// All variables in the new environment will be related to its origin.
Environment Environment::operator +(const Environment &b)
{
    Environment res(varList);
    for (auto iter = b.varList.begin(); iter != b.varList.end(); ++iter)
        res.varList[iter -> first] = iter -> second;
    return res;
}

///@brief Declare a new variable with the specific name in the environment.

/// This function is only for declaration. The new variable will be set to an empty object.
/// If the variable is exist, it will be replace will a new one which stored in a new location.
int Environment::addVar(const string &varName)
{
    varList[varName] = refVar();
    return 1;
}

///@brief Get a variable with a specific name from the environment.

/// It will first trying to fetch the variable from build-in environment before searching in the current environment.
/// When trying to get a non-exist variable, an exception will be throw out.
var Environment::getVar(const string &varName)
{
    if (varList.find(varName) != varList.end())
        return *(varList[varName].ptr);

    if (BuildIn::buildinEnv.varList.find(varName) != BuildIn::buildinEnv.varList.end())
        return *(BuildIn::buildinEnv.varList[varName].ptr);

    throw Exception::errorUnknownId(varName);
}

///@brief Set the value of the specific variable in the environment.

/// This function is only for modification.
/// For a new variable, please declare it first.
/// When trying to set the value of a non-exist variable, an exception will be throw out.
int Environment::setVar(const string &varName, const var &eval)
{
    if (varList.find(varName) == varList.end())
        throw Exception::errorSetUnknownId(varName, "while setting variable's value in a environment");

    varList[varName] = eval;
    return 1;
}

