#include <cstdio>
#include <cstring>
#include <cmath>
#include <ctime>
#include <cassert>
#include <cctype>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <string>
#include <vector>
#include <bitset>
#include <set>
#include <map>
#include <queue>
#include <list>
#include <limits>

#include <gmp.h>
#include <gmpxx.h>
#include <mpfr.h>
#include <mpc.h>

#include "Constant.h"
#include "Exception.h"
#include "scmType.h"
#include "Environment.h"
#include "BuildIn.h"
#include "Interpreter.h"

using namespace std;

///@brief Default constructor.

/// Initialize a new parser.
Interpreter::Interpreter()
{
    mpf_set_default_prec(DEFAULT_FLOAT_PRECISION);
}

///@brief Destructor.
Interpreter::~Interpreter()
{
}

///@brief Entry of the parser.

/// A stream of code and a environment should be given.
/// Environment will be modified by the program in the code stream.
int Interpreter::entry(istringstream &codeStream, Environment &env)
{
    string code = codeStream.str();

    try
    {
        string res;
        var eval;
        _ProgramWithOutput(code, res, eval, env, 1);
        _IntertokenSpace(code);
    }
    catch (Exception e)
    {
        cout << e.msg << endl;
    }

    if (code != "")
    {
        cout << endl;
        cout << endl;
        cout << "Interpret Error::" << endl;
        cout << string(64, '=') << endl;
        cout << endl;
        cout << code << endl;
        cout << endl;
        cout << string(64, '=') << endl;
    }
    cout << endl;
    return STATUS_NORMAL;
}

///@brief Evaluate a class var in a specific enviroment.

/// The evaluation will be stored in eval.
int Interpreter::run(var t, Environment &env, var &eval)
{
    string now;
    Proc func;
    var nowEval, arg;
    vector<var> opt;
    Environment nowEnv;

    if (*(t.tid) != typeid(Pair).name() && *(t.tid) != typeid(Symbol).name())
    {
        eval = t;
        return 1;
    }

    if (*(t.tid) == typeid(Symbol).name())
    {
        eval = env.getVar(t.operator Symbol().code);
        return 1;
    }

    nowEnv = env;
    if (*(t.tid) == typeid(Pair).name())
    {
        nowEval = t.operator Pair().first;

        if (*(nowEval.tid) != typeid(Proc).name())
            throw Exception::errorCallWithoutProc(nowEval.typeName(), nowEval.getStr());

        func = nowEval.operator Proc();

        arg = t.operator Pair().second;
        opt.clear();
        for (; *(arg.tid) == typeid(Pair).name(); arg = arg.operator Pair().second)
        {
            nowEval = arg.operator Pair().first;
            opt.push_back(nowEval.copy());
        }

        if (*(arg.tid) != typeid(EmptyList).name())
            throw Exception::errorInvaildCall("Not a proper list.");

        if (func.buildin)
            return BuildIn::run(func.code, opt, env, eval);
        else
        {
            if (opt.size() < func.arg.size())
                throw Exception::errorArgNumLower(opt.size(), func.code);
            if (func.listArg == "" && opt.size() > func.arg.size())
                throw Exception::errorArgNumUpper(opt.size(), func.code);

            nowEnv = nowEnv+func.env;
            nowEval = var(EmptyList());
            for (; opt.size() > func.arg.size(); opt.pop_back())
                nowEval = var(Pair(opt.back(), nowEval));
            if (func.listArg != "")
            {
                nowEnv.addVar(func.listArg);
                nowEnv.setVar(func.listArg, nowEval);
            }
            for (int i = 0; i < opt.size(); ++i)
            {
                nowEnv.addVar(func.arg[i]);
                nowEnv.setVar(func.arg[i], opt[i]);
            }
            return _Program(func.code, now, eval, nowEnv, 1);
        }
    }
}

const vector<string> Interpreter::digitSet2 = {"0", "1"};
const vector<string> Interpreter::digitSet8 = {"0", "1", "2", "3", "4", "5", "6", "7"};
const vector<string> Interpreter::digitSet10 = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
const vector<string> Interpreter::digitSet16 = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};

///@brief Convert all uppercase letter in the string to lowercase.
string Interpreter::lowercase(const string &s)
{
    string res = "";
    for (int i = 0; s[i]; res += tolower(s[i++]));
    return res;
}

///@brief Interpreter function.

/// Check if the first letter in the string by a given function.
/// If success, then read it out.
int Interpreter::charOR(string &s, string &res, int (*f)(int))
{
    if (f(s[0]))
    {
        res = lowercase(s.substr(0, 1));
        s.erase(0, 1);
        return 1;
    }

    return 0;
}

///@brief Interpreter function.

/// Check if the there is a prefix matching the string is the given vector.
/// Verifying in the order from the beginning to the end of the vector.
/// If success, then read it out.
int Interpreter::stringOR(string &s, string &res, const vector<string> &opt)
{
    for (auto iter = opt.begin(); iter != opt.end(); ++iter)
        if (lowercase(s.substr(0, iter -> size())) == *iter)
        {
            res = *iter;
            s.erase(0, iter -> size());
            return 1;
        }

    return 0;
}

//Lexical Structure

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_Token(string &s, string &res, var &eval)
{
    if (_Identifier(s, res))
        return 1;
    if (_Boolean(s, res, eval))
        return 1;
    if (_Number(s, res, eval))
        return 1;
    if (_Character(s, res, eval))
        return 1;
    if (_String(s, res, eval))
        return 1;
    if (stringOR(s, res, {"(", ")", "#(", "\'", "`", ",@", ",", "."}))
        return 1;

    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_Delimiter(string &s, string &res)
{
    if (_Whitespace(s))
    {
        res = " ";
        return 1;
    }

    if (stringOR(s, res, {"(", ")", "\"", ";"}))
        return 1;

    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_Whitespace(string &s)
{
    string now;

    if (charOR(s, now, isspace))
        return 1;

    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_Comment(string &s)
{
    if (s[0] == ';')
    {
        s = s.substr(s.find("\n"));
        return 1;
    }

    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_Atmosphere(string &s)
{
    return _Whitespace(s) || _Comment(s);
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_IntertokenSpace(string &s)
{
    while (_Atmosphere(s));

    return 1;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_Identifier(string &s, string &res)
{
    string now;

    if (_Initial(s, res))
    {
        for (; _Subsequent(s, now); res += now);
        return 1;
    }

    if (_PeculiarIdentifier(s, res))
        return 1;

    res = "";
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_Initial(string &s, string &res)
{
    if (_Letter(s, res))
        return 1;

    if (_SpecialInitial(s, res))
        return 1;

    res = "";
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_Letter(string &s, string &res)
{
    if (charOR(s, res, isalpha))
        return 1;

    res = "";
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_SpecialInitial(string &s, string &res)
{
    if (stringOR(s, res, {"!", "$", "%", "&", "*", "/", ":", "<", "=", ">", "?", "^", "_", "~"}))
        return 1;

    res = "";
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_Subsequent(string &s, string &res)
{
    var nowEval;

    if (_Initial(s, res))
        return 1;

    if (_DigitR(10, s, res, nowEval))
        return 1;

    if (_SpecialSubsequent(s, res))
        return 1;

    res = "";
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_SpecialSubsequent(string &s, string &res)
{
    if (stringOR(s, res, {"+", "-", ".", "@"}))
        return 1;

    res = "";
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_PeculiarIdentifier(string &s, string &res)
{
    if (stringOR(s, res, {"+", "-", "..."}))
        return 1;

    res = "";
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_SyntanticKeyword(string &s, string &res)
{
    if (_ExpressionKeyword(s, res))
        return 1;

    if (stringOR(s, res, {"else", "=>", "define", "unquote-splicing", "unquote"}))
        return 1;

    res = "";
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_ExpressionKeyword(string &s, string &res)
{
    if (stringOR(s, res, {"quote", "lambda", "if", "set!", "begin", "cond", "and", "or", "case", "let*", "letrec", "let", "do", "delay", "quasiquote"}))
        return 1;

    res = "";
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_Variable(string &s, string &res)
{
    string now, buffer, code;

    buffer = s;
    if (_Identifier(buffer, res))
    {
        code = res;
        if (! _SyntanticKeyword(code, now))
        {
            s = buffer;
            return 1;
        }
        else if (code != "")
        {
            s = buffer;
            return 1;
        }
    }

    res = "";
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_Boolean(string &s, string &res, var &eval)
{
    if (stringOR(s, res, {"#t", "#f"}))
    {
        eval = var(res == "#t");
        return 1;
    }

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_Character(string &s, string &res, var &eval)
{
    // #\ <character name>
    if (stringOR(s, res, {R"(#\space)", R"(#\newline)"}))
    {
        if (res == R"(#\space)")
            eval = var(' ');
        else if (res == R"(#\newline)")
            eval = var('\n');
        return 1;
    }

    // #\ <any character>
    if (s.substr(0, 2) == R"(#\)" && s[2])
    {
        res = s.substr(0, 3);
        s.erase(0, 3);
        eval = var(res[0]);
        return 1;
    }

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_CharacterName(string &s, string& res)
{
    if (stringOR(s, res, {"space", "newline"}))
        return 1;

    res = "";
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_String(string &s, string &res, var &eval)
{
    string buffer, now;

    buffer = s;
    if (stringOR(buffer, res, {"\""}))
    {
        for (; _StringElement(buffer, now); res += now);
        if (stringOR(buffer, now, {"\""}))
        {
            res += now;
            s = buffer;
            eval = var(res.substr(1, res.size()-2));
            return 1;
        }
    }

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_StringElement(string &s, string &res)
{
    if (s[0] && s[0] != '\\' && s[0] != '\"')
    {
        res = s.substr(0, 1);
        s.erase(0, 1);
        return 1;
    }

    if (stringOR(s, res, {R"(\")", R"(\\)"}))
        return 1;

    res = "";
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_Number(string &s, string &res, var &eval)
{
    if (_NumR(2, s, res, eval))
        return 1;

    if (_NumR(8, s, res, eval))
        return 1;

    if (_NumR(10, s, res, eval))
        return 1;

    if (_NumR(16, s, res, eval))
        return 1;

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_NumR(const int &base, string &s, string &res, var &eval)
{
    if (base != 2 && base != 8 && base != 10 && base != 16)
        throw Exception::errorBase(base);

    string buffer = s, now, pref;
    int exact;

    if (_PrefixR(base, buffer, res))
    {
        pref = res;
        if (pref.find("#i"))
            exact = 1;
        else if (pref.find("#e"))
            exact = 0;
        else
            exact = -1;
        if (_ComplexR(base, buffer, now, eval, exact))
        {
            res += now;
            s = buffer;
            return 1;
        }
    }

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_ComplexR(const int &base, string &s, string &res, var &eval, const int &exact)
{
    if (base != 2 && base != 8 && base != 10 && base != 16)
        throw Exception::errorBase(base);

    string buffer, now, sign;
    var nowEval;

    // <real R> @ <real R>
    buffer = s;
    if (_RealR(base, buffer, res, eval, exact))
        if (stringOR(buffer, now, {"@"}))
        {
            res += now;
            if (_RealR(base, buffer, now, nowEval, exact))
            {
                res += now;
                s = buffer;
                eval = var(Complex((eval*var(cos(nowEval))).operator mpf_class(), (eval*var(sin(nowEval))).operator mpf_class()), 1);
                return 1;
            }
        }

    // <real R> + <ureal R> i
    // <real R> - <ureal R> i
    buffer = s;
    if (_RealR(base, buffer, res, eval, exact))
        if (stringOR(buffer, sign, {"+", "-"}))
        {
            res += sign;
            if (_URealR(base, buffer, now, nowEval, exact))
            {
                res += now;
                if (stringOR(buffer, now, {"i"}))
                {
                    res += now;
                    s = buffer;
                    if (*(eval.tid) == typeid(mpq_class).name() && *(nowEval.tid) == typeid(mpq_class).name())
                        eval = var(RtlComplex(eval.operator mpq_class(), nowEval.operator mpq_class()), *(eval.opt) | *(nowEval.opt));
                    else
                        eval = var(Complex(eval.operator mpf_class(), nowEval.operator mpf_class()), *(eval.opt) | *(nowEval.opt));
                    if (sign == "-")
                        eval = eval.conjugate();
                    return 1;
                }
            }
        }

    // <real R> + i
    // <real R> - i
    buffer = s;
    if (_RealR(base, buffer, res, eval, exact))
        if (stringOR(buffer, sign, {"+i", "-i"}))
        {
            res += sign;
            s = buffer;
            if (*(eval.tid) == typeid(mpq_class).name())
                eval = var(RtlComplex(eval.operator mpq_class(), (mpq_class)1), *(eval.opt));
            else
                eval = var(Complex(eval.operator mpf_class(), (mpf_class)1), *(eval.opt));
            if (sign == "-i")
                eval = eval.conjugate();
            return 1;
        }

    // + <ureal R> i
    // - <ureal R> i
    buffer = s;
    if (stringOR(buffer, sign, {"+", "-"}))
    {
        res = sign;
        if (_URealR(base, buffer, now, eval, exact))
        {
            res += now;
            if (stringOR(buffer, now, {"i"}))
            {
                res += now;
                s = buffer;
                if (*(eval.tid) == typeid(mpq_class).name())
                    eval = var(RtlComplex((mpq_class)0, eval.operator mpq_class()), *(eval.opt));
                else
                    eval = var(Complex((mpf_class)0, eval.operator mpf_class()), *(eval.opt));
                if (sign == "-")
                    eval = eval.conjugate();
                return 1;
            }
        }
    }

    // + i
    // - i
    if (stringOR(s, res, {"+i", "-i"}))
    {
        eval = var(RtlComplex((mpq_class)0, (mpq_class)(res == "+i" ? 1 : -1)), 0);
        return 1;
    }

    // <real R>
    if (_RealR(base, s, res, eval, exact))
        return 1;

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_RealR(const int &base, string &s, string &res, var &eval, const int &exact)
{
    if (base != 2 && base != 8 && base != 10 && base != 16)
        throw Exception::errorBase(base);

    string buffer, now;
    var nowEval;

    // <sign> <ureal R>
    buffer = s;
    if (_Sign(buffer, res))
    {
        eval = var((mpq_class)(res != "-" ? 1 : -1));
        if (_URealR(base, buffer, now, nowEval, exact))
        {
            res += now;
            s = buffer;
            eval = eval*nowEval;
            return 1;
        }
    }

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_URealR(const int &base, string &s, string &res, var &eval, const int &exact)
{
    if (base != 2 && base != 8 && base != 10 && base != 16)
        throw Exception::errorBase(base);

    string buffer, now;
    var nowEval;

    // <uinteger R> / <uinteger R>
    buffer = s;
    if (_UIntegerR(base, buffer, res, eval))
        if (stringOR(buffer, now, {"/"}))
        {
            res += now;
            if (_UIntegerR(base, buffer, now, nowEval))
            {
                res += now;
                s = buffer;
                eval = eval/nowEval;
                return 1;
            }
        }

    // <decimal R>
    if (base == 10 && _Decimal10(s, res, eval, exact))
        return 1;

    // <uinteger R>
    if (_UIntegerR(base, s, res, eval))
        return 1;

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_Decimal10(string &s, string &res, var &eval, const int &exact)
{
    string buffer, now;
    var nowEval;

    // . <digit 10>+ #* <suffix>
    buffer = s;
    if (stringOR(buffer, res, {"."}))
        if (_DigitR(10, buffer, now, eval))
        {
            res += now;
            for (; _DigitR(10, buffer, now, nowEval); res += now)
                eval = eval*var((mpq_class)10)+nowEval;
            for (; stringOR(buffer, now, {"#"}); res += now)
            for (int i = res.size()-1; i; --i)
                eval = eval/var((mpq_class)10);
            *(eval.opt) |= 1;
            if (_Suffix(buffer, now))
            {
                res += now;
                s = buffer;
                if (now != "")
                {
                    for (int i = atoi(now.substr(1).c_str()), j = abs(i); j; --j)
                        eval = i > 0 ? eval*var((mpq_class)10) : eval/var((mpq_class)10);
                    if (eval.floatRange() && exact)
                        eval = var(eval.operator mpf_class(), 1);
                    if (exact == 0)
                        *(eval.opt) &= ~1;
                }
                return 1;
            }
        }

    // <digit10>+ . <digit 10>* #* <suffix>
    buffer = s;
    if (_DigitR(10, buffer, res, eval))
    {
        for (; _DigitR(10, buffer, now, nowEval); res += now)
            eval = eval*var((mpq_class)10)+nowEval;
        if (stringOR(buffer, now, {"."}))
        {
            res += now;
            for (; _DigitR(10, buffer, now, nowEval); res += now)
                eval = eval*var((mpq_class)10)+nowEval;
            for (int i = res.length()-1-res.find("."); i; --i)
                eval = eval/var((mpq_class)10);
            for (; stringOR(buffer, now, {"#"}); res += now);
            *(eval.opt) |= 1;
            if (_Suffix(buffer, now))
            {
                res += now;
                s = buffer;
                if (now != "")
                {
                    for (int i = atoi(now.substr(1).c_str()), j = abs(i); j; --j)
                        eval = i > 0 ? eval*var((mpq_class)10) : eval/var((mpq_class)10);
                    if (eval.floatRange() && exact)
                        eval = var(eval.operator mpf_class(), 1);
                    if (exact == 0)
                        *(eval.opt) &= ~1;
                }
                return 1;
            }
        }
    }

    // <digit10>+ #+ . #* <suffix>
    buffer = s;
    if (_DigitR(10, buffer, res, eval))
    {
        for (; _DigitR(10, buffer, now, nowEval); res += now)
            eval = eval*var((mpq_class)10)+nowEval;
        if (stringOR(buffer, now, {"#"}))
        {
            res += now;
            eval = eval*var((mpq_class)10);
            for (; stringOR(buffer, now, {"#"}); res += now)
                eval = eval*var((mpq_class)10);
            if (stringOR(buffer, now, {"."}))
            {
                res += now;
                for (; stringOR(buffer, now, {"#"}); res += now);
                *(eval.opt) |= 1;
                if (_Suffix(buffer, now))
                {
                    res += now;
                    s = buffer;
                    if (now != "")
                    {
                        for (int i = atoi(now.substr(1).c_str()), j = abs(i); j; --j)
                            eval = i > 0 ? eval*var((mpq_class)10) : eval/var((mpq_class)10);
                        if (eval.floatRange() && exact)
                            eval = var((mpf_class)(eval.operator mpq_class()), 1);
                        if (exact == 0)
                            *(eval.opt) &= ~1;
                    }
                    return 1;
                }
            }
        }
    }

    // <uinteger 10> <suffix>
    buffer = s;
    if (_UIntegerR(10, buffer, res, eval))
        if (_Suffix(buffer, now))
        {
            res += now;
            s = buffer;
            if (now != "")
            {
                *(eval.opt) |= 1;
                for (int i = atoi(now.substr(1).c_str()), j = abs(i); j; --j)
                    eval = i > 0 ? eval*var((mpq_class)10) : eval/var((mpq_class)10);
                if (eval.floatRange() && exact)
                    eval = var((mpf_class)(eval.operator mpq_class()), 1);
                if (exact == 0)
                    *(eval.opt) &= ~1;
            }
            else
                if (exact == 0)
                    *(eval.opt) &= ~1;
            return 1;
        }

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_UIntegerR(const int &base, string &s, string &res, var &eval)
{
    if (base != 2 && base != 8 && base != 10 && base != 16)
        throw Exception::errorBase(base);

    string buffer, now;
    var nowEval;

    // <digit R>+ #*
    buffer = s;
    if (_DigitR(base, buffer, res, eval))
    {
        for (; _DigitR(base, buffer, now, nowEval); res += now)
            eval = eval*var((mpq_class)base)+nowEval;
        for (; stringOR(buffer, now, {"#"}); res += now)
        { 
            eval = eval*var((mpq_class)base);
            *(eval.opt) |= 1;
        }
        s = buffer;
        return 1;
    }

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_PrefixR(const int &base, string &s, string &res)
{
    if (base != 2 && base != 8 && base != 10 && base != 16)
        throw Exception::errorBase(base);

    string buffer, now;
    
    buffer = s;
    if (_RadixR(base, buffer, res))
        if (_Exactness(buffer, now))
        {
            res = now+res;
            s = buffer;
            return 1;
        }

    buffer = s;
    if (_Exactness(buffer, res))
        if (_RadixR(base, buffer, now))
        {
            res += now;
            s = buffer;
            return 1;
        }

    res = "";
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_Suffix(string &s, string &res)
{
    string buffer = s, now;
    var nowEval;

    if (_ExponentMarker(buffer, res))
        if (_Sign(buffer, now))
        {
            res += now;
            if (_DigitR(10, buffer, now, nowEval))
            {
                res += now;
                for (; _DigitR(10, buffer, now, nowEval); res += now);
                s = buffer;
                return 1;
            }
        }

    res = "";
    return 1;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_ExponentMarker(string &s, string &res)
{
    if (stringOR(s, res, {"e", "s", "f", "d", "l"}))
        return 1;

    res = "";
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_Sign(string &s, string &res)
{
    if (stringOR(s, res, {"+", "-"}))
        return 1;

    res = "";
    return 1;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_Exactness(string &s, string &res)
{
    if (stringOR(s, res, {"#i", "#e"}))
        return 1;

    res = "";
    return 1;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_RadixR(int base, string &s, string &res)
{
    if (base != 2 && base != 8 && base != 10 && base != 16)
        throw Exception::errorBase(base);

    if (base == 2 && stringOR(s, res, {"#b"}))
        return 1;

    if (base == 8 && stringOR(s, res, {"#o"}))
        return 1;

    if (base == 10 && stringOR(s, res, {"#d", ""}))
        return 1;

    if (base == 16 && stringOR(s, res, {"#x"}))
        return 1;

    res = "";
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_DigitR(int base, string &s, string &res, var &eval)
{
    if (base != 2 && base != 8 && base != 10 && base != 16)
        throw Exception::errorBase(base);

    if (base == 2 && stringOR(s, res, digitSet2))
    {
        eval = var((mpq_class)(res[0]-'0'));
        return 1;
    }

    if (base == 8 && stringOR(s, res, digitSet8))
    {
        eval = var((mpq_class)(res[0]-'0'));
        return 1;
    }

    if (base == 10 && stringOR(s, res, digitSet10))
    {
        eval = var((mpq_class)(res[0]-'0'));
        return 1;
    }

    if (base == 16 && stringOR(s, res, digitSet16))
    {
        eval = var((mpq_class)(res[0]-(isdigit(res[0]) ? '0' : 'a')));
        return 1;
    }

    res = "";
    eval = var();
    return 0;
}

//External representations

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_Datum(string &s, string &res, var &eval, Environment &env)
{
    if (_SimpleDatum(s, res, eval, env))
        return 1;

    if (_CompoundDatum(s, res, eval, env))
        return 1;

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_SimpleDatum(string &s, string &res, var &eval, Environment &env)
{
    if (_Boolean(s, res, eval))
        return 1;

    if (_Number(s, res, eval))
        return 1;

    if (_Character(s, res, eval))
        return 1;

    if (_String(s, res, eval))
        return 1;

    if (_Symbol(s, res, eval))
        return 1;

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_Symbol(string &s, string &res, var &eval)
{
    if (_Identifier(s, res))
    {
        eval = var(Symbol(res));
        return 1;
    }

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_CompoundDatum(string &s, string &res, var &eval, Environment &env)
{
    if (_List(s, res, eval, env))
        return 1;

    if (_Vector(s, res, eval, env))
        return 1;

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_List(string &s, string &res, var &eval, Environment &env)
{
    string buffer, now;
    var nowEval;
    vector<var> datumList;

    // (<datum>*)
    buffer = s;
    datumList.clear();
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        for (; _Datum(buffer, now, nowEval, env); datumList.push_back(nowEval))
        {
            res += now;
            _IntertokenSpace(buffer);
            res += " ";
        }
        datumList.push_back(var(EmptyList()));
        if (stringOR(buffer, now, {")"}))
        {
            res += now;
            s = buffer;
            eval = datumList.back();
            datumList.pop_back();
            for (; datumList.size(); datumList.pop_back())
                eval = var(Pair(datumList.back(), eval));
            return 1;
        }
    }

    // (<datum>+ . <datum>)
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (_Datum(buffer, now, nowEval, env))
        {
            datumList.push_back(nowEval);
            res += now;
            _IntertokenSpace(buffer);
            res += " ";
            for (; _Datum(buffer, now, nowEval, env); datumList.push_back(nowEval))
            {
                res += now;
                _IntertokenSpace(buffer);
                res += " ";
            }
            if (stringOR(buffer, now, {"."}))
            {
                res += now;
                _IntertokenSpace(buffer);
                res += " ";
                if (_Datum(buffer, now, nowEval, env))
                {
                    datumList.push_back(nowEval);
                    res += now;
                    _IntertokenSpace(buffer);
                    res += " ";
                    if (stringOR(buffer, now, {")"}))
                    {
                        res += now;
                        s = buffer;
                        eval = datumList.back();
                        datumList.pop_back();
                        for (; datumList.size(); datumList.pop_back())
                            eval = var(Pair(datumList.back(), eval));
                        return 1;
                    }
                }
            }
        }
    }

    // <abbreviation>
    if (_Abbreviation(s, res, eval, env))
        return 1;

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_Abbreviation(string &s, string &res, var &eval, Environment &env)
{
    string buffer = s, now, pref;
    var nowEval;

    if (_AbbrevPrefix(buffer, pref))
    {
        res = pref;
        _IntertokenSpace(buffer);
        res += " ";
        if (_Datum(buffer, now, nowEval, env))
        {
            res += now;
            s = buffer;
            if (pref == "\'")
                eval = var(Pair(Symbol("quote"), var(Pair(eval, var(EmptyList())))));
            // Quasiquote
            return 1;
        }
    }

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_AbbrevPrefix(string &s, string &res)
{
    if (stringOR(s, res, {"\'", "`", ",@", ","}))
        return 1;

    res = "";
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_Vector(string &s, string &res, var &eval, Environment &env)
{
    string buffer = s, now, code;
    var nowEval;
    vector<var> datumList;

    datumList.clear();
    if (stringOR(buffer, res, {"#("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        for (; _Datum(buffer, now, nowEval, env); datumList.push_back(nowEval))
        {
            res += now;
            _IntertokenSpace(buffer);
            res += " ";
        }
        if (stringOR(buffer, now, {")"}))
        {
            res += now;
            s = buffer;
            eval = var(Vector());
            eval.vecClear();
            for (auto iter = datumList.begin(); iter != datumList.end(); ++iter)
                eval.vecPushBack(*iter);
            return 1;
        }
    }

    res = "";
    eval = var();
    return 0;
}

//Expressions

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_Expression(string &s, string &res, var &eval, Environment &env, const int &exec)
{
    if (_Literal(s, res, eval, env, exec))
        return 1;

    if (_Variable(s, res))
    {
        if (exec)
            eval = env.getVar(res);
        else
            eval = var(Symbol(res));
        return 1;
    }

    if (_LambdaExpression(s, res, eval, env, exec))
        return 1;

    if (_Conditional(s, res, eval, env, exec))
        return 1;

    if (_Assignment(s, res, eval, env, exec))
        return 1;

    if (_DerivedExpression(s, res, eval, env, exec))
        return 1;

    if (_ProcedureCall(s, res, eval, env, exec))
        return 1;

    if (_MacroUse(s, res))
        return 1;

    if (_MacroBlock(s, res))
        return 1;

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_Literal(string &s, string &res, var &eval, Environment &env, const int &exec)
{
    if (_Quotation(s, res, eval, env, exec))
        return 1;
    if (_SelfEvaluating(s, res, eval))
        return 1;

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// For more details, please refer to R5RS.
int Interpreter::_SelfEvaluating(string &s, string &res, var &eval)
{
    if (_Boolean(s, res, eval))
        return 1;
    if (_Number(s, res, eval))
        return 1;
    if (_Character(s, res, eval))
        return 1;
    if (_String(s, res, eval))
        return 1;

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_Quotation(string &s, string &res, var &eval, Environment &env, const int &exec)
{
    string buffer, now, code;

    buffer = s;
    res = "";
    if (stringOR(buffer, res, {"\'"}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (_Datum(buffer, now, eval, env))
        {
            res += now;
            s = buffer;
            if (! exec)
                eval = var(Pair(var(Symbol("quote")), var(Pair(eval, var(EmptyList())))));
            return 1;
        }
    }

    buffer = s;
    res = "";
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (stringOR(buffer, now, {"quote"}))
        {
            res += now;
            _IntertokenSpace(buffer);
            res += " ";
            if (_Datum(buffer, now, eval, env))
            {
                res += now;
                _IntertokenSpace(buffer);
                res += " ";
                if (stringOR(buffer, now, {")"}))
                {
                    res += now;
                    s = buffer;
                    if (! exec)
                        eval = var(Pair(var(Symbol("quote")), var(Pair(eval, var(EmptyList())))));
                    return 1;
                }
            }
        }
    }

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_ProcedureCall(string &s, string &res, var &eval, Environment &env, const int &exec)
{
    string buffer, now, optCode;
    vector<string> oprCode;
    var nowEval;

    // (<operator> <operand>*)
    buffer = s;
    oprCode.clear();
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (_Operator(buffer, optCode, nowEval, env, 0))
        {
            res += optCode;
            _IntertokenSpace(buffer);
            res += " ";
            for (; _Operand(buffer, now, nowEval, env, 0); oprCode.push_back(now))
            {
                res += now;
                _IntertokenSpace(buffer);
                res += " ";
            }
            if (stringOR(buffer, now, {")"}))
            {
                res += now;
                s = buffer;
                if (exec)
                {
                    _Operator(optCode, now, nowEval, env, 1);
                    eval = var(Pair(nowEval, var(EmptyList())));
                    for (auto iter = oprCode.begin(); iter != oprCode.end(); ++iter)
                    {
                        _Operand(*iter, now, nowEval, env, 1);
                        eval.append(nowEval);
                    }
                    nowEval = eval;
                    return run(nowEval, env, eval);
                }
                return 1;
            }
        }
    }

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_Operator(string &s, string &res, var &eval, Environment &env, const int &exec)
{
    if (_Expression(s, res, eval, env, exec))
        return 1;

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_Operand(string &s, string &res, var &eval, Environment &env, const int &exec)
{
    if (_Expression(s, res, eval, env, exec))
        return 1;

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_LambdaExpression(string &s, string &res, var &eval, Environment &env, const int &exec)
{
    string buffer, now;
    var nowEval;
    Environment nowEnv;

    buffer = s;
    nowEnv = env;
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (stringOR(buffer, now, {"lambda"}))
        {
            res += now;
            _IntertokenSpace(buffer);
            res += " ";
            if (_Formals(buffer, now, eval, env))
            {
                res += now;
                _IntertokenSpace(buffer);
                res += " ";
                if (_Body(buffer, now, nowEval, nowEnv, 0))
                {
                    res += now;
                    eval.setCode(now);
                    _IntertokenSpace(buffer);
                    res += " ";
                    if (stringOR(buffer, now, {")"}))
                    {
                        res += now;
                        s = buffer;
                        if (! exec)
                            eval = var(Pair(var(Symbol("lambda")), var(EmptyList())));
                        return 1;
                    }
                }
            }
        }
    }

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// It will add the matching variable the the arguments list of "eval".
/// For more details, please refer to R5RS.
int Interpreter::_Formals(string &s, string &res, var &eval, Environment &env)
{
    string buffer, now;

    buffer = s;
    eval = var(Proc(env));
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (_Variable(buffer, now))
        {
            res += now;
            eval.addArg(now);
            _IntertokenSpace(buffer);
            res += " ";
            while (_Variable(buffer, now))
            {
                res += now;
                eval.addArg(now);
                _IntertokenSpace(buffer);
                res += " ";
            }
            if (stringOR(buffer, now, {"."}))
            {
                res += now;
                _IntertokenSpace(buffer);
                res += " ";
                if (_Variable(buffer, now))
                {
                    res += now;
                    eval.setListArg(now);
                    _IntertokenSpace(buffer);
                    res += " ";
                    if (stringOR(buffer, now, {")"}))
                    {
                        res += now;
                        s = buffer;
                        return 1;
                    }
                }
            }
        }
    }

    buffer = s;
    eval = var(Proc(env));
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        while (_Variable(buffer, now))
        {
            res += now;
            eval.addArg(now);
            _IntertokenSpace(buffer);
            res += " ";
        }
        if (stringOR(buffer, now, {")"}))
        {
            res += now;
            s = buffer;
            return 1;
        }
    }

    eval = var(Proc(env));
    if (_Variable(s, res))
    {
        eval.setListArg(res);
        return 1;
    }

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_Body(string &s, string &res, var &eval, Environment &env, const int &exec)
{
    string buffer, now;
    var nowEval;

    buffer = s;
    res = "";
    for (; _Definition(buffer, now, env, 0);)
    {
        res += now;
        _IntertokenSpace(buffer);
        res += " ";
    }
    if (_Sequence(buffer, now, nowEval, env, 0))
    {
        res += now;
        s = buffer;
        buffer = res;
        eval = var();
        if (exec)
        {
            for (; _Definition(buffer, now, env, 1); _IntertokenSpace(buffer));
            return _Sequence(buffer, now, eval, env, 1);
        }
        return 1;
    }

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_Sequence(string &s, string &res, var &eval, Environment &env, const int &exec)
{
    string buffer, now;
    var nowEval;
    bool flag;

    buffer = s;
    flag = 0;
    res = "";
    for (; _Command(buffer, now, nowEval, env, 0); flag = 1)
    {
        res += now;
        _IntertokenSpace(buffer);
        res += " ";
    }
    if (_Expression(buffer, now, nowEval, env, 0))
    {
        res += now;
        flag = 1;
    }

    if (flag)
    {
        s = buffer;
        buffer = res;
        eval = var();
        if (exec)
        {
            for (; _Command(buffer, now, nowEval, env, 1); _IntertokenSpace(buffer))
                eval = nowEval;
            if (_Expression(buffer, now, nowEval, env, 1))
                eval = nowEval;
        }
        return 1;
    }

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_Command(string &s, string &res, var &eval, Environment &env, const int &exec)
{
    if (_Expression(s, res, eval, env, exec))
        return 1;

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_Conditional(string &s, string &res, var &eval, Environment &env, const int &exec)
{
    string buffer, now, testCode, ifCode, elseCode;
    var nowEval;
    Environment nowEnv;

    buffer = s;
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (stringOR(buffer, now, {"if"}))
        {
            res += now;
            _IntertokenSpace(buffer);
            res += " ";
            if (_Test(buffer, testCode, nowEval, env, 0))
            {
                res += testCode;
                _IntertokenSpace(buffer);
                res += " ";
                if (_Consequent(buffer, ifCode, nowEval, env, 0))
                {
                    res += ifCode;
                    _IntertokenSpace(buffer);
                    res += " ";
                    if (_Alternate(buffer, elseCode, nowEval, env, 0))
                    {
                        res += elseCode;
                        _IntertokenSpace(buffer);
                        res += " ";
                        if (stringOR(buffer, now, {")"}))
                        {
                            res += now;
                            s = buffer;
                            if (exec)
                            {
                                _Test(testCode, now, nowEval, env, 1);
                                if (nowEval.operator bool())
                                    return _Consequent(ifCode, now, eval, env, 1);
                                else
                                    return _Alternate(elseCode, now, eval, env, 1);
                            }
                            else
                                eval = var(Pair(var(Symbol("if")), var(EmptyList())));
                            return 1;
                        }
                    }
                }
            }
        }
    }

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_Test(string &s, string &res, var &eval, Environment &env, const int &exec)
{
    if (_Expression(s, res, eval, env, exec))
        return 1;

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_Consequent(string &s, string &res, var &eval, Environment &env, const int &exec)
{
    if (_Expression(s, res, eval, env, exec))
        return 1;

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_Alternate(string &s, string &res, var &eval, Environment &env, const int &exec)
{
    if (_Expression(s, res, eval, env, exec))
        return 1;

    res = "";
    eval = var();
    return 1;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_Assignment(string &s, string &res, var &eval, Environment &env, const int &exec)
{
    string buffer, now, varName, expCode;
    var nowEval;

    buffer = s;
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (stringOR(buffer, now, {"set!"}))
        {
            res += now;
            _IntertokenSpace(buffer);
            res += " ";
            if (_Variable(buffer, varName))
            {
                res += varName;
                _IntertokenSpace(buffer);
                res += " ";
                if (_Expression(buffer, expCode, nowEval, env, 0))
                {
                    res += expCode;
                    _IntertokenSpace(buffer);
                    res += " ";
                    if (stringOR(buffer, now, {")"}))
                    {
                        res += now;
                        s = buffer;
                        if (exec)
                        {
                            _Expression(expCode, now, nowEval, env, 1);
                            env.setVar(varName, nowEval);
                            eval = var();
                        }
                        else
                            eval = var(Pair(var(Symbol("set!")), var(Pair(var(Symbol(varName)), var(EmptyList())))));
                        return 1;
                    }
                }
            }
        }
    }

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_DerivedExpression(string &s, string &res, var &eval, Environment &env, const int &exec)
{
    string buffer, now, step, elseStr, expStr, bodyStr, seqStr, testStr, doResultStr;
    var nowEval, targetEval;
    Environment nowEnv, tmpEnv;
    vector<string> condList, caseList, testList, bindList, stepList, commandList;
    int match;

    // (cond <cond clause>* (else <sequence>))
    buffer = s;
    condList.clear();
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (stringOR(buffer, now, {"cond"}))
        {
            res += now;
            _IntertokenSpace(buffer);
            res += " ";
            for (; _CondClause(buffer, now, nowEval, env, 0); condList.push_back(now))
            {
                res += now;
                _IntertokenSpace(buffer);
                res += " ";
            }
            if (stringOR(buffer, now, {"("}))
            {
                res += now;
                _IntertokenSpace(buffer);
                res += " ";
                if (stringOR(buffer, now, {"else"}))
                {
                    res += now;
                    _IntertokenSpace(buffer);
                    res += " ";
                    nowEnv = env;
                    if (_Sequence(buffer, elseStr, nowEval, nowEnv, 0))
                    {
                        res += elseStr;
                        _IntertokenSpace(buffer);
                        res += " ";
                        if (stringOR(buffer, now, {")"}))
                        {
                            res += now;
                            s = buffer;
                            eval = var();
                            if (exec)
                            {
                                match = 0;
                                for (auto iter = condList.begin(); iter != condList.end(); ++iter)
                                    if (_CondClause(*iter, now, nowEval, env, 1) & 2)
                                    {
                                        eval = nowEval;
                                        match = 1;
                                        break;
                                    }
                                if (! match)
                                {
                                    nowEnv = env;
                                    return _Sequence(elseStr, now, eval, nowEnv, 1);
                                }
                            }
                            return 1;
                        }
                    }
                }
            }
        }
    }

    // (cond <cond clause>+)
    buffer = s;
    condList.clear();
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (stringOR(buffer, now, {"cond"}))
        {
            res += now;
            _IntertokenSpace(buffer);
            res += " ";
            if (_CondClause(buffer, now, nowEval, env, 0))
            {
                res += now;
                _IntertokenSpace(buffer);
                res += " ";
                condList.push_back(now);
                for (; _CondClause(buffer, now, nowEval, env, 0); condList.push_back(now))
                {
                    res += now;
                    _IntertokenSpace(buffer);
                    res += " ";
                }
                if (stringOR(buffer, now, {")"}))
                {
                    res += now;
                    s = buffer;
                    eval = var();
                    if (exec)
                        for (auto iter = condList.begin(); iter != condList.end(); ++iter)
                            if (_CondClause(*iter, now, nowEval, env, 1) & 2)
                            {
                                eval = nowEval;
                                match = 1;
                                break;
                            }
                    return 1;
                }
            }
        }
    }

    // (case <expression> <case clause>* (else <sequence>))
    buffer = s;
    caseList.clear();
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (stringOR(buffer, now, {"case"}))
        {
            res += now;
            _IntertokenSpace(buffer);
            res += " ";
            if (_Expression(buffer, expStr, nowEval, env, 0))
            {
                res += expStr;
                _IntertokenSpace(buffer);
                res += " ";
                for (; _CaseClause(buffer, now, nowEval, env, targetEval, 0); caseList.push_back(now))
                {
                    res += now;
                    _IntertokenSpace(buffer);
                    res += " ";
                }
                if (stringOR(buffer, now, {"("}))
                {
                    res += now;
                    _IntertokenSpace(buffer);
                    res += " ";
                    if (stringOR(buffer, now, {"else"}))
                    {
                        res += now;
                        _IntertokenSpace(buffer);
                        res += " ";
                        nowEnv = env;
                        if (_Sequence(buffer, elseStr, nowEval, nowEnv, 0))
                        {
                            res += elseStr;
                            _IntertokenSpace(buffer);
                            res += " ";
                            if (stringOR(buffer, now, {")"}))
                            {
                                res += now;
                                s = buffer;
                                eval = var();
                                if (exec)
                                {
                                    _Expression(expStr, now, targetEval, env, 1);
                                    match = 0;
                                    for (auto iter = caseList.begin(); iter != caseList.end(); ++iter)
                                        if (_CaseClause(*iter, now, nowEval, env, targetEval, 1) & 2)
                                        {
                                            eval = nowEval;
                                            match = 1;
                                            break;
                                        }
                                    if (! match)
                                    {
                                        nowEnv = env;
                                        return _Sequence(elseStr, now, eval, nowEnv, 1);
                                    }
                                }
                                return 1;
                            }
                        }
                    }
                }
            }
        }
    }

    // (case <expression> <case clause>+)
    buffer = s;
    caseList.clear();
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (stringOR(buffer, now, {"case"}))
        {
            res += now;
            _IntertokenSpace(buffer);
            res += " ";
            if (_Expression(buffer, expStr, nowEval, env, 0))
            {
                res += expStr;
                _IntertokenSpace(buffer);
                res += " ";
                if (_CaseClause(buffer, now, nowEval, env, targetEval, 0))
                {
                    res += now;
                    _IntertokenSpace(buffer);
                    res += " ";
                    caseList.push_back(now);
                    for (; _CaseClause(buffer, now, nowEval, env, targetEval, 0); caseList.push_back(now))
                    {
                        res += now;
                        _IntertokenSpace(buffer);
                        res += " ";
                    }
                    if (stringOR(buffer, now, {")"}))
                    {
                        res += now;
                        s = buffer;
                        eval = var();
                        if (exec)
                        {
                            _Expression(expStr, now, targetEval, env, 1);
                            for (auto iter = caseList.begin(); iter != caseList.end(); ++iter)
                                if (_CaseClause(*iter, now, nowEval, env, targetEval, 1) & 2)
                                {
                                    eval = nowEval;
                                    match = 1;
                                    break;
                                }
                        }
                        return 1;
                    }
                }
            }
        }
    }

    // (and <test>*)
    buffer = s;
    testList.clear();
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (stringOR(buffer, now, {"and"}))
        {
            res += now;
            _IntertokenSpace(buffer);
            res += " ";
            for (; _Test(buffer, now, nowEval, env, 0); testList.push_back(now))
            {
                res += now;
                _IntertokenSpace(buffer);
                res += " ";
            }
            if (stringOR(buffer, now, {")"}))
            {
                res += now;
                s = buffer;
                if (exec)
                {
                    eval = var(true);
                    for (auto iter = testList.begin(); iter != testList.end(); ++iter)
                    {
                        _Test(*iter, now, nowEval, env, 1);
                        if (! nowEval.operator bool())
                        {
                            eval = nowEval;
                            break;
                        }
                    }
                }
                else
                    eval = var();
                return 1;
            }
        }
    }

    // (or <test>*)
    buffer = s;
    testList.clear();
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (stringOR(buffer, now, {"or"}))
        {
            res += now;
            _IntertokenSpace(buffer);
            res += " ";
            for (; _Test(buffer, now, nowEval, env, 0); testList.push_back(now))
            {
                res += now;
                _IntertokenSpace(buffer);
                res += " ";
            }
            if (stringOR(buffer, now, {")"}))
            {
                res += now;
                s = buffer;
                if (exec)
                {
                    eval = var(false);
                    for (auto iter = testList.begin(); iter != testList.end(); ++iter)
                    {
                        _Test(*iter, now, nowEval, env, 1);
                        if (nowEval.operator bool())
                        {
                            eval = nowEval;
                            break;
                        }
                    }
                }
                else
                    eval = var();
                return 1;
            }
        }
    }

    // (let* (<binding spec>*) <body>)
    buffer = s;
    nowEnv = env;
    bindList.clear();
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (stringOR(buffer, now, {"let*"}))
        {
            res += now;
            _IntertokenSpace(buffer);
            res += " ";
            if (stringOR(buffer, now, {"("}))
            {
                res += now;
                _IntertokenSpace(buffer);
                res += " ";
                for (; _BindingSpec(buffer, now, nowEval, nowEnv, 0); bindList.push_back(now))
                {
                    res += now;
                    _IntertokenSpace(buffer);
                    res += " ";
                }
                if (stringOR(buffer, now, {")"}))
                {
                    res += now;
                    _IntertokenSpace(buffer);
                    res += " ";
                    if (_Body(buffer, bodyStr, nowEval, nowEnv, 0))
                    {
                        res += now;
                        _IntertokenSpace(buffer);
                        res += " ";
                        if (stringOR(buffer, now, {")"}))
                        {
                            res += now;
                            s = buffer;
                            if (exec)
                            {
                                for (auto iter = bindList.begin(); iter != bindList.end(); ++iter)
                                    _BindingSpec(*iter, now, nowEval, nowEnv, 1);
                                return _Body(bodyStr, now, eval, nowEnv, 1);
                            }
                            else
                                eval = var();
                            return 1;
                        }
                    }
                }
            }
        }
    }

    // (letrec (<binding spec>*) <body>)
    buffer = s;
    nowEnv = env;
    bindList.clear();
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (stringOR(buffer, now, {"letrec"}))
        {
            res += now;
            _IntertokenSpace(buffer);
            res += " ";
            if (stringOR(buffer, now, {"("}))
            {
                res += now;
                _IntertokenSpace(buffer);
                res += " ";
                for (; _BindingSpec(buffer, now, nowEval, nowEnv, 0); bindList.push_back(now))
                {
                    res += now;
                    _IntertokenSpace(buffer);
                    res += " ";
                }
                if (stringOR(buffer, now, {")"}))
                {
                    res += now;
                    _IntertokenSpace(buffer);
                    res += " ";
                    if (_Body(buffer, bodyStr, nowEval, nowEnv, 0))
                    {
                        res += now;
                        _IntertokenSpace(buffer);
                        res += " ";
                        if (stringOR(buffer, now, {")"}))
                        {
                            res += now;
                            s = buffer;
                            if (exec)
                            {
                                for (auto iter = bindList.begin(); iter != bindList.end(); ++iter)
                                    _BindingSpec(*iter, now, nowEval, nowEnv, 2);
                                for (auto iter = bindList.begin(); iter != bindList.end(); ++iter)
                                    _BindingSpec(*iter, now, nowEval, nowEnv, 1);
                                return _Body(bodyStr, now, eval, nowEnv, 1);
                            }
                            else
                                eval = var();
                            return 1;
                        }
                    }
                }
            }
        }
    }

    // (let <variable> (<binding spec>*) <body>)

    // (let (<binding spec>*) <body>)
    buffer = s;
    nowEnv = env;
    bindList.clear();
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (stringOR(buffer, now, {"let"}))
        {
            res += now;
            _IntertokenSpace(buffer);
            res += " ";
            if (stringOR(buffer, now, {"("}))
            {
                res += now;
                _IntertokenSpace(buffer);
                res += " ";
                for (; _BindingSpec(buffer, now, nowEval, nowEnv, 0); bindList.push_back(now))
                {
                    res += now;
                    _IntertokenSpace(buffer);
                    res += " ";
                }
                if (stringOR(buffer, now, {")"}))
                {
                    res += now;
                    _IntertokenSpace(buffer);
                    res += " ";
                    if (_Body(buffer, bodyStr, nowEval, nowEnv, 0))
                    {
                        res += now;
                        _IntertokenSpace(buffer);
                        res += " ";
                        if (stringOR(buffer, now, {")"}))
                        {
                            res += now;
                            s = buffer;
                            if (exec)
                            {
                                for (auto iter = bindList.begin(); iter != bindList.end(); ++iter)
                                {
                                    tmpEnv = env;
                                    _BindingSpec(*iter, now, nowEval, tmpEnv, 1);
                                    nowEnv = nowEnv+tmpEnv;
                                }
                                return _Body(bodyStr, now, eval, nowEnv, 1);
                            }
                            else
                                eval = var();
                            return 1;
                        }
                    }
                }
            }
        }
    }

    // (begin <sequence>)
    buffer = s;
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += now;
        if (stringOR(buffer, now, {"begin"}))
        {
            res += now;
            _IntertokenSpace(buffer);
            res += " ";
            if (_Sequence(buffer, seqStr, nowEval, env, 0))
            {
                res += now;
                _IntertokenSpace(buffer);
                res += " ";
                if (stringOR(buffer, now, {")"}))
                {
                    res += now;
                    s = buffer;
                    if (exec)
                        return _Sequence(seqStr, now, eval, env, 1);
                    else
                        eval = var();
                    return 1;
                }
            }
        }
    }

    // (do (<iteration spec>*) (<test> <do result>) <command>*)
    buffer = s;
    stepList.clear();
    nowEnv = env;
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (stringOR(buffer, now, {"do"}))
        {
            res += now;
            _IntertokenSpace(buffer);
            res += " ";
            if (stringOR(buffer, now, {"("}))
            {
                res += now;
                _IntertokenSpace(buffer);
                res += " ";
                for (; _IterationSpec(buffer, now, nowEnv, step, 0); stepList.push_back(now))
                {
                    res += now;
                    _IntertokenSpace(buffer);
                    res += " ";
                }
                if (stringOR(buffer, now, {")"}))
                {
                    res += now;
                    _IntertokenSpace(buffer);
                    res += " ";
                    if (stringOR(buffer, now, {"("}))
                    {
                        res += now;
                        _IntertokenSpace(buffer);
                        res += " ";
                        if (_Test(buffer, testStr, nowEval, nowEnv, 0))
                        {
                            res += testStr;
                            _IntertokenSpace(buffer);
                            res += " ";
                            if (_DoResult(buffer, doResultStr, nowEval, nowEnv, 0))
                            {
                                res += doResultStr;
                                _IntertokenSpace(buffer);
                                res += " ";
                                if (stringOR(buffer, now, {")"}))
                                {
                                    res += now;
                                    _IntertokenSpace(buffer);
                                    res += " ";
                                    for (; _Command(buffer, now, nowEval, nowEnv, 0); commandList.push_back(now))
                                    {
                                        res += now;
                                        _IntertokenSpace(buffer);
                                        res += " ";
                                    }
                                    if (stringOR(buffer, now, {")"}))
                                    {
                                        res += now;
                                        s = buffer;
                                        if (exec)
                                        {
                                            for (auto iter = stepList.begin(); iter != stepList.end(); ++iter)
                                            {
                                                _IterationSpec(*iter, now, nowEnv, step, 1);
                                                *iter = step;
                                            }
                                            while (1)
                                            {
                                                buffer = testStr;
                                                _Test(buffer, now, nowEval, nowEnv, 1);
                                                if (nowEval.operator bool())
                                                {
                                                    for (auto iter = commandList.begin(); iter != commandList.end(); ++iter)
                                                    {
                                                        buffer = *iter;
                                                        _Command(buffer, now, nowEval, nowEnv, 1);
                                                    }
                                                    for (auto iter = stepList.begin(); iter != stepList.end(); ++iter)
                                                    {
                                                        buffer = *iter;
                                                        _Step(buffer, now, nowEval, nowEnv, 1);
                                                    }
                                                }
                                                else
                                                {
                                                    return _DoResult(doResultStr, now, eval, nowEnv, 1);
                                                }
                                            }
                                        }
                                        else
                                            eval = var();
                                        return 1;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    // (delay <expression>)
    buffer = s;
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += now;
        if (stringOR(buffer, now, {"delay"}))
        {
            res += now;
            _IntertokenSpace(buffer);
            res += " ";
            if (_Expression(buffer, expStr, nowEval, env, 0))
            {
                res += now;
                _IntertokenSpace(buffer);
                res += " ";
                if (stringOR(buffer, now, {")"}))
                {
                    res += now;
                    s = buffer;
                    if (exec)
                        eval = var(Proc(expStr, env));
                    else
                        eval = var();
                    return 1;
                }
            }
        }
    }

    // <quasiquotation>

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_CondClause(string &s, string &res, var &eval, Environment &env, const int &exec)
{
    string buffer, now, testCode, seqCode;
    var nowEval, testEval;
    Environment nowEnv;

    // (<test> <sequence>)
    buffer = s;
    nowEnv = env;
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (_Test(buffer, testCode, nowEval, nowEnv, 0))
        {
            res += testCode;
            _IntertokenSpace(buffer);
            res += " ";
            if (_Sequence(buffer, seqCode, nowEval, nowEnv, 0))
            {
                res += seqCode;
                _IntertokenSpace(buffer);
                res += " ";
                if (stringOR(buffer, now, {")"}))
                {
                    res += now;
                    s = buffer;
                    if (exec)
                    {
                        _Test(testCode, now, nowEval, nowEnv, 1);
                        if (nowEval.operator bool())
                        {
                            _Sequence(seqCode, now, eval, nowEnv, 1);
                            return 3;
                        }
                        else
                            eval = var();
                    }
                    else
                        eval = var();
                    return 1;
                }
            }
        }
    }

    // (<test> => <recipient>)
    buffer = s;
    nowEnv = env;
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (_Test(buffer, testCode, nowEval, nowEnv, 0))
        {
            res += testCode;
            _IntertokenSpace(buffer);
            res += " ";
            if (stringOR(buffer, now, {"=>"}))
            {
                res += now;
                _IntertokenSpace(buffer);
                res += " ";
                if (_Recipient(buffer, seqCode, nowEval, nowEnv, 0))
                {
                    res += seqCode;
                    s = buffer;
                    _IntertokenSpace(buffer);
                    res += " ";
                    if (stringOR(buffer, now, {")"}))
                    {
                        res += now;
                        s = buffer;
                        if (exec)
                        {
                            _Test(testCode, now, nowEval, nowEnv, 1);
                            testEval = nowEval;
                            if (testEval.operator bool())
                            {
                                _Recipient(testCode, now, nowEval, nowEnv, 1);
                                nowEval = var(Pair(nowEval, var(Pair(testEval, var()))));
                                run(nowEval, nowEnv, eval);
                                return 3;
                            }
                            else
                                eval = var();
                        }
                        else
                            eval = var();
                        return 1;
                    }
                }
            }
        }
    }

    // (<test>)
    buffer = s;
    nowEnv = env;
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (_Test(buffer, testCode, nowEval, nowEnv, 0))
        {
            res += testCode;
            _IntertokenSpace(buffer);
            res += " ";
            if (stringOR(buffer, now, {")"}))
            {
                res += now;
                s = buffer;
                if (exec)
                {
                    _Test(testCode, now, nowEval, nowEnv, 1);
                    if (nowEval.operator bool())
                    {
                        eval = nowEval;
                        return 3;
                    }
                    else
                    {
                        eval = var();
                        return 1;
                    }
                }
                else
                {
                    eval = var();
                    return 1;
                }
            }
        }
    }

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_Recipient(string &s, string &res, var &eval, Environment &env, const int &exec)
{
    if (_Expression(s, res, eval, env, exec))
        return 1;

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_CaseClause(string &s, string &res, var &eval, Environment &env, var &target, const int &exec)
{
    string buffer, now, seqCode;
    var nowEval;
    Environment nowEnv = env;
    int flag;

    buffer = s;
    flag = 0;
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (stringOR(buffer, now, {"("}))
        {
            res += now;
            _IntertokenSpace(buffer);
            res += " ";
            for (; _Datum(buffer, now, nowEval, nowEnv); flag |= (nowEval == target))
            {
                res += now;
                _IntertokenSpace(buffer);
                res += " ";
            }
            if (stringOR(buffer, now, {")"}))
            {
                res += now;
                _IntertokenSpace(buffer);
                res += " ";
                if (_Sequence(buffer, seqCode, nowEval, nowEnv, 0))
                {
                    res += seqCode;
                    _IntertokenSpace(buffer);
                    res += " ";
                    if (stringOR(buffer, now, {")"}))
                    {
                        res += now;
                        s = buffer;
                        if (exec)
                        {
                            if (flag)
                            {
                                _Sequence(seqCode, now, eval, nowEnv, 1);
                                return 3;
                            }
                            else
                                eval = var();
                        }
                        else
                            eval = var();
                        return 1;
                    }
                }
            }
        }
    }

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed. Variables will be declared and assigned to its value.
/// If exec = 2, it just declare the variables without assignment.
/// For more details, please refer to R5RS.
int Interpreter::_BindingSpec(string &s, string &res, var &eval, Environment &env, const int &exec)
{
    string buffer, now, varName, expCode;
    var nowEval;

    buffer = s;
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (_Variable(buffer, varName))
        {
            res += varName;
            _IntertokenSpace(buffer);
            res += " ";
            if (_Expression(buffer, expCode, nowEval, env, 0))
            {
                res += now;
                _IntertokenSpace(buffer);
                res += " ";
                if (stringOR(buffer, now, {")"}))
                {
                    res += now;
                    s = buffer;
                    if (exec & 1)
                    {
                        _Expression(expCode, now, eval, env, 1);
                        env.addVar(varName);
                        env.setVar(varName, eval);
                    }
                    if (exec & 2)
                        env.addVar(varName);
                    if (! exec)
                        eval = var();
                    return 1;
                }
            }
        }
    }

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// The code which should be executed for the variable after each iteration will be store in "step".
/// For more details, please refer to R5RS.
int Interpreter::_IterationSpec(string &s, string &res, Environment &env, string &step, const int &exec)
{
    string buffer, now, varName, initCode;
    var nowEval;

    // (<variable> <init> <step>)
    buffer = s;
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (_Variable(buffer, varName))
        {
            res += varName;
            _IntertokenSpace(buffer);
            res += " ";
            if (_Init(buffer, initCode, nowEval, env, 0))
            {
                res += initCode;
                _IntertokenSpace(buffer);
                res += " ";
                if (_Step(buffer, step, nowEval, env, 0))
                {
                    res += step;
                    _IntertokenSpace(buffer);
                    res += " ";
                    if (stringOR(buffer, now, {")"}))
                    {
                        res += now;
                        s = buffer;
                        if (exec)
                        {
                            _Init(initCode, now, nowEval, env, 1);
                            env.addVar(varName);
                            env.setVar(varName, nowEval);
                        }
                        else
                            step = "";
                        return 1;
                    }
                }
            }
        }
    }

    // (<variable> <init>)
    buffer = s;
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (_Variable(buffer, varName))
        {
            res += varName;
            _IntertokenSpace(buffer);
            res += " ";
            if (_Init(buffer, initCode, nowEval, env, 0))
            {
                res += initCode;
                _IntertokenSpace(buffer);
                res += " ";
                if (stringOR(buffer, now, {")"}))
                {
                    res += now;
                    s = buffer;
                    step = "";
                    if (exec)
                    {
                        _Init(initCode, now, nowEval, env, 1);
                        env.addVar(varName);
                        env.setVar(varName, nowEval);
                    }
                    return 1;
                }
            }
        }
    }

    res = "";
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_Init(string &s, string &res, var &eval, Environment &env, const int &exec)
{
    if (_Expression(s, res, eval, env, exec))
        return 1;

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_Step(string &s, string &res, var &eval, Environment &env, const int &exec)
{
    if (_Expression(s, res, eval, env, exec))
        return 1;

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_DoResult(string &s, string &res, var &eval, Environment &env, const int &exec)
{
    if (_Expression(s, res, eval, env, exec))
        return 1;

    res = "";
    eval = var();
    return 1;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_MacroUse(string &s, string &res)
{
    res = "";
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_Keyword(string &s, string &res)
{
    if (_Identifier(s, res))
        return 1;

    res = "";
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_MacroBlock(string &s, string &res)
{
    res = "";
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_SyntaxSpec(string &s, string &res)
{
    res = "";
    return 0;
}

//Programs and definitions

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// The function is different from _Program since it will print out the evaluation of each command or definition.
/// For more details, please refer to R5RS.
int Interpreter::_ProgramWithOutput(string &s, string &res, var &eval, Environment &env, const int &exec)
{
    string now;
    var nowEval;

    res = "";
    eval = var();
    for (; _CommandOrDefinition(s, now, nowEval, env, exec); eval = nowEval)
    {
        if (*(nowEval.tid) != typeid(Empty).name())
            cout << nowEval.getStr() << endl;
        res += now;
        _IntertokenSpace(s);
        res += " ";
    }

    return 1;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_Program(string &s, string &res, var &eval, Environment &env, const int &exec)
{
    _IntertokenSpace(s);

    string now;
    var nowEval;

    res = "";
    eval = var();
    for (; _CommandOrDefinition(s, now, nowEval, env, exec); eval = nowEval)
    {
        res += now;
        _IntertokenSpace(s);
        res += " ";
    }

    return 1;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_CommandOrDefinition(string &s, string &res, var &eval, Environment &env, const int &exec)
{
    _IntertokenSpace(s);

    string buffer, now;
    var nowEval;
    Environment nowEnv;
    vector<string> CODList;

    // (begin <command or definition>+)
    buffer = s;
    nowEnv = env;
    CODList.clear();
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (stringOR(buffer, now, {"begin"}))
        {
            res += now;
            _IntertokenSpace(buffer);
            res += " ";
            if (_CommandOrDefinition(buffer, now, nowEval, nowEnv, 0))
            {
                res += now;
                _IntertokenSpace(buffer);
                res += " ";
                CODList.push_back(now);
                for (; _CommandOrDefinition(buffer, now, nowEval, nowEnv, 0); CODList.push_back(now))
                {
                    res += now;
                    _IntertokenSpace(buffer);
                    res += " ";
                }
                if (stringOR(buffer, now, {")"}))
                {
                    res += now;
                    s = buffer;
                    if (exec)
                    {
                        for (auto iter = CODList.begin(); iter != CODList.end(); ++iter)
                            _CommandOrDefinition(*iter, now, eval, nowEnv, 1);
                    }
                    else
                        eval = var();
                    return 1;
                }
            }
        }
    }

    // <command>
    if (_Command(s, res, eval, env, exec))
        return 1;

    // <definition>
    if (_Definition(s, res, env, exec))
    {
        eval = var();
        return 1;
    }

    // <syntax definition>
    if (_SyntaxDefinition(s, res, eval, env, exec))
        return 1;

    res = "";
    eval = var();
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_Definition(string &s, string &res, Environment &env, const int &exec)
{
    _IntertokenSpace(s);

    string buffer, now, varName, bodyCode, defCode, expCode;
    var nowEval;
    Environment nowEnv;
    vector<string> defList;

    // (define (<variable> <def formals>) <body>)
    buffer = s;
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (stringOR(buffer, now, {"define"}))
        {
            res += now;
            _IntertokenSpace(buffer);
            res += " ";
            if (stringOR(buffer, now, {"("}))
            {
                res += now;
                _IntertokenSpace(buffer);
                res += " ";
                if (_Variable(buffer, varName))
                {
                    res += varName;
                    _IntertokenSpace(buffer);
                    res += " ";
                    if (_DefFormals(buffer, defCode, nowEval, env, 0))
                    {
                        res += defCode;
                        _IntertokenSpace(buffer);
                        res += " ";
                        if (stringOR(buffer, now, {")"}))
                        {
                            res += now;
                            _IntertokenSpace(buffer);
                            res += " ";
                            if (_Body(buffer, bodyCode, nowEval, env, 0))
                            {
                                res += bodyCode;
                                _IntertokenSpace(buffer);
                                res += " ";
                                if (stringOR(buffer, now, {")"}))
                                {
                                    res += now;
                                    s = buffer;
                                    if (exec)
                                    {
                                        _DefFormals(defCode, now, nowEval, env, 1);
                                        nowEval.setCode(bodyCode);
                                        if (env.varList.find(varName) == env.varList.end())
                                            env.addVar(varName);
                                        env.setVar(varName, nowEval);
                                    }
                                    return 1;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    // (define <variable> <expression>)
    buffer = s;
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (stringOR(buffer, now, {"define"}))
        {
            res += now;
            _IntertokenSpace(buffer);
            res += " ";
            if (_Variable(buffer, varName))
            {
                res += varName;
                _IntertokenSpace(buffer);
                res += " ";
                if (_Expression(buffer, expCode, nowEval, env, 0))
                {
                    res += expCode;
                    _IntertokenSpace(buffer);
                    res += " ";
                    if (stringOR(buffer, now, {")"}))
                    {
                        res += now;
                        s = buffer;
                        if (exec)
                        {
                            _Expression(expCode, now, nowEval, env, 1);
                            if (env.varList.find(varName) == env.varList.end())
                                env.addVar(varName);
                            env.setVar(varName, nowEval);
                        }
                        return 1;
                    }
                }
            }
        }
    }

    // (begin <definition>*)
    buffer = s;
    defList.clear();
    if (stringOR(buffer, res, {"("}))
    {
        _IntertokenSpace(buffer);
        res += " ";
        if (stringOR(buffer, now, {"begin"}))
        {
            res += now;
            _IntertokenSpace(buffer);
            res += " ";
            for (; _Definition(buffer, now, env, 0); defList.push_back(now))
            {
                res += now;
                _IntertokenSpace(buffer);
                res += " ";
            }
            if (stringOR(buffer, now, {")"}))
            {
                res += now;
                s = buffer;
                if (exec)
                    for (auto iter = defList.begin(); iter != defList.end(); ++iter)
                        _Definition(*iter, now, env, 1);
                return 1;
            }
        }
    }

    res = "";
    return 0;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// It will add the variables to the arguments list of "eval".
/// For more details, please refer to R5RS.
int Interpreter::_DefFormals(string &s, string &res, var &eval, Environment &env, const int &exec)
{
    _IntertokenSpace(s);

    string buffer, now, nowRes;
    var nowEval;

    buffer = s;
    res = "";
    eval = var(Proc(env));
    while (_Variable(buffer, now))
    {
        res += now;
        _IntertokenSpace(buffer);
        res += " ";
        if (exec)
            eval.addArg(now);
    }

    s = buffer;
    nowRes = res;

    _IntertokenSpace(buffer);
    nowRes += " ";
    if (stringOR(buffer, now, {"."}))
    {
        nowRes += now;
        _IntertokenSpace(buffer);
        nowRes += " ";
        if (_Variable(buffer, now))
        {
            res = nowRes += now;
            s = buffer;
            if (exec)
                eval.setListArg(now);
        }
    }

    if (! exec)
        eval = var();

    return 1;
}

///@brief Formal syntax matching function.

/// Matching a symbol in the formal syntax defined in R5RS and trying to read it out.
/// If exec = 1, the code matched will be executed.
/// For more details, please refer to R5RS.
int Interpreter::_SyntaxDefinition(string &s, string &res, var &eval, Environment &env, const int &exec)
{
    _IntertokenSpace(s);

    res = "";
    eval = var();
    return 0;
}

