#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <ctime>
#include <cassert>
#include <cctype>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <string>
#include <vector>
#include <bitset>
#include <set>
#include <map>
#include <queue>
#include <list>
#include <typeinfo>
#include <limits>

#include <gmp.h>
#include <gmpxx.h>
#include <mpfr.h>
#include <mpc.h>

#include "Constant.h"
#include "scmType.h"
#include "Exception.h"
#include "Environment.h"

using namespace std;

//class RtlComplex

///@brief Default constructor.

/// Set the value to 0.
RtlComplex::RtlComplex():re(), im() {}

///@brief Constructor.

/// Construct from a rational number.
RtlComplex::RtlComplex(const mpq_class &_re):re(_re), im() {}

///@brief Constructor.

/// Construct with real part and imaginary part.
/// Both parts should be mpq_class.
RtlComplex::RtlComplex(const mpq_class &_re, const mpq_class &_im):re(_re), im(_im) {}

///@brief Copy constructor.
RtlComplex::RtlComplex(const RtlComplex &b):re(b.re), im(b.im) {}

///@brief Assignment.
RtlComplex RtlComplex::operator =(const RtlComplex &b)
{
    re = b.re;
    im = b.im;
    return *this;
}

///@brief Plus.
RtlComplex RtlComplex::operator +(const RtlComplex &b) const
{
    return RtlComplex(re+b.re, im+b.im);
}

///@brief Minus.
RtlComplex RtlComplex::operator -(const RtlComplex &b) const
{
    return RtlComplex(re-b.re, im-b.im);
}

///@brief Multiplication.
RtlComplex RtlComplex::operator *(const RtlComplex &b) const
{
    return RtlComplex(re*b.re-im*b.im, re*b.im+im*b.re);
}

///@brief Division.
RtlComplex RtlComplex::operator /(const RtlComplex &b) const
{
    return RtlComplex((re*b.re+im*b.im)/b.norm(), (-re*b.im+im*b.re)/b.norm());
}

///@brief Get the conjugation.
RtlComplex RtlComplex::conjugate() const
{
    return RtlComplex(re, -im);
}

///@brief Equal to a mpq_class.
bool RtlComplex::operator ==(const mpq_class &b) const
{
    return re == b && im == 0;
}

///@brief Equal to a mpf_class.
bool RtlComplex::operator ==(const mpf_class &b) const
{
    return re == b && im == 0;
}

///@brief Equal to a RtlComplex.
bool RtlComplex::operator ==(const RtlComplex &b) const
{
    return re == b.re && im == b.im;
}

///@brief Equal to a Complex.
bool RtlComplex::operator ==(const Complex &b) const
{
    return re == b.re && im == b.im;
}

///@brief Inequal to a mpq_class.
bool RtlComplex::operator !=(const mpq_class &b) const
{
    return re != b || im != 0;
}

///@brief Inequal to a mpf_class.
bool RtlComplex::operator !=(const mpf_class &b) const
{
    return re != b || im != 0;
}

///@brief Inequal to a RtlComplex.
bool RtlComplex::operator !=(const RtlComplex &b) const
{
    return re != b.re || im != b.im;
}

///@brief Inequal to a Complex.
bool RtlComplex::operator !=(const Complex &b) const
{
    return re != b.re || im != b.im;
}

///@brief Get the norm.
mpq_class RtlComplex::norm() const
{
    return re*re+im*im;
}

///@brief Get the absolute value.
mpf_class RtlComplex::abs() const
{
    return sqrt(mpf_class(re*re+im*im));
}

///@brief Get the string representation.
string RtlComplex::getStr() const
{
    ostringstream buffer;
    buffer << re;
    if (im != 0)
    {
        if (im > 0)
            buffer << "+";
        buffer << im << "i";
    }
    return buffer.str();
}

// class Complex

///@brief Default constructure.

/// Set the value to 0.
Complex::Complex():re(), im() {}

///@brief Constructor.

/// Constructure from mpq_class.
Complex::Complex(const mpq_class &_re):re(_re), im() {}

///@brief Constructor.

/// Constructure from mpf_class.
Complex::Complex(const mpf_class &_re):re(_re), im() {}

///@brief Constructor.

/// Constructure with real part and imaginary part.
/// Both parts should be mpf_class.
Complex::Complex(const mpf_class &_re, const mpf_class &_im):re(_re), im(_im) {}

///@brief Constructor.

///@brief Constructure from RtlComplex.
Complex::Complex(const RtlComplex &_val):re(_val.re), im(_val.im) {}

///@brief Copy constructor.
Complex::Complex(const Complex &b):re(b.re), im(b.im) {}

///@brief Assignment.
Complex Complex::operator =(const Complex &b)
{
    re = b.re;
    im = b.im;
    return *this;
}

///@brief Plus.
Complex Complex::operator +(const Complex &b) const
{
    return Complex(re+b.re, im+b.im);
}

///@brief Minus.
Complex Complex::operator -(const Complex &b) const
{
    return Complex(re-b.re, im-b.im);
}

///@brief Multiplication.
Complex Complex::operator *(const Complex &b) const
{
    return Complex(re*b.re-im*b.im, re*b.im+im*b.re);
}

///@brief Division.
Complex Complex::operator /(const Complex &b) const
{
    return Complex((re*b.re+im*b.im)/b.norm(), (-re*b.im+im*b.re)/b.norm());
}

///@brief Get the conjugation.
Complex Complex::conjugate() const
{
    return Complex(re, -im);
}

///@brief Equal to mpq_class.
bool Complex::operator ==(const mpq_class &b) const
{
    return re == b && im == 0;
}

///@brief Equal to mpf_class.
bool Complex::operator ==(const mpf_class &b) const
{
    return re == b && im == 0;
}

///@brief Equal to RtlComplex.
bool Complex::operator ==(const RtlComplex &b) const
{
    return re == b.re && im == b.im;
}

///@brief Equal to Complex.
bool Complex::operator ==(const Complex &b) const
{
    return re == b.re && im == b.im;
}

///@brief Inequal to mpq_class.
bool Complex::operator !=(const mpq_class &b) const
{
    return re != b || im != 0;
}

///@brief Inequal to mpf_class.
bool Complex::operator !=(const mpf_class &b) const
{
    return re != b || im != 0;
}

///@brief Inequal to RtlComplex.
bool Complex::operator !=(const RtlComplex &b) const
{
    return re != b.re || im != b.im;
}

///@brief Inequal to Complex.
bool Complex::operator !=(const Complex &b) const
{
    return re != b.re || im != b.im;
}

///@brief Get the norm.
mpf_class Complex::norm() const
{
    return re*re+im*im;
}

///@brief Get the absolute value.
mpf_class Complex::abs() const
{
    return sqrt(re*re+im*im);
}

///@brief Get the string representation.
string Complex::getStr() const
{
    ostringstream buffer;
    buffer << re;
    if (im != 0)
    {
        if (im > 0)
            buffer << "+";
        buffer << im << "i";
    }
    return buffer.str();
}

//class Symbol

///@brief Default constructor.

/// Construct with empty code.
Symbol::Symbol():code("") {}

///@brief Constructor.

/// Construct with the code.
Symbol::Symbol(const string &_code):code(_code) {}

///@brief Copy constructor.
Symbol::Symbol(const Symbol &b):code(b.code) {}

///@brief Assignment.
Symbol Symbol::operator =(const Symbol &b)
{
    code = b.code;
}

//class Proc

///@brief Default constructor.

/// No argument. Empty code and environment.
Proc::Proc():buildin(0), arg(), listArg(""), code(""), env(Environment()) {}

///@brief Constructor.

/// Construct with a specific environment.
Proc::Proc(const Environment &_env):buildin(0), arg(), listArg(""), code(""), env(_env) {}

///@brief Constructor.

/// Construct with code and a specific environment.
Proc::Proc(const string &_code, const Environment &_env):buildin(0), arg(), listArg(""), code(_code), env(_env) {}

///@brief Constructor.

/// Construct a build-in procedure with code and a specific environment.
Proc::Proc(const int &_buildin, const string &_code, const Environment &_env):buildin(_buildin), arg(), listArg(""), code(_code), env(_env) {}

///@brief Copy Constructor.
Proc::Proc(const Proc &b):buildin(b.buildin), arg(b.arg), listArg(b.listArg), code(b.code), env(b.env) {}

///@brief Assignment.
Proc Proc::operator =(const Proc &b)
{
    buildin = b.buildin;
    arg = b.arg;
    listArg = b.listArg;
    code = b.code;
    env = b.env;
    return *this;
}

///@brief Add a argument to the formal arguments list.
void Proc::addArg(const string &argName)
{
    for (auto iter = arg.begin(); iter != arg.end(); ++iter)
        if (*iter == argName)
            throw Exception("Error: Duplicated name for arguments");

    arg.push_back(argName);
    env.addVar(argName);
}

///@brief Set the list-argument for the unlimited arguments list.
void Proc::setListArg(const string &argName)
{
    listArg = argName;
    env.addVar(argName);
}

//class var

///@brief Default constructor.

/// Construct with Empty and set the tag to 0.
var::var():val(new Empty), opt(new int(0)), tid(new string(typeid(Empty).name())), ref(new int(1)) {}

///@brief Constructor.

/// Construct with Empty and the tag. Similar to the default constructor.
var::var(const Empty &_val, const int &_opt):val((void*)new Empty(_val)), opt(new int(_opt)), tid(new string(typeid(Empty).name())), ref(new int(1)) {}

///@brief Constructor.

/// Construct with EmptyList and the tag.
var::var(const EmptyList &_val, const int &_opt):val((void*)new EmptyList(_val)), opt(new int(_opt)), tid(new string(typeid(EmptyList).name())), ref(new int(1)) {}

///@brief Constructor.

/// Construct with bool and the tag.
var::var(const bool &_val, const int &_opt):val((void*)new bool(_val)), opt(new int(_opt)), tid(new string(typeid(bool).name())), ref(new int(1)) {}

///@brief Constructor.

/// Construct with mpq_class and the tag.
var::var(const mpq_class &_val, const int &_opt):val((void*)new mpq_class(_val)), opt(new int(_opt)), tid(new string(typeid(mpq_class).name())), ref(new int(1)) {}

///@brief Constructor.

/// Construct with mpf_class and the tag.
var::var(const mpf_class &_val, const int &_opt):val((void*)new mpf_class(_val)), opt(new int(_opt)), tid(new string(typeid(mpf_class).name())), ref(new int(1)) {}

///@brief Constructor.

/// Construct with RtlComplex and the tag.
var::var(const RtlComplex &_val, const int &_opt):val((void*)new RtlComplex(_val)), opt(new int(_opt)), tid(new string(typeid(RtlComplex).name())), ref(new int(1)) {}

///@brief Constructor.

/// Construct with Complex and the tag.
var::var(const Complex &_val, const int &_opt):val((void*)new Complex(_val)), opt(new int(_opt)), tid(new string(typeid(Complex).name())), ref(new int(1)) {}

///@brief Constructor.

/// Construct with char and the tag.
var::var(const char &_val, const int &_opt):val((void*)new char(_val)), opt(new int(_opt)), tid(new string(typeid(char).name())), ref(new int(1)) {}

///@brief Constructor.

/// Construct with string and the tag.
var::var(const string &_val, const int &_opt):val((void*)new string(_val)), opt(new int(_opt)), tid(new string(typeid(string).name())), ref(new int(1)) {}

///@brief Constructor.

/// Construct with Pair and the tag.
var::var(const Pair &_val, const int &_opt):val((void*)new Pair(_val)), opt(new int(_opt)), tid(new string(typeid(Pair).name())), ref(new int(1)) {}

///@brief Constructor.

/// Construct with Vector and the tag.
var::var(const Vector &_val, const int &_opt):val((void*)new Vector(_val)), opt(new int(_opt)), tid(new string(typeid(Vector).name())), ref(new int(1)) {}

///@brief Constructor.

/// Construct with Symbol and the tag.
var::var(const Symbol &_val, const int &_opt):val((void*)new Symbol(_val)), opt(new int(_opt)), tid(new string(typeid(Symbol).name())), ref(new int(1)) {}

///@brief Constructor.

/// Construct with Proc and the tag.
var::var(const Proc &_val, const int &_opt):val((void*)new Proc(_val)), opt(new int(_opt)), tid(new string(typeid(Proc).name())), ref(new int(1)) {}

///@brief Copy constructor.

/// Increase to the reference counter.
var::var(const var &b):val(b.val), opt(b.opt), tid(b.tid), ref(b.ref)
{
    ++(*ref);
}

///@brief Assignment.

/// Cut down the reference relationship with the initial object and set it up to the specific one.
/// Decrease to the reference counter of initial one and destruct it if necessary.
/// Increase to the reference counter of specific one.
var & var::operator =(const var &b)
{
    if (! --(*ref))
    {
        typeDelete(Empty);
        typeDelete(EmptyList);
        typeDelete(bool);
        typeDelete(mpq_class);
        typeDelete(mpf_class);
        typeDelete(RtlComplex);
        typeDelete(Complex);
        typeDelete(char);
        typeDelete(string);
        typeDelete(Pair);
        typeDelete(Vector);
        typeDelete(Symbol);
        typeDelete(Proc);
        delete opt;
        delete tid;
        delete ref;
    }

    val = b.val;
    opt = b.opt;
    tid = b.tid;
    ref = b.ref;
    ++(*ref);
    return *this;
}

///@brief Desturctor.

/// Decrease the reference counter and delete all the pointer inside if there is no reference of it.
var::~var()
{
    if (! --(*ref))
    {
        typeDelete(Empty);
        typeDelete(EmptyList);
        typeDelete(bool);
        typeDelete(mpq_class);
        typeDelete(mpf_class);
        typeDelete(RtlComplex);
        typeDelete(Complex);
        typeDelete(char);
        typeDelete(string);
        typeDelete(Pair);
        typeDelete(Vector);
        typeDelete(Symbol);
        typeDelete(Proc);
        delete opt;
        delete tid;
        delete ref;
    }
}

///@brief Copy Function.

/// Return an independent copy.
var var::copy() const
{
    typeCopy(Empty);
    typeCopy(EmptyList);
    typeCopy(bool);
    typeCopy(mpq_class);
    typeCopy(mpf_class);
    typeCopy(RtlComplex);
    typeCopy(Complex);
    typeCopy(char);
    typeCopy(string);
    typeCopy(Pair);
    typeCopy(Vector);
    typeCopy(Symbol);
    typeCopy(Proc);
}

///@brief Conversion.

/// Convert the weak type object to Empty.
var::operator Empty() const
{
    if (*tid != typeid(Empty).name())
        return *(Empty *)val;

    throw Exception::errorConvert(typeName(), var().typeName());
}

///@brief Conversion.

/// Convert the weak type object to EmptyList.
var::operator EmptyList() const
{
    if (*tid == typeid(EmptyList).name())
        return *(EmptyList *)val;

    throw Exception::errorConvert(typeName(), var(EmptyList()).typeName());
}

///@brief Conversion.

/// Convert the weak type object to bool.
var::operator bool() const
{
    if (*tid == typeid(bool).name())
        if (! *(bool *)val)
            return 0;
    return 1;
}

///@brief Conversion.

/// Convert the weak type object to mpq_class.
var::operator mpq_class() const
{
    if (*tid == typeid(mpq_class).name())
        return *(mpq_class *)val;

    throw Exception::errorConvert(typeName(), var(mpq_class()).typeName());
}

///@brief Conversion.

/// Convert the weak type object to mpf_class.
var::operator mpf_class() const
{
    if (*tid == typeid(mpq_class).name())
        return mpf_class(*(mpq_class *)val);
    if (*tid == typeid(mpf_class).name())
        return *(mpf_class *)val;

    throw Exception::errorConvert(typeName(), var(mpf_class()).typeName());
}

///@brief Conversion.

/// Convert the weak type object to RtlComplex.
var::operator RtlComplex() const
{
    if (*tid == typeid(mpq_class).name())
        return RtlComplex(*(mpq_class *)val);
    if (*tid == typeid(RtlComplex).name())
        return *(RtlComplex *)val;

    throw Exception::errorConvert(typeName(), var(RtlComplex()).typeName());
}

///@brief Conversion.

/// Convert the weak type object to Complex.
var::operator Complex() const
{
    if (*tid == typeid(mpq_class).name())
        return Complex(*(mpq_class *)val);
    if (*tid == typeid(mpf_class).name())
        return Complex(*(mpf_class *)val);
    if (*tid == typeid(RtlComplex).name())
        return Complex(*(RtlComplex *)val);
    if (*tid == typeid(Complex).name())
        return *(Complex *)val;

    throw Exception::errorConvert(typeName(), var(Complex()).typeName());
}

///@brief Conversion.

/// Convert the weak type object to char.
var::operator char() const
{
    if (*tid == typeid(char).name())
        return *(char *)val;

    throw Exception::errorConvert(typeName(), var(char()).typeName());
}

///@brief Conversion.

/// Convert the weak type object to string.
var::operator string() const
{
    if (*tid == typeid(string).name())
        return *(string *)val;

    throw Exception::errorConvert(typeName(), var(string()).typeName());
}

///@brief Conversion.

/// Convert the weak type object to Pair.
var::operator Pair() const
{
    if (*tid == typeid(Pair).name())
        return *(Pair *)val;

    throw Exception::errorConvert(typeName(), var(Pair()).typeName());
}

///@brief Conversion.

/// Convert the weak type object to Vector.
var::operator Vector() const
{
    if (*tid == typeid(Vector).name())
        return *(Vector *)val;

    throw Exception::errorConvert(typeName(), var(Vector()).typeName());
}

///@brief Conversion.

/// Convert the weak type object to Symbol.
var::operator Symbol() const
{
    if (*tid == typeid(Symbol).name())
        return *(Symbol *)val;

    throw Exception::errorConvert(typeName(), var(Symbol()).typeName());
}

///@brief Conversion.

/// Convert the weak type object to Proc.
var::operator Proc() const
{
    if (*tid == typeid(Proc).name())
        return *(Proc *)val;

    throw Exception::errorConvert(typeName(), var(Proc()).typeName());
}

///@brief Get the string representation.
string var::getStr(const int &style) const
{
    if (*tid == typeid(Empty).name())
        return "";

    if (*tid == typeid(EmptyList).name())
        return "()";

    if (*tid == typeid(bool).name())
        return *(bool *)val ? "#t" : "#f";

    if (*tid == typeid(mpq_class).name())
    {
        ostringstream buffer;
        buffer << *(mpq_class *)val;
        return buffer.str();
    }

    if (*tid == typeid(mpf_class).name())
    {
        ostringstream buffer;
        buffer << *(mpf_class *)val;
        return buffer.str();
    }

    if (*tid == typeid(RtlComplex).name())
        return ((RtlComplex *)val) -> getStr();

    if (*tid == typeid(Complex).name())
        return ((Complex *)val) -> getStr();

    if (*tid == typeid(char).name())
        if (style == NORMAL_REPRESENT_STYLE)
            return string("#\\")+(*(char *)val);
        else if (style == DISPLAY_REPRESENT_STYLE)
            return string("")+(*(char *)val);

    if (*tid == typeid(string).name())
    {
        string res = *(string *)val;
        for (int i = 0; res[i]; ++i)
            if (res[i] == '\\')
                if (res[i+1] == '\\' || res[i+1] == '\"')
                    res.erase(i, 1);
        if (style == NORMAL_REPRESENT_STYLE)
            return "\""+res+"\"";
        else if (style == DISPLAY_REPRESENT_STYLE)
            return res;
    }

    if (*tid == typeid(Pair).name())
    {
        ostringstream buffer;
        buffer << "(" << ((Pair *)val) -> first.getStr();
        var* ptr = &(((Pair *)val) -> second);
        for (; *(ptr -> tid) == typeid(Pair).name(); ptr = &(((Pair *)(ptr -> val)) -> second))
            buffer << " " << ((Pair *)(ptr -> val)) -> first.getStr();
        if (*(ptr -> tid) == typeid(EmptyList).name())
            buffer << ")";
        else
            buffer << " . " << ptr -> getStr() << ")";
        return buffer.str();
    }

    if (*tid == typeid(Vector).name())
    {
        ostringstream sout;
        sout << "#(";
        bool flag = 0;
        for (auto iter = ((Vector *)val) -> begin(); iter != ((Vector *)val) -> end(); ++iter)
        {
            if (flag)
                sout << " ";
            flag = 1;
            sout << iter -> getStr();
        }
        sout << ")";
        return sout.str();
    }

    if (*tid == typeid(Symbol).name())
        return ((Symbol *)val) -> code;

    if (*tid == typeid(Proc).name())
        return "#<procedure>";

    return "";
}

///@brief Get the type name generated by typeid().
string var::typeName() const
{
    if (*tid == typeid(Empty).name())
        return "empty";
    if (*tid == typeid(EmptyList).name())
        return "empty list";
    if (*tid == typeid(bool).name())
        return "boolean";
    if (*tid == typeid(mpq_class).name())
        return "rational number";
    if (*tid == typeid(mpf_class).name())
        return "float";
    if (*tid == typeid(RtlComplex).name())
        return "rational complex";
    if (*tid == typeid(Complex).name())
        return "complex";
    if (*tid == typeid(char).name())
        return "char";
    if (*tid == typeid(string).name())
        return "string";
    if (*tid == typeid(Pair).name())
        return "pair";
    if (*tid == typeid(Vector).name())
        return "vector";
    if (*tid == typeid(Symbol).name())
        return "symbol";
    if (*tid == typeid(Proc).name())
        return "procedure";

    return "unknown type";
}

///@brief Plus.
var var::operator +(const var &b)
{
    typeMatch(mpq_class, mpq_class, mpq_class, +);
    typeMatch(mpq_class, mpf_class, mpf_class, +);
    typeMatch(mpq_class, RtlComplex, RtlComplex, +);
    typeMatch(mpq_class, Complex, Complex, +);
    typeMatch(mpf_class, mpq_class, mpf_class, +);
    typeMatch(mpf_class, mpf_class, mpf_class, +);
    typeMatch(mpf_class, RtlComplex, Complex, +);
    typeMatch(mpf_class, Complex, Complex, +);
    typeMatch(RtlComplex, mpq_class, RtlComplex, +);
    typeMatch(RtlComplex, mpf_class, Complex, +);
    typeMatch(RtlComplex, RtlComplex, RtlComplex, +);
    typeMatch(RtlComplex, Complex, Complex, +);
    typeMatch(Complex, mpq_class, Complex, +);
    typeMatch(Complex, mpf_class, Complex, +);
    typeMatch(Complex, RtlComplex, Complex, +);
    typeMatch(Complex, Complex, Complex, +);

    typeMatch(string, string, string, +);

    throw Exception::errorOperator(typeName(), b.typeName(), "+");
}

///@brief Minus.
var var::operator -(const var &b)
{
    typeMatch(mpq_class, mpq_class, mpq_class, -);
    typeMatch(mpq_class, mpf_class, mpf_class, -);
    typeMatch(mpq_class, RtlComplex, RtlComplex, -);
    typeMatch(mpq_class, Complex, Complex, -);
    typeMatch(mpf_class, mpq_class, mpf_class, -);
    typeMatch(mpf_class, mpf_class, mpf_class, -);
    typeMatch(mpf_class, RtlComplex, Complex, -);
    typeMatch(mpf_class, Complex, Complex, -);
    typeMatch(RtlComplex, mpq_class, RtlComplex, -);
    typeMatch(RtlComplex, mpf_class, Complex, -);
    typeMatch(RtlComplex, RtlComplex, RtlComplex, -);
    typeMatch(RtlComplex, Complex, Complex, -);
    typeMatch(Complex, mpq_class, Complex, -);
    typeMatch(Complex, mpf_class, Complex, -);
    typeMatch(Complex, RtlComplex, Complex, -);
    typeMatch(Complex, Complex, Complex, -);

    throw Exception::errorOperator(typeName(), b.typeName(), "-");
}

///@brief Multiplication.
var var::operator *(const var &b)
{
    typeMatch(mpq_class, mpq_class, mpq_class, *);
    typeMatch(mpq_class, mpf_class, mpf_class, *);
    typeMatch(mpq_class, RtlComplex, RtlComplex, *);
    typeMatch(mpq_class, Complex, Complex, *);
    typeMatch(mpf_class, mpq_class, mpf_class, *);
    typeMatch(mpf_class, mpf_class, mpf_class, *);
    typeMatch(mpf_class, RtlComplex, Complex, *);
    typeMatch(mpf_class, Complex, Complex, *);
    typeMatch(RtlComplex, mpq_class, RtlComplex, *);
    typeMatch(RtlComplex, mpf_class, Complex, *);
    typeMatch(RtlComplex, RtlComplex, RtlComplex, *);
    typeMatch(RtlComplex, Complex, Complex, *);
    typeMatch(Complex, mpq_class, Complex, *);
    typeMatch(Complex, mpf_class, Complex, *);
    typeMatch(Complex, RtlComplex, Complex, *);
    typeMatch(Complex, Complex, Complex, *);

    throw Exception::errorOperator(typeName(), b.typeName(), "*");
}

///@brief Division.
var var::operator /(const var &b)
{
    typeMatch(mpq_class, mpq_class, mpq_class, /);
    typeMatch(mpq_class, mpf_class, mpf_class, /);
    typeMatch(mpq_class, RtlComplex, RtlComplex, /);
    typeMatch(mpq_class, Complex, Complex, /);
    typeMatch(mpf_class, mpq_class, mpf_class, /);
    typeMatch(mpf_class, mpf_class, mpf_class, /);
    typeMatch(mpf_class, RtlComplex, Complex, /);
    typeMatch(mpf_class, Complex, Complex, /);
    typeMatch(RtlComplex, mpq_class, RtlComplex, /);
    typeMatch(RtlComplex, mpf_class, Complex, /);
    typeMatch(RtlComplex, RtlComplex, RtlComplex, /);
    typeMatch(RtlComplex, Complex, Complex, /);
    typeMatch(Complex, mpq_class, Complex, /);
    typeMatch(Complex, mpf_class, Complex, /);
    typeMatch(Complex, RtlComplex, Complex, /);
    typeMatch(Complex, Complex, Complex, /);

    throw Exception::errorOperator(typeName(), b.typeName(), "/");
}

///@brief Modulo.

/// Only available for integers.
var var::operator %(const var &b)
{
    if (*tid == typeid(mpq_class).name() && *(b.tid) == typeid(mpq_class).name())
        return var((mpq_class)(operator mpq_class().get_num() % b.operator mpq_class().get_num()), (*opt) | (*(b.opt)));

    throw Exception::errorOperator(typeName(), b.typeName(), "%");
}

///@brief Get the conjugation.

/// Only available for complex number.
var var::conjugate() const
{
    if (*tid == typeid(RtlComplex).name())
        return var(RtlComplex(((RtlComplex*)val) -> re, -((RtlComplex*)val) -> im), *opt);
    if (*tid == typeid(Complex).name())
        return var(Complex(((Complex*)val) -> re, -((Complex*)val) -> im), *opt);

    throw Exception::errorOperator(typeName(), "conjugate()");
}

///@brief Less than.

///Only available for real number and string.
bool var::operator <(const var &b) const
{
    if (*tid == typeid(mpq_class).name() && *(b.tid) == typeid(mpq_class).name())
        return operator mpq_class() < b.operator mpq_class();
    if (*tid == typeid(mpq_class).name() && *(b.tid) == typeid(mpf_class).name())
        return operator mpq_class() < b.operator mpf_class();
    if (*tid == typeid(mpf_class).name() && *(b.tid) == typeid(mpq_class).name())
        return operator mpf_class() < b.operator mpq_class();
    if (*tid == typeid(mpf_class).name() && *(b.tid) == typeid(mpf_class).name())
        return operator mpf_class() < b.operator mpf_class();

    if (*tid == typeid(string).name() && *(b.tid) == typeid(string).name())
        return operator string() < b.operator string();

    throw Exception::errorOperator(typeName(), b.typeName(), "<");
}

///@brief Greater than.

///Only available for real number and string.
bool var::operator >(const var &b) const
{
    if (*tid == typeid(mpq_class).name() && *(b.tid) == typeid(mpq_class).name())
        return operator mpq_class() > b.operator mpq_class();
    if (*tid == typeid(mpq_class).name() && *(b.tid) == typeid(mpf_class).name())
        return operator mpq_class() > b.operator mpf_class();
    if (*tid == typeid(mpf_class).name() && *(b.tid) == typeid(mpq_class).name())
        return operator mpf_class() > b.operator mpq_class();
    if (*tid == typeid(mpf_class).name() && *(b.tid) == typeid(mpf_class).name())
        return operator mpf_class() > b.operator mpf_class();

    if (*tid == typeid(string).name() && *(b.tid) == typeid(string).name())
        return operator string() > b.operator string();

    throw Exception::errorOperator(typeName(), b.typeName(), ">");
}

///@brief Less than or equal to.

///Only available for real number and string.
bool var::operator <=(const var &b) const
{
    if (*tid == typeid(mpq_class).name() && *(b.tid) == typeid(mpq_class).name())
        return operator mpq_class() <= b.operator mpq_class();
    if (*tid == typeid(mpq_class).name() && *(b.tid) == typeid(mpf_class).name())
        return operator mpq_class() <= b.operator mpf_class();
    if (*tid == typeid(mpf_class).name() && *(b.tid) == typeid(mpq_class).name())
        return operator mpf_class() <= b.operator mpq_class();
    if (*tid == typeid(mpf_class).name() && *(b.tid) == typeid(mpf_class).name())
        return operator mpf_class() <= b.operator mpf_class();

    if (*tid == typeid(string).name() && *(b.tid) == typeid(string).name())
        return operator string() <= b.operator string();

    throw Exception::errorOperator(typeName(), b.typeName(), "<=");
}

///@brief Greater than or equal to.

///Only available for real number and string.
bool var::operator >=(const var &b) const
{
    if (*tid == typeid(mpq_class).name() && *(b.tid) == typeid(mpq_class).name())
        return operator mpq_class() >= b.operator mpq_class();
    if (*tid == typeid(mpq_class).name() && *(b.tid) == typeid(mpf_class).name())
        return operator mpq_class() >= b.operator mpf_class();
    if (*tid == typeid(mpf_class).name() && *(b.tid) == typeid(mpq_class).name())
        return operator mpf_class() >= b.operator mpq_class();
    if (*tid == typeid(mpf_class).name() && *(b.tid) == typeid(mpf_class).name())
        return operator mpf_class() >= b.operator mpf_class();

    if (*tid == typeid(string).name() && *(b.tid) == typeid(string).name())
        return operator string() >= b.operator string();

    throw Exception::errorOperator(typeName(), b.typeName(), ">=");
}

///@brief Equal to.

/// Compare recursively.
bool var::operator ==(const var &b) const
{
    if (*tid == typeid(Empty).name() && *(b.tid) == typeid(Empty).name())
        return 1;

    if (*tid == typeid(EmptyList).name() && *(b.tid) == typeid(EmptyList).name())
        return 1;

    if (*tid == typeid(bool).name() && *(b.tid) == typeid(bool).name())
        return operator bool() == b.operator bool();

    if (*tid == typeid(mpq_class).name() && *(b.tid) == typeid(mpq_class).name())
        return *opt == *(b.opt) && operator mpq_class() == b.operator mpq_class();
    if (*tid == typeid(mpq_class).name() && *(b.tid) == typeid(mpf_class).name())
        return *opt == *(b.opt) && operator mpq_class() == b.operator mpf_class();
    if (*tid == typeid(mpq_class).name() && *(b.tid) == typeid(RtlComplex).name())
        return *opt == *(b.opt) && b.operator RtlComplex() == operator mpq_class();
    if (*tid == typeid(mpq_class).name() && *(b.tid) == typeid(Complex).name())
        return *opt == *(b.opt) && b.operator Complex() == operator mpq_class();

    if (*tid == typeid(mpf_class).name() && *(b.tid) == typeid(mpq_class).name())
        return *opt == *(b.opt) && operator mpf_class() == b.operator mpq_class();
    if (*tid == typeid(mpf_class).name() && *(b.tid) == typeid(mpf_class).name())
        return *opt == *(b.opt) && operator mpf_class() == b.operator mpf_class();
    if (*tid == typeid(mpf_class).name() && *(b.tid) == typeid(RtlComplex).name())
        return *opt == *(b.opt) && b.operator RtlComplex() == operator mpf_class();
    if (*tid == typeid(mpf_class).name() && *(b.tid) == typeid(Complex).name())
        return *opt == *(b.opt) && b.operator Complex() == operator mpf_class();

    if (*tid == typeid(RtlComplex).name() && *(b.tid) == typeid(mpq_class).name())
        return *opt == *(b.opt) && operator RtlComplex() == b.operator mpq_class();
    if (*tid == typeid(RtlComplex).name() && *(b.tid) == typeid(mpf_class).name())
        return *opt == *(b.opt) && operator RtlComplex() == b.operator mpf_class();
    if (*tid == typeid(RtlComplex).name() && *(b.tid) == typeid(RtlComplex).name())
        return *opt == *(b.opt) && operator RtlComplex() == b.operator RtlComplex();
    if (*tid == typeid(RtlComplex).name() && *(b.tid) == typeid(Complex).name())
        return *opt == *(b.opt) && operator RtlComplex() == b.operator Complex();

    if (*tid == typeid(Complex).name() && *(b.tid) == typeid(mpq_class).name())
        return *opt == *(b.opt) && operator Complex() == b.operator mpq_class();
    if (*tid == typeid(Complex).name() && *(b.tid) == typeid(mpf_class).name())
        return *opt == *(b.opt) && operator Complex() == b.operator mpf_class();
    if (*tid == typeid(Complex).name() && *(b.tid) == typeid(RtlComplex).name())
        return *opt == *(b.opt) && operator Complex() == b.operator RtlComplex();
    if (*tid == typeid(Complex).name() && *(b.tid) == typeid(Complex).name())
        return *opt == *(b.opt) && operator Complex() == b.operator Complex();

    if (*tid == typeid(char).name() && *(b.tid) == typeid(char).name())
        return operator char() == b.operator char();

    if (*tid == typeid(string).name() && *(b.tid) == typeid(string).name())
        return operator string() == b.operator string();

    if (*tid == typeid(Pair).name() && *(b.tid) == typeid(Pair).name())
        return val == b.val;

    if (*tid == typeid(Vector).name() && *(b.tid) == typeid(Vector).name())
        return val == b.val;

    if (*tid == typeid(Symbol).name() && *(b.tid) == typeid(Symbol).name())
        return operator Symbol().code == b.operator Symbol().code;

    if (*tid == typeid(Proc).name() && *(b.tid) == typeid(Proc).name())
        return val == b.val;

    return 0;
}

///@brief Inequal to.

/// Compare recursively.
bool var::operator !=(const var &b) const
{
    if (*tid == typeid(Empty).name() && *(b.tid) == typeid(Empty).name())
        return 0;

    if (*tid == typeid(EmptyList).name() && *(b.tid) == typeid(EmptyList).name())
        return 0;

    if (*tid == typeid(bool).name() && *(b.tid) == typeid(bool).name())
        return operator bool() != b.operator bool();

    if (*tid == typeid(mpq_class).name() && *(b.tid) == typeid(mpq_class).name())
        return *opt != *(b.opt) || operator mpq_class() != b.operator mpq_class();
    if (*tid == typeid(mpq_class).name() && *(b.tid) == typeid(mpf_class).name())
        return *opt != *(b.opt) || operator mpq_class() != b.operator mpf_class();
    if (*tid == typeid(mpq_class).name() && *(b.tid) == typeid(RtlComplex).name())
        return *opt != *(b.opt) || b.operator RtlComplex() != operator mpq_class();
    if (*tid == typeid(mpq_class).name() && *(b.tid) == typeid(Complex).name())
        return *opt != *(b.opt) || b.operator Complex() != operator mpq_class();

    if (*tid == typeid(mpf_class).name() && *(b.tid) == typeid(mpq_class).name())
        return *opt != *(b.opt) || operator mpf_class() != b.operator mpq_class();
    if (*tid == typeid(mpf_class).name() && *(b.tid) == typeid(mpf_class).name())
        return *opt != *(b.opt) || operator mpf_class() != b.operator mpf_class();
    if (*tid == typeid(mpf_class).name() && *(b.tid) == typeid(RtlComplex).name())
        return *opt != *(b.opt) || b.operator RtlComplex() != operator mpf_class();
    if (*tid == typeid(mpf_class).name() && *(b.tid) == typeid(Complex).name())
        return *opt != *(b.opt) || b.operator Complex() != operator mpf_class();

    if (*tid == typeid(RtlComplex).name() && *(b.tid) == typeid(mpq_class).name())
        return *opt != *(b.opt) || operator RtlComplex() != b.operator mpq_class();
    if (*tid == typeid(RtlComplex).name() && *(b.tid) == typeid(mpf_class).name())
        return *opt != *(b.opt) || operator RtlComplex() != b.operator mpf_class();
    if (*tid == typeid(RtlComplex).name() && *(b.tid) == typeid(RtlComplex).name())
        return *opt != *(b.opt) || operator RtlComplex() != b.operator RtlComplex();
    if (*tid == typeid(RtlComplex).name() && *(b.tid) == typeid(Complex).name())
        return *opt != *(b.opt) || operator RtlComplex() != b.operator Complex();

    if (*tid == typeid(Complex).name() && *(b.tid) == typeid(mpq_class).name())
        return *opt != *(b.opt) || operator Complex() != b.operator mpq_class();
    if (*tid == typeid(Complex).name() && *(b.tid) == typeid(mpf_class).name())
        return *opt != *(b.opt) || operator Complex() != b.operator mpf_class();
    if (*tid == typeid(Complex).name() && *(b.tid) == typeid(RtlComplex).name())
        return *opt != *(b.opt) || operator Complex() != b.operator RtlComplex();
    if (*tid == typeid(Complex).name() && *(b.tid) == typeid(Complex).name())
        return *opt != *(b.opt) || operator Complex() != b.operator Complex();

    if (*tid == typeid(char).name() && *(b.tid) == typeid(char).name())
        return operator char() != b.operator char();

    if (*tid == typeid(string).name() && *(b.tid) == typeid(string).name())
        return operator string() != b.operator string();

    if (*tid == typeid(Pair).name() && *(b.tid) == typeid(Pair).name())
        return val != b.val;

    if (*tid == typeid(Vector).name() && *(b.tid) == typeid(Vector).name())
        return val != b.val;

    if (*tid == typeid(Symbol).name() && *(b.tid) == typeid(Symbol).name())
        return operator Symbol().code != b.operator Symbol().code;

    if (*tid == typeid(Proc).name() && *(b.tid) == typeid(Proc).name())
        return val != b.val;

    return 1;
}

///@brief Get the real part.

/// Only available for a complex number.
var var::getReal() const
{
    if (*tid == typeid(RtlComplex).name())
        return var(((RtlComplex *)val) -> re);
    if (*tid == typeid(Complex).name())
        return var(((Complex *)val) -> re);

    throw Exception::errorOperator(typeName(), "getReal()");
}

///@brief Get the imaginary part.

/// Only available for a complex number.
var var::getImag() const
{
    if (*tid == typeid(RtlComplex).name())
        return var(((RtlComplex *)val) -> im);
    if (*tid == typeid(Complex).name())
        return var(((Complex *)val) -> im);

    throw Exception::errorOperator(typeName(), "getImag()");
}

///@brief Add a argument to the formal arguments list.

/// Only available for a procedure.
void var::addArg(const string &s)
{
    if (*tid != typeid(Proc).name())
        throw Exception::errorOperator(typeName(), "addArg(const string &)");

    ((Proc *)val) -> addArg(s);
}

///@brief Set the list-argument for the unlimited arguments list.

/// Only available for a procedure.
void var::setListArg(const string &s)
{
    ((Proc *)val) -> setListArg(s);
}

///@brief Set the code of the procedure.

/// Only available for a procedure.
void var::setCode(const string &s)
{
    if (*tid != typeid(Proc).name())
        throw Exception::errorOperator(typeName(), "setCode(const string &)");

    ((Proc *)val) -> code = s;
}

///@brief Clear the vector.

/// Only available for a vector.
void var::vecClear()
{
    if (*tid != typeid(Vector).name())
        throw Exception::errorOperator(typeName(), "vecClear()");

    ((Vector *)val) -> clear();
}

///@brief Get the element at the specific index of the vector.

/// Only available for a vector.
var var::vecGet(const int &index)
{
    if (*tid != typeid(Vector).name())
        throw Exception::errorOperator(typeName(), "vecGet(const int &)");

    if (index < 0 || index >= ((Vector *)val) -> size())
        throw Exception::errorIndex(index);

    return (*(Vector *)val)[index];
}

///@brief Append a element at the back of the vector.

/// Only available for a vector.
void var::vecPushBack(const var &b)
{
    if (*tid != typeid(Vector).name())
        throw Exception::errorOperator(typeName(), "vecPushBack(const var &)");

    ((Vector *)val) -> push_back(b);
}

///@brief Set the first element of the pair to a specific one.

/// Only available for a pair.
void var::setFirst(const var &b)
{
    if (*tid != typeid(Pair).name())
        throw Exception::errorOperator(typeName(), "setFirst(const var &)");

    ((Pair *)val) -> first = b;
}

///@brief Set the second element of the pair to a specific one.

/// Only available for a pair.
void var::setSecond(const var &b)
{
    if (*tid != typeid(Pair).name())
        throw Exception::errorOperator(typeName(), "setSecond(const var &)");

    ((Pair *)val) -> second = b;
}

///@brief Append a element to the back of the list.

/// Only available for a list.
void var::append(const var &b)
{
    if (*tid != typeid(Pair).name())
        throw Exception::errorOperator(typeName(), "append(const var &)");

    if (*(((Pair *)val) -> second.tid) == typeid(Pair).name())
        ((Pair *)val) -> second.append(b);
    else if (*(((Pair *)val) -> second.tid) == typeid(EmptyList).name())
        ((Pair *)val) -> second = var(Pair(b, var(EmptyList())));
    else
        ((Pair *)val) -> second = var(Pair(((Pair *)val) -> second, b));
}

///@brief Append a list to the back of the list.

/// It will connect the two list instead of setting it as the last element.
/// Only available for a list.
void var::appendList(const var &b)
{
    if (*tid == typeid(EmptyList).name())
    {
        *this = b;
        return;
    }

    if (*tid == typeid(Pair).name())
    {
        if (*(b.tid) == typeid(EmptyList).name())
            return;

        var nowEval;

        if (*(((Pair *)val) -> second.tid) == typeid(Pair).name())
            ((Pair *)val) -> second.appendList(b);
        else if (*(((Pair *)val) -> second.tid) == typeid(EmptyList).name())
        {
            for (nowEval = b; *(nowEval.tid) == typeid(Pair).name(); nowEval = nowEval.operator Pair().second);
            if (*(nowEval.tid) == typeid(EmptyList).name())
                ((Pair *)val) -> second = b;
            else
                ((Pair *)val) -> second = b;
        }
        else
            ((Pair *)val) -> second = var(Pair(((Pair *)val) -> second, b));
        return;
    }

    throw Exception::errorOperator(typeName(), "appendList(const var &)");
}

///@brief Check the value of number.

/// Check whether the number is too large and should be convert to mpf_class, if allowed, for higher efficiency.
/// Only available for a real number.
bool var::floatRange() const
{
    return cmp(abs(operator mpf_class()), numeric_limits<double>::min()) < 0 || cmp(abs(operator mpf_class()), numeric_limits<double>::max()) > 0;
}

///@brief Sin function for class var.
var sin(const var &a)
{
    if (*(a.tid) == typeid(mpq_class).name() || *(a.tid) == typeid(mpf_class).name())
    {
        mpfr_t t;
        mpfr_init_set_f(t, a.operator mpf_class().get_mpf_t(), MPFR_RNDN);
        mpfr_sin(t, t, MPFR_RNDN);

        mpf_t r;
        mpf_init(r);
        mpfr_get_f(r, t, MPFR_RNDN);

        var res(mpf_class(r), 1);

        mpfr_clear(t);
        mpf_clear(r);
        return res;
    }
    if (*(a.tid) == typeid(RtlComplex).name() || *(a.tid) == typeid(Complex).name())
    {
        mpc_t t;
        mpc_init2(t, DEFAULT_FLOAT_PRECISION);
        mpc_set_f_f(t, a.getReal().operator mpf_class().get_mpf_t(), a.getImag().operator mpf_class().get_mpf_t(), MPC_RNDNN);
        mpc_sin(t, t, MPC_RNDNN);

        mpfr_t _r, _i;
        mpfr_init2(_r, DEFAULT_FLOAT_PRECISION);
        mpfr_init2(_i, DEFAULT_FLOAT_PRECISION);
        mpc_real(_r, t, MPFR_RNDN);
        mpc_imag(_i, t, MPFR_RNDN);

        mpf_t r, i;
        mpf_init(r);
        mpf_init(i);
        mpfr_get_f(r, _r, MPFR_RNDN);
        mpfr_get_f(i, _i, MPFR_RNDN);

        var res(Complex(mpf_class(r), mpf_class(i)), 1);

        mpc_clear(t);
        mpfr_clear(_r);
        mpfr_clear(_i);
        mpf_clear(r);
        mpf_clear(i);
        return res;
    }

    throw Exception::errorOperator(a.typeName(), "sin(const var &)");
}

///@brief Cos function for class var.
var cos(const var &a)
{
    if (*(a.tid) == typeid(mpq_class).name() || *(a.tid) == typeid(mpf_class).name())
    {
        mpfr_t t;
        mpfr_init_set_f(t, a.operator mpf_class().get_mpf_t(), MPFR_RNDN);
        mpfr_cos(t, t, MPFR_RNDN);

        mpf_t r;
        mpf_init(r);
        mpfr_get_f(r, t, MPFR_RNDN);

        var res(mpf_class(r), 1);

        mpfr_clear(t);
        mpf_clear(r);
        return res;
    }
    if (*(a.tid) == typeid(RtlComplex).name() || *(a.tid) == typeid(Complex).name())
    {
        mpc_t t;
        mpc_init2(t, DEFAULT_FLOAT_PRECISION);
        mpc_set_f_f(t, a.getReal().operator mpf_class().get_mpf_t(), a.getImag().operator mpf_class().get_mpf_t(), MPC_RNDNN);
        mpc_cos(t, t, MPC_RNDNN);

        mpfr_t _r, _i;
        mpfr_init2(_r, DEFAULT_FLOAT_PRECISION);
        mpfr_init2(_i, DEFAULT_FLOAT_PRECISION);
        mpc_real(_r, t, MPFR_RNDN);
        mpc_imag(_i, t, MPFR_RNDN);

        mpf_t r, i;
        mpf_init(r);
        mpf_init(i);
        mpfr_get_f(r, _r, MPFR_RNDN);
        mpfr_get_f(i, _i, MPFR_RNDN);

        var res(Complex(mpf_class(r), mpf_class(i)), 1);

        mpc_clear(t);
        mpfr_clear(_r);
        mpfr_clear(_i);
        mpf_clear(r);
        mpf_clear(i);
        return res;
    }

    throw Exception::errorOperator(a.typeName(), "cos(const var &)");
}

///@brief Tan function for class var.
var tan(const var &a)
{
    if (*(a.tid) == typeid(mpq_class).name() || *(a.tid) == typeid(mpf_class).name())
    {
        mpfr_t t;
        mpfr_init_set_f(t, a.operator mpf_class().get_mpf_t(), MPFR_RNDN);
        mpfr_tan(t, t, MPFR_RNDN);

        mpf_t r;
        mpf_init(r);
        mpfr_get_f(r, t, MPFR_RNDN);

        var res(mpf_class(r), 1);

        mpfr_clear(t);
        mpf_clear(r);
        return res;
    }
    if (*(a.tid) == typeid(RtlComplex).name() || *(a.tid) == typeid(Complex).name())
    {
        mpc_t t;
        mpc_init2(t, DEFAULT_FLOAT_PRECISION);
        mpc_set_f_f(t, a.getReal().operator mpf_class().get_mpf_t(), a.getImag().operator mpf_class().get_mpf_t(), MPC_RNDNN);
        mpc_tan(t, t, MPC_RNDNN);

        mpfr_t _r, _i;
        mpfr_init2(_r, DEFAULT_FLOAT_PRECISION);
        mpfr_init2(_i, DEFAULT_FLOAT_PRECISION);
        mpc_real(_r, t, MPFR_RNDN);
        mpc_imag(_i, t, MPFR_RNDN);

        mpf_t r, i;
        mpf_init(r);
        mpf_init(i);
        mpfr_get_f(r, _r, MPFR_RNDN);
        mpfr_get_f(i, _i, MPFR_RNDN);

        var res(Complex(mpf_class(r), mpf_class(i)), 1);

        mpc_clear(t);
        mpfr_clear(_r);
        mpfr_clear(_i);
        mpf_clear(r);
        mpf_clear(i);
        return res;
    }

    throw Exception::errorOperator(a.typeName(), "tan(const var &)");
}

///@brief Asin function for class var.
var asin(const var &a)
{
    if (*(a.tid) == typeid(mpq_class).name() || *(a.tid) == typeid(mpf_class).name())
    {
        mpfr_t t;
        mpfr_init_set_f(t, a.operator mpf_class().get_mpf_t(), MPFR_RNDN);
        mpfr_asin(t, t, MPFR_RNDN);

        mpf_t r;
        mpf_init(r);
        mpfr_get_f(r, t, MPFR_RNDN);

        var res(mpf_class(r), 1);

        mpfr_clear(t);
        mpf_clear(r);
        return res;
    }
    if (*(a.tid) == typeid(RtlComplex).name() || *(a.tid) == typeid(Complex).name())
    {
        mpc_t t;
        mpc_init2(t, DEFAULT_FLOAT_PRECISION);
        mpc_set_f_f(t, a.getReal().operator mpf_class().get_mpf_t(), a.getImag().operator mpf_class().get_mpf_t(), MPC_RNDNN);
        mpc_asin(t, t, MPC_RNDNN);

        mpfr_t _r, _i;
        mpfr_init2(_r, DEFAULT_FLOAT_PRECISION);
        mpfr_init2(_i, DEFAULT_FLOAT_PRECISION);
        mpc_real(_r, t, MPFR_RNDN);
        mpc_imag(_i, t, MPFR_RNDN);

        mpf_t r, i;
        mpf_init(r);
        mpf_init(i);
        mpfr_get_f(r, _r, MPFR_RNDN);
        mpfr_get_f(i, _i, MPFR_RNDN);

        var res(Complex(mpf_class(r), mpf_class(i)), 1);

        mpc_clear(t);
        mpfr_clear(_r);
        mpfr_clear(_i);
        mpf_clear(r);
        mpf_clear(i);
        return res;
    }

    throw Exception::errorOperator(a.typeName(), "asin(const var &)");
}

///@brief Acos function for class var.
var acos(const var &a)
{
    if (*(a.tid) == typeid(mpq_class).name() || *(a.tid) == typeid(mpf_class).name())
    {
        mpfr_t t;
        mpfr_init_set_f(t, a.operator mpf_class().get_mpf_t(), MPFR_RNDN);
        mpfr_acos(t, t, MPFR_RNDN);

        mpf_t r;
        mpf_init(r);
        mpfr_get_f(r, t, MPFR_RNDN);

        var res(mpf_class(r), 1);

        mpfr_clear(t);
        mpf_clear(r);
        return res;
    }
    if (*(a.tid) == typeid(RtlComplex).name() || *(a.tid) == typeid(Complex).name())
    {
        mpc_t t;
        mpc_init2(t, DEFAULT_FLOAT_PRECISION);
        mpc_set_f_f(t, a.getReal().operator mpf_class().get_mpf_t(), a.getImag().operator mpf_class().get_mpf_t(), MPC_RNDNN);
        mpc_acos(t, t, MPC_RNDNN);

        mpfr_t _r, _i;
        mpfr_init2(_r, DEFAULT_FLOAT_PRECISION);
        mpfr_init2(_i, DEFAULT_FLOAT_PRECISION);
        mpc_real(_r, t, MPFR_RNDN);
        mpc_imag(_i, t, MPFR_RNDN);

        mpf_t r, i;
        mpf_init(r);
        mpf_init(i);
        mpfr_get_f(r, _r, MPFR_RNDN);
        mpfr_get_f(i, _i, MPFR_RNDN);

        var res(Complex(mpf_class(r), mpf_class(i)), 1);

        mpc_clear(t);
        mpfr_clear(_r);
        mpfr_clear(_i);
        mpf_clear(r);
        mpf_clear(i);
        return res;
    }

    throw Exception::errorOperator(a.typeName(), "acos(const var &)");
}

///@brief Atan function for class var.
var atan(const var &a)
{
    if (*(a.tid) == typeid(mpq_class).name() || *(a.tid) == typeid(mpf_class).name())
    {
        mpfr_t t;
        mpfr_init_set_f(t, a.operator mpf_class().get_mpf_t(), MPFR_RNDN);
        mpfr_atan(t, t, MPFR_RNDN);

        mpf_t r;
        mpf_init(r);
        mpfr_get_f(r, t, MPFR_RNDN);

        var res(mpf_class(r), 1);

        mpfr_clear(t);
        mpf_clear(r);
        return res;
    }
    if (*(a.tid) == typeid(RtlComplex).name() || *(a.tid) == typeid(Complex).name())
    {
        mpc_t t;
        mpc_init2(t, DEFAULT_FLOAT_PRECISION);
        mpc_set_f_f(t, a.getReal().operator mpf_class().get_mpf_t(), a.getImag().operator mpf_class().get_mpf_t(), MPC_RNDNN);
        mpc_atan(t, t, MPC_RNDNN);

        mpfr_t _r, _i;
        mpfr_init2(_r, DEFAULT_FLOAT_PRECISION);
        mpfr_init2(_i, DEFAULT_FLOAT_PRECISION);
        mpc_real(_r, t, MPFR_RNDN);
        mpc_imag(_i, t, MPFR_RNDN);

        mpf_t r, i;
        mpf_init(r);
        mpf_init(i);
        mpfr_get_f(r, _r, MPFR_RNDN);
        mpfr_get_f(i, _i, MPFR_RNDN);

        var res(Complex(mpf_class(r), mpf_class(i)), 1);

        mpc_clear(t);
        mpfr_clear(_r);
        mpfr_clear(_i);
        mpf_clear(r);
        mpf_clear(i);
        return res;
    }

    throw Exception::errorOperator(a.typeName(), "atan(const var &)");
}

///@brief Atan function with 2 arguments for class var.

/// Only available for real number.
var atan2(const var &a, const var &b)
{
    if (*(a.tid) == typeid(mpq_class).name() || *(a.tid) == typeid(mpf_class).name())
        if (*(b.tid) == typeid(mpq_class).name() || *(b.tid) == typeid(mpf_class).name())
        {
            mpfr_t t1;
            mpfr_init_set_f(t1, a.operator mpf_class().get_mpf_t(), MPFR_RNDN);
            mpfr_t t2;
            mpfr_init_set_f(t2, a.operator mpf_class().get_mpf_t(), MPFR_RNDN);
            mpfr_atan2(t1, t1, t2, MPFR_RNDN);

            mpf_t r;
            mpf_init(r);
            mpfr_get_f(r, t1, MPFR_RNDN);

            var res(mpf_class(r), 1);
            mpfr_clear(t1);
            mpfr_clear(t2);
            mpf_clear(r);
            return res;
        }

    throw Exception::errorOperator(a.typeName(), b.typeName(), "atan2(const var &, const var &)");
}

///@brief Floor function for class var.
var floor(const var &a)
{
    if (*(a.tid) == typeid(mpq_class).name() || *(a.tid) == typeid(mpf_class).name())
    {
        mpfr_t t;
        mpfr_init_set_f(t, a.operator mpf_class().get_mpf_t(), MPFR_RNDN);
        mpfr_floor(t, t);

        mpf_t r;
        mpf_init(r);
        mpfr_get_f(r, t, MPFR_RNDN);

        var res(mpf_class(r), *(a.opt));
        mpfr_clear(t);
        mpf_clear(r);
        return res;
    }

    throw Exception::errorOperator(a.typeName(), "floor(const var &)");
}

///@brief Ceiling function for class var.
var ceil(const var &a)
{
    if (*(a.tid) == typeid(mpq_class).name() || *(a.tid) == typeid(mpf_class).name())
    {
        mpfr_t t;
        mpfr_init_set_f(t, a.operator mpf_class().get_mpf_t(), MPFR_RNDN);
        mpfr_ceil(t, t);

        mpf_t r;
        mpf_init(r);
        mpfr_get_f(r, t, MPFR_RNDN);

        var res(mpf_class(r), *(a.opt));
        mpfr_clear(t);
        mpf_clear(r);
        return res;
    }

    throw Exception::errorOperator(a.typeName(), "ceil(const var &)");
}

///@brief Truncate function for class var.
var trunc(const var &a)
{
    if (*(a.tid) == typeid(mpq_class).name() || *(a.tid) == typeid(mpf_class).name())
    {
        mpfr_t t;
        mpfr_init_set_f(t, a.operator mpf_class().get_mpf_t(), MPFR_RNDN);
        mpfr_trunc(t, t);

        mpf_t r;
        mpf_init(r);
        mpfr_get_f(r, t, MPFR_RNDN);

        var res(mpf_class(r), *(a.opt));
        mpfr_clear(t);
        mpf_clear(r);
        return res;
    }

    throw Exception::errorOperator(a.typeName(), "trunc(const var &)");
}

///@brief Round function for class var.
var round(const var &a)
{
    if (*(a.tid) == typeid(mpq_class).name() || *(a.tid) == typeid(mpf_class).name())
    {
        mpfr_t t;
        mpfr_init_set_f(t, a.operator mpf_class().get_mpf_t(), MPFR_RNDN);
        mpfr_round(t, t);

        mpf_t r;
        mpf_init(r);
        mpfr_get_f(r, t, MPFR_RNDN);

        var res(mpf_class(r), *(a.opt));
        mpfr_clear(t);
        mpf_clear(r);
        return res;
    }

    throw Exception::errorOperator(a.typeName(), "round(const var &)");
}

///@brief Square root function for class var.
var sqrt(const var &a)
{
    if (*(a.tid) == typeid(mpq_class).name() || *(a.tid) == typeid(mpf_class).name())
    {
        if (a.operator mpf_class() >= 0)
        {
            mpfr_t t;
            mpfr_init_set_f(t, a.operator mpf_class().get_mpf_t(), MPFR_RNDN);
            mpfr_sqrt(t, t, MPFR_RNDN);

            mpf_t r;
            mpf_init(r);
            mpfr_get_f(r, t, MPFR_RNDN);

            var res(mpf_class(r), 1);
            mpfr_clear(t);
            mpf_clear(r);
            return res;
        }
        else
        {
            mpc_t t;
            mpc_init2(t, DEFAULT_FLOAT_PRECISION);
            mpc_set_f(t, a.operator mpf_class().get_mpf_t(), MPC_RNDNN);
            mpc_sqrt(t, t, MPC_RNDNN);

            mpfr_t _r, _i;
            mpfr_init2(_r, DEFAULT_FLOAT_PRECISION);
            mpfr_init2(_i, DEFAULT_FLOAT_PRECISION);
            mpc_real(_r, t, MPFR_RNDN);
            mpc_imag(_i, t, MPFR_RNDN);

            mpf_t r, i;
            mpf_init(r);
            mpf_init(i);
            mpfr_get_f(r, _r, MPFR_RNDN);
            mpfr_get_f(i, _i, MPFR_RNDN);

            var res(Complex(mpf_class(r), mpf_class(i)), 1);

            mpc_clear(t);
            mpfr_clear(_r);
            mpfr_clear(_i);
            mpf_clear(r);
            mpf_clear(i);
            return res;
        }
    }
    if (*(a.tid) == typeid(RtlComplex).name() || *(a.tid) == typeid(Complex).name())
    {
        mpc_t t;
        mpc_init2(t, DEFAULT_FLOAT_PRECISION);
        mpc_set_f_f(t, a.getReal().operator mpf_class().get_mpf_t(), a.getImag().operator mpf_class().get_mpf_t(), MPC_RNDNN);
        mpc_sqrt(t, t, MPC_RNDNN);

        mpfr_t _r, _i;
        mpfr_init2(_r, DEFAULT_FLOAT_PRECISION);
        mpfr_init2(_i, DEFAULT_FLOAT_PRECISION);
        mpc_real(_r, t, MPFR_RNDN);
        mpc_imag(_i, t, MPFR_RNDN);

        mpf_t r, i;
        mpf_init(r);
        mpf_init(i);
        mpfr_get_f(r, _r, MPFR_RNDN);
        mpfr_get_f(i, _i, MPFR_RNDN);

        var res(Complex(mpf_class(r), mpf_class(i)), 1);

        mpc_clear(t);
        mpfr_clear(_r);
        mpfr_clear(_i);
        mpf_clear(r);
        mpf_clear(i);
        return res;
    }

    throw Exception::errorOperator(a.typeName(), "sqrt(const var &)");
}

///@brief Exponential function for class var.
var exp(const var &a)
{
    if (*(a.tid) == typeid(mpq_class).name() || *(a.tid) == typeid(mpf_class).name())
    {
        mpfr_t t;
        mpfr_init_set_f(t, a.operator mpf_class().get_mpf_t(), MPFR_RNDN);
        mpfr_exp(t, t, MPFR_RNDN);

        mpf_t r;
        mpf_init(r);
        mpfr_get_f(r, t, MPFR_RNDN);

        var res(mpf_class(r), 1);
        mpfr_clear(t);
        mpf_clear(r);
        return res;
    }
    if (*(a.tid) == typeid(RtlComplex).name() || *(a.tid) == typeid(Complex).name())
    {
        mpc_t t;
        mpc_init2(t, DEFAULT_FLOAT_PRECISION);
        mpc_set_f_f(t, a.getReal().operator mpf_class().get_mpf_t(), a.getImag().operator mpf_class().get_mpf_t(), MPC_RNDNN);
        mpc_log(t, t, MPC_RNDNN);

        mpfr_t _r, _i;
        mpfr_init2(_r, DEFAULT_FLOAT_PRECISION);
        mpfr_init2(_i, DEFAULT_FLOAT_PRECISION);
        mpc_real(_r, t, MPFR_RNDN);
        mpc_imag(_i, t, MPFR_RNDN);

        mpf_t r, i;
        mpf_init(r);
        mpf_init(i);
        mpfr_get_f(r, _r, MPFR_RNDN);
        mpfr_get_f(i, _i, MPFR_RNDN);

        var res(Complex(mpf_class(r), mpf_class(i)), 1);

        mpc_clear(t);
        mpfr_clear(_r);
        mpfr_clear(_i);
        mpf_clear(r);
        mpf_clear(i);
        return res;
    }

    throw Exception::errorOperator(a.typeName(), "exp(const var &)");
}

///@brief Natural logarithm function for class var.
var log(const var &a)
{
    if (*(a.tid) == typeid(mpq_class).name() || *(a.tid) == typeid(mpf_class).name())
    {
        if (a.operator mpf_class() >= 0)
        {
            mpfr_t t;
            mpfr_init_set_f(t, a.operator mpf_class().get_mpf_t(), MPFR_RNDN);
            mpfr_log(t, t, MPFR_RNDN);

            mpf_t r;
            mpf_init(r);
            mpfr_get_f(r, t, MPFR_RNDN);

            var res(mpf_class(r), 1);
            mpfr_clear(t);
            mpf_clear(r);
            return res;
        }
        else
        {
            mpc_t t;
            mpc_init2(t, DEFAULT_FLOAT_PRECISION);
            mpc_set_f(t, a.operator mpf_class().get_mpf_t(), MPC_RNDNN);
            mpc_log(t, t, MPC_RNDNN);

            mpfr_t _r, _i;
            mpfr_init2(_r, DEFAULT_FLOAT_PRECISION);
            mpfr_init2(_i, DEFAULT_FLOAT_PRECISION);
            mpc_real(_r, t, MPFR_RNDN);
            mpc_imag(_i, t, MPFR_RNDN);

            mpf_t r, i;
            mpf_init(r);
            mpf_init(i);
            mpfr_get_f(r, _r, MPFR_RNDN);
            mpfr_get_f(i, _i, MPFR_RNDN);

            var res(Complex(mpf_class(r), mpf_class(i)), 1);

            mpc_clear(t);
            mpfr_clear(_r);
            mpfr_clear(_i);
            mpf_clear(r);
            mpf_clear(i);
            return res;
        }
    }
    if (*(a.tid) == typeid(RtlComplex).name() || *(a.tid) == typeid(Complex).name())
    {
        mpc_t t;
        mpc_init2(t, DEFAULT_FLOAT_PRECISION);
        mpc_set_f_f(t, a.getReal().operator mpf_class().get_mpf_t(), a.getImag().operator mpf_class().get_mpf_t(), MPC_RNDNN);
        mpc_log(t, t, MPC_RNDNN);

        mpfr_t _r, _i;
        mpfr_init2(_r, DEFAULT_FLOAT_PRECISION);
        mpfr_init2(_i, DEFAULT_FLOAT_PRECISION);
        mpc_real(_r, t, MPFR_RNDN);
        mpc_imag(_i, t, MPFR_RNDN);

        mpf_t r, i;
        mpf_init(r);
        mpf_init(i);
        mpfr_get_f(r, _r, MPFR_RNDN);
        mpfr_get_f(i, _i, MPFR_RNDN);

        var res(Complex(mpf_class(r), mpf_class(i)), 1);

        mpc_clear(t);
        mpfr_clear(_r);
        mpfr_clear(_i);
        mpf_clear(r);
        mpf_clear(i);
        return res;
    }

    throw Exception::errorOperator(a.typeName(), "exp(const log &)");
}

///@brief Power function for class var.
var pow(const var &a, const var &b)
{
    if (*(a.tid) == typeid(mpq_class).name() || *(a.tid) == typeid(mpf_class).name())
        if (*(b.tid) == typeid(mpq_class).name() || *(b.tid) == typeid(mpf_class).name())
        {
            mpfr_t t1;
            mpfr_init_set_f(t1, a.operator mpf_class().get_mpf_t(), MPFR_RNDN);
            mpfr_t t2;
            mpfr_init_set_f(t2, a.operator mpf_class().get_mpf_t(), MPFR_RNDN);
            mpfr_pow(t1, t1, t2, MPFR_RNDN);

            mpf_t r;
            mpf_init(r);
            mpfr_get_f(r, t1, MPFR_RNDN);

            var res(mpf_class(r), 1);
            mpfr_clear(t1);
            mpfr_clear(t2);
            mpf_clear(r);
            return res;
        }
    if (*(a.tid) == typeid(mpq_class).name() || *(a.tid) == typeid(mpf_class).name() || *(a.tid) == typeid(RtlComplex).name() || *(a.tid) == typeid(Complex).name())
        if (*(b.tid) == typeid(mpq_class).name() || *(b.tid) == typeid(mpf_class).name() || *(b.tid) == typeid(RtlComplex).name() || *(b.tid) == typeid(Complex).name())
        {
            mpc_t t1, t2;
            mpc_init2(t1, DEFAULT_FLOAT_PRECISION);
            mpc_init2(t2, DEFAULT_FLOAT_PRECISION);
            mpc_set_f_f(t1, a.operator Complex().re.get_mpf_t(), a.operator Complex().im.get_mpf_t(), MPC_RNDNN);
            mpc_set_f_f(t2, b.operator Complex().re.get_mpf_t(), b.operator Complex().im.get_mpf_t(), MPC_RNDNN);
            mpc_pow(t1, t1, t2, MPC_RNDNN);

            mpfr_t _r, _i;
            mpfr_init2(_r, DEFAULT_FLOAT_PRECISION);
            mpfr_init2(_i, DEFAULT_FLOAT_PRECISION);
            mpc_real(_r, t1, MPFR_RNDN);
            mpc_imag(_i, t1, MPFR_RNDN);

            mpf_t r, i;
            mpf_init(r);
            mpf_init(i);
            mpfr_get_f(r, _r, MPFR_RNDN);
            mpfr_get_f(i, _i, MPFR_RNDN);

            var res(Complex(mpf_class(r), mpf_class(i)), 1);

            mpc_clear(t1);
            mpc_clear(t2);
            mpfr_clear(_r);
            mpfr_clear(_i);
            mpf_clear(r);
            mpf_clear(i);
            return res;
        }

    throw Exception::errorOperator(a.typeName(), b.typeName(), "pow(const var &, const var &)");
}

///@brief Absolute value function for class var.
var abs(const var &a)
{
    if (*(a.tid) == typeid(mpq_class).name())
        return var((mpq_class)abs(a.operator mpq_class()), *(a.opt));
    else if (*(a.tid) == typeid(mpf_class).name())
        return var((mpf_class)abs(a.operator mpf_class()), *(a.opt));
    else if (*(a.tid) == typeid(RtlComplex).name())
        return var(a.operator RtlComplex().abs(), *(a.opt));
    else if (*(a.tid) == typeid(Complex).name())
        return var(a.operator Complex().abs(), *(a.opt));

    throw Exception::errorOperator(a.typeName(), "abs(const var &)");
}

///@brief Default consturctor.

/// Reference from a new empty var.
refVar::refVar():ptr(new var()) {}

///@brief Constructor.

/// Reference from a specific object.
/// It will construct a copy of the object on heap.
refVar::refVar(const var &b):ptr(new var(b.copy())) {}

///@brief Copy consturctor.
refVar::refVar(const refVar &b):ptr(b.ptr)
{
    ++(*(ptr -> ref));
}

///@brief Destructor.

/// Decrease the reference counter and delete the pointer if necessary.
refVar::~refVar()
{
    if (! --(*(ptr -> ref)))
    {
        ++(*(ptr -> ref));
        delete ptr;
    }
}

///@brief Assignment.

/// Assigned by var.
/// The referenced object will by modify.
refVar refVar::operator =(const var &b)
{
    var _b = b.copy();
    ++(*(b.ref));
    ptr -> val = _b.val;
    ptr -> opt = _b.opt;
    ptr -> tid = _b.tid;
    ++(*(_b.ref));
    return *this;
}

///@brief Assignment.
/// Assigned by refVar.
/// It will reference from a new object without modifying the initial one.
refVar refVar::operator =(const refVar &b)
{
    if (! --(*(ptr -> ref)))
    {
        ++(*(ptr -> ref));
        delete ptr;
    }

    ptr = b.ptr;
    ++(*(ptr -> ref));
    return *this;
}

