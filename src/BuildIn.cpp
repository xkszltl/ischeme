#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <ctime>
#include <cassert>
#include <cctype>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <string>
#include <vector>
#include <bitset>
#include <set>
#include <map>
#include <queue>
#include <list>
#include <limits>

#include <gmp.h>
#include <gmpxx.h>
#include <mpfr.h>
#include <mpc.h>

#include "Constant.h"
#include "scmType.h"
#include "Environment.h"
#include "Interpreter.h"
#include "BuildIn.h"

using namespace std;

Environment BuildIn::buildinEnv;

///@brief Initialize the build-in environment.

/// Some library procedures may be added for convenience or efficiency.
void BuildIn::setEnv()
{
    var nowEval;
    addBuildIn("eqv?");
    addBuildIn("eq?");
    addBuildIn("number?");
    addBuildIn("complex?");
    addBuildIn("real?");
    addBuildIn("rational?");
    addBuildIn("integer?");
    addBuildIn("exact?");
    addBuildIn("inexact?");
    addBuildIn("=");
    addBuildIn("<");
    addBuildIn(">");
    addBuildIn("<=");
    addBuildIn(">=");
    addBuildIn("+");
    addBuildIn("*");
    addBuildIn("-");
    addBuildIn("/");
    addBuildIn("quotient");
    addBuildIn("remainder");
    addBuildIn("modulo");
    addBuildIn("numerator");
    addBuildIn("denominator");
    addBuildIn("floor");
    addBuildIn("ceiling");
    addBuildIn("truncate");
    addBuildIn("round");
    addBuildIn("exp");
    addBuildIn("log");
    addBuildIn("sin");
    addBuildIn("cos");
    addBuildIn("tan");
    addBuildIn("asin");
    addBuildIn("acos");
    addBuildIn("atan");
    addBuildIn("sqrt");
    addBuildIn("expt");
    addBuildIn("make-rectangular");
    addBuildIn("make-polar");
    addBuildIn("real-part");
    addBuildIn("imag-part");
    addBuildIn("magnitude");
    addBuildIn("angle");
    addBuildIn("exact->inexact");
    addBuildIn("inexact->exact");
    addBuildIn("number->string");
    addBuildIn("string->number");
    addBuildIn("pair?");
    addBuildIn("cons");
    addBuildIn("car");
    addBuildIn("cdr");
    addBuildIn("set-car!");
    addBuildIn("set-cdr!");
    addBuildIn("symbol?");
    addBuildIn("symbol->string");
    addBuildIn("string->symbol");
    addBuildIn("char?");
    addBuildIn("char=?");
    addBuildIn("char<?");
    addBuildIn("char>?");
    addBuildIn("char<=?");
    addBuildIn("char>=?");
    addBuildIn("char->integer");
    addBuildIn("integer->char");
    addBuildIn("string?");
    addBuildIn("make-string");
    addBuildIn("string-length");
    addBuildIn("string-ref");
    addBuildIn("string-set!");
    addBuildIn("vector?");
    addBuildIn("make-vector");
    addBuildIn("vector-length");
    addBuildIn("vector-ref");
    addBuildIn("vector-set!");
    addBuildIn("procedure?");
    addBuildIn("apply");
    //addBuildIn("call-with-current-continuation");
    //addBuildIn("values");
    //addBuildIn("call-with-values");
    //addBuildIn("dynamic-wind");

    //Library Procedure
    addBuildIn("equal?");
    addBuildIn("zero?");
    addBuildIn("positive?");
    addBuildIn("negative?");
    addBuildIn("odd?");
    addBuildIn("even?");
    addBuildIn("max");
    addBuildIn("min");
    addBuildIn("abs");
    addBuildIn("gcd");
    addBuildIn("lcm");
    //addBuildIn("rationalize");
    addBuildIn("not");
    addBuildIn("boolean?");
    addBuildIn("null?");
    addBuildIn("list?");
    addBuildIn("list");
    addBuildIn("length");
    addBuildIn("append");
    addBuildIn("reverse");
    addBuildIn("list-tail");
    addBuildIn("list-ref");
    addBuildIn("memq");
    addBuildIn("memv");
    addBuildIn("member");
    addBuildIn("assq");
    addBuildIn("assv");
    addBuildIn("assoc");
    addBuildIn("char-ci=?");
    addBuildIn("char-ci<?");
    addBuildIn("char-ci>?");
    addBuildIn("char-ci<=?");
    addBuildIn("char-ci>=?");
    addBuildIn("char-alphabetic?");
    addBuildIn("char-numeric?");
    addBuildIn("char-whitespace?");
    addBuildIn("char-upper-case?");
    addBuildIn("char-lower-case?");
    addBuildIn("char-upcase");
    addBuildIn("char-downcase");
    addBuildIn("string");
    addBuildIn("string=?");
    addBuildIn("string-ci=?");
    addBuildIn("string<?");
    addBuildIn("string>?");
    addBuildIn("string<=?");
    addBuildIn("string>=?");
    addBuildIn("string-ci<?");
    addBuildIn("string-ci>?");
    addBuildIn("string-ci<=?");
    addBuildIn("string-ci>=?");
    addBuildIn("substring");
    addBuildIn("string-append");
    addBuildIn("string->list");
    addBuildIn("list->string");
    addBuildIn("string-copy");
    addBuildIn("string-fill!");
    addBuildIn("vector");
    addBuildIn("display");
    addBuildIn("newline");
    addBuildIn("load");
}

///@brief Evaluate a build-in procedure with an arguments list.

/// The evaluation will be store in "eval".
/// Some library procedures may also be defined for convenience.
int BuildIn::run(string procName, vector<var> arg, Environment &env, var &eval)
{
    if (procName == "eqv?")
    {
        exceptionArgNum(2, 2, "eqv?");

        if (*(arg[0].tid) == *(arg[1].tid) && (*(arg[0].tid) == typeid(Pair).name() || *(arg[0].tid) == typeid(string).name() || *(arg[0].tid) == typeid(Vector).name()))
            eval = var(arg[0].val == arg[1].val);
        else
            eval = var(arg[0] == arg[1]);

        return 1;
    }

    if (procName == "eq?")
    {
        exceptionArgNum(2, 2, "eq?");

        if (*(arg[0].tid) == typeid(Empty).name() && *(arg[1].tid) == typeid(Empty).name())
            eval = var(false);
        else if (*(arg[0].tid) == typeid(EmptyList).name() && *(arg[1].tid) == typeid(EmptyList).name())
            eval = var(true);
        else
            eval = var(arg[0].val == arg[1].val);
        return 1;
    }

    if (procName == "number?")
    {
        exceptionArgNum(1, 1, "number?");

        if (*(arg[0].tid) == typeid(mpq_class).name())
            eval = var(true);
        else if (*(arg[0].tid) == typeid(mpf_class).name())
            eval = var(true);
        else if (*(arg[0].tid) == typeid(RtlComplex).name())
            eval = var(true);
        else if (*(arg[0].tid) == typeid(Complex).name())
            eval = var(true);
        else
            eval = var(false);
        return 1;
    }

    if (procName == "complex?")
    {
        exceptionArgNum(1, 1, "complex?");

        if (*(arg[0].tid) == typeid(mpq_class).name())
            eval = var(true);
        else if (*(arg[0].tid) == typeid(mpf_class).name())
            eval = var(true);
        else if (*(arg[0].tid) == typeid(RtlComplex).name())
            eval = var(true);
        else if (*(arg[0].tid) == typeid(Complex).name())
            eval = var(true);
        else
            eval = var(false);
        return 1;
    }

    if (procName == "real?")
    {
        exceptionArgNum(1, 1, "real?");

        if (*(arg[0].tid) == typeid(mpq_class).name())
            eval = var(true);
        else if (*(arg[0].tid) == typeid(mpf_class).name())
            eval = var(true);
        else if (*(arg[0].tid) == typeid(RtlComplex).name())
            eval = var(arg[0].operator RtlComplex().im == 0);
        else if (*(arg[0].tid) == typeid(Complex).name())
            eval = var(arg[0].operator Complex().im == 0);
        else
            eval = var(false);
        return 1;
    }

    if (procName == "rational?")
    {
        exceptionArgNum(1, 1, "rational?");

        if (*(arg[0].tid) == typeid(mpq_class).name())
            eval = var(true);
        else if (*(arg[0].tid) == typeid(RtlComplex).name())
            eval = var(arg[0].operator RtlComplex().im == 0);
        else
            eval = var(false);
        return 1;
    }

    if (procName == "integer?")
    {
        exceptionArgNum(1, 1, "integer?");

        if (*(arg[0].tid) == typeid(mpq_class).name())
            eval = var(arg[0].operator mpq_class().get_den() == 1);
        else if (*(arg[0].tid) == typeid(RtlComplex).name())
            eval = var(arg[0].operator RtlComplex().im == 0 && arg[0].operator RtlComplex().re.get_den() == 1);
        else
            eval = var(false);
        return 1;
    }

    if (procName == "exact?")
    {
        exceptionArgNum(1, 1, "exact?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name() && *(arg[i].tid) != typeid(RtlComplex).name() && *(arg[i].tid) != typeid(Complex).name())
                throw Exception::errorArgType(arg[i].typeName(), "exact?", i);

        eval = var(! (bool)(*(arg[0].opt) & 1));
        return 1;
    }

    if (procName == "inexact?")
    {
        exceptionArgNum(1, 1, "inexact?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name() && *(arg[i].tid) != typeid(RtlComplex).name() && *(arg[i].tid) != typeid(Complex).name())
                throw Exception::errorArgType(arg[i].typeName(), "inexact?", i);

        eval = var((bool)((*(arg[0].opt) & 1)));
        return 1;
    }

    if (procName == "=")
    {
        exceptionArgNumLower(1, "=");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name() && *(arg[i].tid) != typeid(RtlComplex).name() && *(arg[i].tid) != typeid(Complex).name())
                throw Exception::errorArgType(arg[i].typeName(), "=", i);

        bool flag = 1;
        for (int i = 1; i < arg.size(); ++i)
            flag &= (arg[i-1] == arg[i]);
        eval = var(flag);
        return 1;
    }

    if (procName == "<")
    {
        exceptionArgNumLower(1, "<");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name())
                throw Exception::errorArgType(arg[i].typeName(), "<", i);

        bool flag = 1;
        for (int i = 1; i < arg.size(); ++i)
            flag &= (arg[i-1] < arg[i]);
        eval = var(flag);
        return 1;
    }

    if (procName == ">")
    {
        exceptionArgNumLower(1, ">");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name())
                throw Exception::errorArgType(arg[i].typeName(), ">", i);

        bool flag = 1;
        for (int i = 1; i < arg.size(); ++i)
            flag &= (arg[i-1] > arg[i]);
        eval = var(flag);
        return 1;
    }

    if (procName == "<=")
    {
        exceptionArgNumLower(1, "<=");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name())
                throw Exception::errorArgType(arg[i].typeName(), "<=", i);

        bool flag = 1;
        for (int i = 1; i < arg.size(); ++i)
            flag &= (arg[i-1] <= arg[i]);
        eval = var(flag);
        return 1;
    }

    if (procName == ">=")
    {
        exceptionArgNumLower(1, ">=");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name())
                throw Exception::errorArgType(arg[i].typeName(), ">=", i);

        bool flag = 1;
        for (int i = 1; i < arg.size(); ++i)
            flag &= (arg[i-1] >= arg[i]);
        eval = var(flag);
        return 1;
    }

    if (procName == "+")
    {
        exceptionArgNumLower(0, "+");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name() && *(arg[i].tid) != typeid(RtlComplex).name() && *(arg[i].tid) != typeid(Complex).name())
                throw Exception::errorArgType(arg[i].typeName(), "+", i);

        eval = var(mpq_class(0));
        for (int i = 0; i < arg.size(); eval = eval+arg[i++]);
        return 1;
    }

    if (procName == "*")
    {
        exceptionArgNumLower(0, "*");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name() && *(arg[i].tid) != typeid(RtlComplex).name() && *(arg[i].tid) != typeid(Complex).name())
                throw Exception::errorArgType(arg[i].typeName(), "*", i);

        eval = var(mpq_class(1));
        for (int i = 0; i < arg.size(); eval = eval*arg[i++]);
        return 1;
    }

    if (procName == "-")
    {
        exceptionArgNumLower(1, "-");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name() && *(arg[i].tid) != typeid(RtlComplex).name() && *(arg[i].tid) != typeid(Complex).name())
                throw Exception::errorArgType(arg[i].typeName(), "-", i);

        if (arg.size() == 1)
            eval = var(mpq_class(0))-arg[0];
        else
        {
            eval = arg[0];
            for (int i = 1; i < arg.size(); eval = eval-arg[i++]);
        }
        return 1;
    }

    if (procName == "/")
    {
        exceptionArgNumLower(1, "/");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name() && *(arg[i].tid) != typeid(RtlComplex).name() && *(arg[i].tid) != typeid(Complex).name())
                throw Exception::errorArgType(arg[i].typeName(), "/", i);

        for (int i = (arg.size() == 1 ? 0 : 1); i < arg.size(); ++i)
            if ((*(arg[i].tid) == typeid(mpq_class).name() || *(arg[i].tid) == typeid(RtlComplex).name()) && arg[i].operator RtlComplex().norm().get_num() == 0 ||
                (*(arg[i].tid) == typeid(mpf_class).name() || *(arg[i].tid) == typeid(Complex).name()) && arg[i].operator Complex().norm() == 0)
            {
                ostringstream buffer;
                buffer << arg[0].getStr();
                for (int i = 1; i < arg.size(); buffer << " " << arg[i++].getStr());
                throw Exception::errorDivideByZero("/", buffer.str());
            }

        if (arg.size() == 1)
            eval = var(mpq_class(1))/arg[0];
        else
        {
            eval = arg[0];
            for (int i = 1; i < arg.size(); eval = eval/arg[i++]);
        }
        return 1;
    }

    if (procName == "quotient")
    {
        exceptionArgNum(2, 2, "quotient");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name())
                throw Exception::errorArgType(arg[i].typeName(), "quotient", i);
            else if (arg[i].operator mpq_class().get_den() != 1)
                throw Exception::errorArgType("integer", "quotient", i);

        if (arg[1].operator mpq_class().get_num() == 0)
            throw Exception::errorDivideByZero("quotient", arg[0].getStr()+" "+arg[1].getStr());

        eval = var(mpq_class(arg[0].operator mpq_class().get_num()/arg[1].operator mpq_class().get_num()), *(arg[0].opt) | *(arg[1].opt));
        return 1;
    }

    if (procName == "remainder")
    {
        exceptionArgNum(2, 2, "remainder");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name())
                throw Exception::errorArgType(arg[i].typeName(), "remainder", i);
            else if (arg[i].operator mpq_class().get_den() != 1)
                throw Exception::errorArgType("integer", "remainder", i);

        if (arg[1].operator mpq_class().get_num() == 0)
            throw Exception::errorDivideByZero("remainder", arg[0].getStr()+" "+arg[1].getStr());

        eval = var(mpq_class(arg[0].operator mpq_class().get_num()), *(arg[0].opt)) % var(mpq_class(arg[1].operator mpq_class().get_num()), *(arg[1].opt));
        return 1;
    }

    if (procName == "modulo")
    {
        exceptionArgNum(2, 2, "modulo");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name())
                throw Exception::errorArgType(arg[i].typeName(), "modulo", i);
            else if (arg[i].operator mpq_class().get_den() != 1)
                throw Exception::errorArgType("rational number", "modulo", i);

        if (arg[1].operator mpq_class().get_num() == 0)
            throw Exception::errorDivideByZero("modulo", arg[0].getStr()+" "+arg[1].getStr());

        eval = var(mpq_class(arg[0].operator mpq_class().get_num()), *(arg[0].opt)) % var(mpq_class(arg[1].operator mpq_class().get_num()), *(arg[1].opt));
        if (arg[1] < var(mpq_class(0)) && eval > var(mpq_class(0)))
            eval = eval+arg[1];
        if (arg[1] > var(mpq_class(0)) && eval < var(mpq_class(0)))
            eval = eval+arg[1];
        return 1;
    }

    if (procName == "numerator")
    {
        exceptionArgNum(1, 1, "numerator");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name())
                throw Exception::errorArgType(arg[i].typeName(), "numerator", i);

        eval = var(mpq_class(arg[0].operator mpq_class().get_num()));
        return 1;
    }

    if (procName == "denominator")
    {
        exceptionArgNum(1, 1, "denominator");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name())
                throw Exception::errorArgType(arg[i].typeName(), "denominator", i);

        eval = var(mpq_class(arg[0].operator mpq_class().get_den()));
        return 1;
    }

    if (procName == "floor")
    {
        exceptionArgNum(1, 1, "floor");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name())
                throw Exception::errorArgType(arg[i].typeName(), "floor", i);

        eval = floor(arg[0]);
        return 1;
    }

    if (procName == "ceiling")
    {
        exceptionArgNum(1, 1, "ceiling");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name())
                throw Exception::errorArgType(arg[i].typeName(), "ceiling", i);

        eval = ceil(arg[0]);
        return 1;
    }

    if (procName == "truncate")
    {
        exceptionArgNum(1, 1, "truncate");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name())
                throw Exception::errorArgType(arg[i].typeName(), "truncate", i);

        eval = trunc(arg[0]);
        return 1;
    }

    if (procName == "round")
    {
        exceptionArgNum(1, 1, "round");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name())
                throw Exception::errorArgType(arg[i].typeName(), "round", i);

        eval = round(arg[0]);
        return 1;
    }

    if (procName == "exp")
    {
        exceptionArgNum(1, 1, "exp");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name() && *(arg[i].tid) != typeid(RtlComplex).name() && *(arg[i].tid) != typeid(Complex).name())
                throw Exception::errorArgType(arg[i].typeName(), "exp", i);

        eval = exp(arg[0]);
        return 1;
    }

    if (procName == "log")
    {
        exceptionArgNum(1, 1, "log");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name() && *(arg[i].tid) != typeid(RtlComplex).name() && *(arg[i].tid) != typeid(Complex).name())
                throw Exception::errorArgType(arg[i].typeName(), "log", i);

        eval = log(arg[0]);
        return 1;
    }

    if (procName == "sin")
    {
        exceptionArgNum(1, 1, "sin");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name() && *(arg[i].tid) != typeid(RtlComplex).name() && *(arg[i].tid) != typeid(Complex).name())
                throw Exception::errorArgType(arg[i].typeName(), "sin", i);

        eval = sin(arg[0]);
        return 1;
    }

    if (procName == "cos")
    {
        exceptionArgNum(1, 1, "cos");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name() && *(arg[i].tid) != typeid(RtlComplex).name() && *(arg[i].tid) != typeid(Complex).name())
                throw Exception::errorArgType(arg[i].typeName(), "cos", i);

        eval = cos(arg[0]);
        return 1;
    }

    if (procName == "tan")
    {
        exceptionArgNum(1, 1, "tan");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name() && *(arg[i].tid) != typeid(RtlComplex).name() && *(arg[i].tid) != typeid(Complex).name())
                throw Exception::errorArgType(arg[i].typeName(), "tan", i);

        eval = tan(arg[0]);
        return 1;
    }

    if (procName == "asin")
    {
        exceptionArgNum(1, 1, "asin");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name() && *(arg[i].tid) != typeid(RtlComplex).name() && *(arg[i].tid) != typeid(Complex).name())
                throw Exception::errorArgType(arg[i].typeName(), "asin", i);

        eval = asin(arg[0]);
        return 1;
    }

    if (procName == "acos")
    {
        exceptionArgNum(1, 1, "acos");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name() && *(arg[i].tid) != typeid(RtlComplex).name() && *(arg[i].tid) != typeid(Complex).name())
                throw Exception::errorArgType(arg[i].typeName(), "acos", i);

        eval = acos(arg[0]);
        return 1;
    }

    if (procName == "atan")
    {
        exceptionArgNum(1, 2, "atan");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name() && *(arg[i].tid) != typeid(RtlComplex).name() && *(arg[i].tid) != typeid(Complex).name())
                throw Exception::errorArgType(arg[i].typeName(), "atan", i);

        if (arg.size() == 1)
            eval = atan(arg[0]);
        else if (arg.size() == 2)
            eval = atan2(arg[0], arg[1]);
        return 1;
    }

    if (procName == "sqrt")
    {
        exceptionArgNum(1, 1, "sqrt");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name() && *(arg[i].tid) != typeid(RtlComplex).name() && *(arg[i].tid) != typeid(Complex).name())
                throw Exception::errorArgType(arg[i].typeName(), "sqrt", i);

        eval = sqrt(arg[0]);
        return 1;
    }

    if (procName == "expt")
    {
        exceptionArgNum(2, 2, "expt");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name() && *(arg[i].tid) != typeid(RtlComplex).name() && *(arg[i].tid) != typeid(Complex).name())
                throw Exception::errorArgType(arg[i].typeName(), "expt", i);

        eval = pow(arg[0], arg[1]);
        return 1;
    }
    if (procName == "make-rectangular")
    {
        exceptionArgNum(2, 2, "make-rectangular");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name())
                throw Exception::errorArgType(arg[i].typeName(), "make-rectangular", i);

        if (*(arg[0].tid) == typeid(mpq_class).name() && *(arg[1].tid) == typeid(mpq_class).name())
            eval = var(RtlComplex(arg[0].operator mpq_class(), arg[1].operator mpq_class()));
        else
            eval = var(Complex(arg[0].operator mpf_class(), arg[1].operator mpf_class()));
        return 1;
    }

    if (procName == "make-polar")
    {
        exceptionArgNum(2, 2, "make-polar");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name())
                throw Exception::errorArgType(arg[i].typeName(), "make-polar", i);

        eval = var(Complex((arg[0]*cos(var(arg[1].operator mpf_class()))).operator mpf_class(), (arg[0]*sin(var(arg[1].operator mpf_class()))).operator mpf_class()));
        return 1;
    }

    if (procName == "real-part")
    {
        exceptionArgNum(1, 1, "real-part");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name() && *(arg[i].tid) != typeid(RtlComplex).name() && *(arg[i].tid) != typeid(Complex).name())
                throw Exception::errorArgType(arg[i].typeName(), "real-part", i);

        if (*(arg[0].tid) == typeid(mpq_class).name() || *(arg[0].tid) == typeid(mpf_class).name())
            eval = arg[0].copy();
        else if (*(arg[0].tid) == typeid(RtlComplex).name())
            eval = var(arg[0].operator RtlComplex().re);
        else if (*(arg[0].tid) == typeid(Complex).name())
            eval = var(arg[0].operator Complex().re);
        return 1;
    }

    if (procName == "imag-part")
    {
        exceptionArgNum(1, 1, "imag-part");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name() && *(arg[i].tid) != typeid(RtlComplex).name() && *(arg[i].tid) != typeid(Complex).name())
                throw Exception::errorArgType(arg[i].typeName(), "imag-part", i);

        if (*(arg[0].tid) == typeid(mpq_class).name())
            eval = var(mpq_class(0));
        if (*(arg[0].tid) == typeid(mpf_class).name())
            eval = var(mpf_class(0));
        else if (*(arg[0].tid) == typeid(RtlComplex).name())
            eval = var(arg[0].operator RtlComplex().im);
        else if (*(arg[0].tid) == typeid(Complex).name())
            eval = var(arg[0].operator Complex().im);
        return 1;
    }

    if (procName == "magnitude")
    {
        exceptionArgNum(1, 1, "magnitude");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name() && *(arg[i].tid) != typeid(RtlComplex).name() && *(arg[i].tid) != typeid(Complex).name())
                throw Exception::errorArgType(arg[i].typeName(), "magnitude", i);

        eval = abs(arg[0]);
        return 1;
    }

    if (procName == "angle")
    {
        exceptionArgNum(1, 1, "angle");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name() && *(arg[i].tid) != typeid(RtlComplex).name() && *(arg[i].tid) != typeid(Complex).name())
                throw Exception::errorArgType(arg[i].typeName(), "angle", i);

        eval = atan2(var(arg[0].operator Complex().re), var(arg[0].operator Complex().im));
        return 1;
    }

    if (procName == "exact->inexact")
    {
        exceptionArgNum(1, 1, "exact-inexact");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name() && *(arg[i].tid) != typeid(RtlComplex).name() && *(arg[i].tid) != typeid(Complex).name())
                throw Exception::errorArgType(arg[i].typeName(), "exact->inexact", i);

        eval = arg[0].copy();
        *(eval.opt) |= 1;
        return 1;
    }

    if (procName == "inexact->exact")
    {
        exceptionArgNum(1, 1, "inexact->exact");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name() && *(arg[i].tid) != typeid(RtlComplex).name() && *(arg[i].tid) != typeid(Complex).name())
                throw Exception::errorArgType(arg[i].typeName(), "inexact->exact", i);

        if (*(arg[0].tid) == typeid(mpq_class).name() || *(arg[0].tid) == typeid(RtlComplex).name())
        {
            eval = arg[0].copy();
            *(eval.opt) &= ~1;
        }
        else if (*(arg[0].tid) == typeid(mpf_class).name())
        {
            mpfr_t t;
            mpfr_init_set_f(t, arg[0].operator mpf_class().get_mpf_t(), MPFR_RNDN);
            mpz_t r;
            mpz_init(r);
            int e = mpfr_get_z_2exp(r, t);
            mpfr_clear(t);
            eval = var(mpq_class(mpz_class(r)), 0);
            for (int i = abs(e); i; --i)
                eval = e > 0 ? eval*var(mpq_class(2)) : eval/var(mpq_class(2));
        }
        else if (*(arg[0].tid) == typeid(Complex).name())
        {
            mpfr_t t1;
            mpfr_init_set_f(t1, arg[0].operator Complex().re.get_mpf_t(), MPFR_RNDN);
            mpz_t r1;
            mpz_init(r1);
            int e1 = mpfr_get_z_2exp(r1, t1);
            mpfr_clear(t1);
            var re = var(mpq_class(mpz_class(r1)), 0);
            for (int i = abs(e1); i; --i)
                re = e1 > 0 ? re*var(mpq_class(2)) : re/var(mpq_class(2));
            mpfr_t t2;
            mpfr_init_set_f(t2, arg[0].operator Complex().im.get_mpf_t(), MPFR_RNDN);
            mpz_t r2;
            mpz_init(r2);
            int e2 = mpfr_get_z_2exp(r2, t2);
            mpfr_clear(t2);
            var im = var(mpq_class(mpz_class(r2)), 0);
            for (int i = abs(e2); i; --i)
                im = e2 > 0 ? im*var(mpq_class(2)) : im/var(mpq_class(2));
            eval = var(RtlComplex(re.operator mpq_class(), im.operator mpq_class()));
        }
        return 1;
    }

    if (procName == "number->string")
    {
        exceptionArgNum(1, 1, "number->string");

        if (*(arg[0].tid) != typeid(mpq_class).name() && *(arg[0].tid) != typeid(mpf_class).name() && *(arg[0].tid) != typeid(RtlComplex).name() && *(arg[0].tid) != typeid(Complex).name())
            throw Exception::errorArgType(arg[0].typeName(), "number->string", 0);

        int base = 10;

        if (arg.size() == 2)
        {
            if (*(arg[1].tid) != typeid(mpq_class).name())
                throw Exception::errorArgType(arg[1].typeName(), "number->string", 1);
            else if (arg[1].operator mpq_class().get_den() != 1)
                throw Exception::errorArgType("rational number", "number->string", 1);

            base = arg[1].operator mpq_class().get_num().get_si();

            if (base != 2 && base != 8 && base != 10 && base != 16)
                throw Exception::errorArgValue(arg[1].getStr(), "number->string", 1);
        }

        if (base == 10)
            eval = var(arg[0].getStr());
        else
        {
            if (*(arg[0].tid) == typeid(mpq_class).name())
                eval = var(arg[0].operator mpq_class().get_str(base));
            else if (*(arg[0].tid) == typeid(RtlComplex).name())
                eval = var(arg[0].operator RtlComplex().re.get_str(base)+(arg[0].operator RtlComplex().im > 0 ? "+" : "")+arg[0].operator RtlComplex().im.get_str(base));
            else
                eval = var();
        }
        return 1;
    }

    if (procName == "string->number")
    {
        exceptionArgNum(1, 1, "string->number");

        if (*(arg[0].tid) != typeid(string).name())
            throw Exception::errorArgType(arg[0].typeName(), "string->number", 0);

        int base = 10;

        if (arg.size() == 2)
        {
            if (*(arg[1].tid) != typeid(mpq_class).name())
                throw Exception::errorArgType(arg[1].typeName(), "string->number", 1);
            else if (arg[1].operator mpq_class().get_den() != 1)
                throw Exception::errorArgType("rational number", "string->number", 1);

            base = arg[1].operator mpq_class().get_num().get_si();

            if (base != 2 && base != 8 && base != 10 && base != 16)
                throw Exception::errorArgValue(arg[1].getStr(), "string->number", 1);
        }

        string s = arg[0].operator string(), now;
        if (! Interpreter::_NumR(base, s, now, eval))
            eval = var(false);
        return 1;
    }

    if (procName == "pair?")
    {
        exceptionArgNum(1, 1, "pair?");

        eval = var(*(arg[0].tid) == typeid(Pair).name());
        return 1;
    }

    if (procName == "cons")
    {
        exceptionArgNum(2, 2, "cons");

        eval = var(Pair(arg[0], arg[1]));
        return 1;
    }

    if (procName == "car")
    {
        exceptionArgNum(1, 1, "car");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(Pair).name())
                throw Exception::errorArgType(arg[i].typeName(), "car", i);

        eval = arg[0].operator Pair().first;
        return 1;
    }

    if (procName == "cdr")
    {
        exceptionArgNum(1, 1, "cdr");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(Pair).name())
                throw Exception::errorArgType(arg[i].typeName(), "cdr", i);

        eval = arg[0].operator Pair().second;
        return 1;
    }

    if (procName == "set-car!")
    {
        exceptionArgNum(2, 2, "set-car!");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(Pair).name())
                throw Exception::errorArgType(arg[i].typeName(), "set-car!", i);

        arg[0].setFirst(arg[1]);
        eval = var();
        return 1;
    }

    if (procName == "set-cdr!")
    {
        exceptionArgNum(2, 2, "set-cdr!");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(Pair).name())
                throw Exception::errorArgType(arg[i].typeName(), "set-cdr!", i);

        arg[0].setSecond(arg[1]);
        eval = var();
        return 1;
    }

    if (procName == "symbol?")
    {
        exceptionArgNum(1, 1, "symbol?");

        eval = var(*(arg[0].tid) == typeid(Symbol).name());
        return 1;
    }

    if (procName == "symbol->string")
    {
        exceptionArgNum(1, 1, "symbol->string");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(Symbol).name())
                throw Exception::errorArgType(arg[i].typeName(), "symbol->string", i);

        eval = var(arg[0].operator Symbol().code);
        return 1;
    }

    if (procName == "string->symbol")
    {
        exceptionArgNum(1, 1, "string->symbol");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(string).name())
                throw Exception::errorArgType(arg[i].typeName(), "symbol->string", i);

        eval = var(Symbol(arg[0].operator string()));
        return 1;
    }

    if (procName == "char?")
    {
        exceptionArgNum(1, 1, "char?");

        eval = var(*(arg[0].tid) == typeid(char).name());
        return 1;
    }

    if (procName == "char=?")
    {
        exceptionArgNum(2, 2, "char=?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(char).name())
                throw Exception::errorArgType(arg[i].typeName(), "char=?", i);

        eval = var(arg[0].operator char() == arg[1].operator char());
        return 1;
    }

    if (procName == "char<?")
    {
        exceptionArgNum(2, 2, "char<?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(char).name())
                throw Exception::errorArgType(arg[i].typeName(), "char<?", i);

        eval = var(arg[0].operator char() < arg[1].operator char());
        return 1;
    }

    if (procName == "char>?")
    {
        exceptionArgNum(2, 2, "char>?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(char).name())
                throw Exception::errorArgType(arg[i].typeName(), "char>?", i);

        eval = var(arg[0].operator char() > arg[1].operator char());
        return 1;
    }

    if (procName == "char<=?")
    {
        exceptionArgNum(2, 2, "char<=?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(char).name())
                throw Exception::errorArgType(arg[i].typeName(), "char<=?", i);

        eval = var(arg[0].operator char() <= arg[1].operator char());
        return 1;
    }

    if (procName == "char>=?")
    {
        exceptionArgNum(2, 2, "char>=?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(char).name())
                throw Exception::errorArgType(arg[i].typeName(), "char>=?", i);

        eval = var(arg[0].operator char() >= arg[1].operator char());
        return 1;
    }

    if (procName == "char->integer")
    {
        exceptionArgNum(1, 1, "char->integer");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(char).name())
                throw Exception::errorArgType(arg[i].typeName(), "char->integer", i);

        eval = var(mpq_class((int)(arg[0].operator char())));
        return 1;
    }

    if (procName == "integer->char")
    {
        exceptionArgNum(1, 1, "integer->char");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name())
                throw Exception::errorArgType(arg[i].typeName(), "integer->char", i);
            else if (arg[i].operator mpq_class().get_den() != 1)
                throw Exception::errorArgType("rational number", "integer->char", i);

        eval = var((char)(arg[0].operator mpq_class().get_num().get_si()));
        return 1;
    }

    if (procName == "string?")
    {
        exceptionArgNum(1, 1, "string?");

        eval = var(*(arg[0].tid) == typeid(string).name());
        return 1;
    }

    if (procName == "make-string")
    {
        exceptionArgNum(1, 2, "make-string");

        if (*(arg[0].tid) != typeid(mpq_class).name())
            throw Exception::errorArgType(arg[0].typeName(), "make-string", 0);
        else if (arg[0].operator mpq_class().get_den() != 1)
            throw Exception::errorArgType("rational number", "make-string", 0);
        else if (arg[0].operator mpq_class() < 0)
            throw Exception::errorArgType("negative integer", "make-string", 0);

        if (arg.size() > 1)
            if (*(arg[1].tid) != typeid(char).name())
                throw Exception::errorArgType(arg[1].typeName(), "make-string", 1);

        eval = var(string(arg[0].operator mpq_class().get_num().get_si(), arg.size() == 1 ? '#' : arg[1].operator char()));
        return 1;
    }

    if (procName == "string-length")
    {
        exceptionArgNum(1, 1, "string-length");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(string).name())
                throw Exception::errorArgType(arg[i].typeName(), "string-length", i);

        eval = var(mpq_class(arg[0].operator string().size()));
        return 1;
    }

    if (procName == "string-ref")
    {
        exceptionArgNum(2, 2, "string-ref");

        if (*(arg[0].tid) != typeid(string).name())
            throw Exception::errorArgType(arg[0].typeName(), "string-ref", 0);

        if (*(arg[1].tid) != typeid(mpq_class).name())
            throw Exception::errorArgType(arg[1].typeName(), "string-ref", 1);
        else if (arg[1].operator mpq_class().get_den() != 1)
            throw Exception::errorArgType("rational number", "string-ref", 1);
        else if (arg[1].operator mpq_class() < 0)
            throw Exception::errorArgType("negative integer", "string-ref", 1);

        eval = var(arg[0].operator string()[arg[1].operator mpq_class().get_num().get_si()]);
        return 1;
    }

    if (procName == "string-set!")
    {
        exceptionArgNum(3, 3, "string-set!");

        if (*(arg[0].tid) != typeid(string).name())
            throw Exception::errorArgType(arg[0].typeName(), "string-set!", 0);

        if (*(arg[1].tid) != typeid(mpq_class).name())
            throw Exception::errorArgType(arg[1].typeName(), "string-set!", 1);
        else if (arg[1].operator mpq_class().get_den() != 1)
            throw Exception::errorArgType("rational number", "string-set!", 1);
        else if (arg[1].operator mpq_class() < 0)
            throw Exception::errorArgType("negative integer", "string-set!", 1);

        if (*(arg[2].tid) != typeid(char).name())
            throw Exception::errorArgType(arg[2].typeName(), "string-set!", 2);

        arg[0].operator string()[arg[1].operator mpq_class().get_num().get_si()] = arg[2].operator char();
        eval = var();
        return 1;
    }

    if (procName == "vector?")
    {
        exceptionArgNum(1, 1, "vector?");

        eval = var(*(arg[0].tid) == typeid(Vector).name());
        return 1;
    }

    if (procName == "make-vector")
    {
        exceptionArgNum(1, 2, "make-vector");

        if (*(arg[0].tid) != typeid(mpq_class).name())
            throw Exception::errorArgType(arg[0].typeName(), "make-vector", 0);
        else if (arg[0].operator mpq_class().get_den() != 1)
            throw Exception::errorArgType("rational number", "make-vector", 0);
        else if (arg[0].operator mpq_class() < 0)
            throw Exception::errorArgType("negative integer", "make-vector", 0);

        eval = var(Vector());
        for (int i = arg[0].operator mpq_class().get_num().get_si(); i; --i)
            eval.vecPushBack(arg.size() == 1 ? var() : arg[1]);
        return 1;
    }

    if (procName == "vector-length")
    {
        exceptionArgNum(1, 1, "vector-length");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(Vector).name())
                throw Exception::errorArgType(arg[i].typeName(), "vector-length", i);

        eval = var(mpq_class(arg[0].operator Vector().size()));
        return 1;
    }

    if (procName == "vector-ref")
    {
        exceptionArgNum(2, 2, "vector-ref");

        if (*(arg[0].tid) != typeid(Vector).name())
            throw Exception::errorArgType(arg[0].typeName(), "vector-ref", 0);

        if (*(arg[1].tid) != typeid(mpq_class).name())
            throw Exception::errorArgType(arg[1].typeName(), "vector-ref", 1);
        else if (arg[1].operator mpq_class().get_den() != 1)
            throw Exception::errorArgType("rational number", "vector-ref", 1);
        else if (arg[1].operator mpq_class() < 0)
            throw Exception::errorArgType("negative integer", "vector-ref", 1);

        eval = var(arg[0].operator Vector()[arg[1].operator mpq_class().get_num().get_si()]);
        return 1;
    }

    if (procName == "vector-set!")
    {
        exceptionArgNum(3, 3, "vector-set!");

        if (*(arg[0].tid) != typeid(Vector).name())
            throw Exception::errorArgType(arg[0].typeName(), "vector-set!", 0);

        if (*(arg[1].tid) != typeid(mpq_class).name())
            throw Exception::errorArgType(arg[1].typeName(), "vector-set!", 1);
        else if (arg[1].operator mpq_class().get_den() != 1)
            throw Exception::errorArgType("rational number", "vector-set!", 1);
        else if (arg[1].operator mpq_class() < 0)
            throw Exception::errorArgType("negative integer", "vector-set!", 1);

        arg[0].operator Vector()[arg[1].operator mpq_class().get_num().get_si()] = arg[2];
        eval = var();
        return 1;
    }

    if (procName == "procedure?")
    {
        exceptionArgNum(1, 1, "procedure?");

        eval = var(*(arg[0].tid) == typeid(Proc).name());
        return 1;
    }

    if (procName == "apply")
    {
        exceptionArgNumLower(1, "procedure?");

        var nowEval = var(Pair(arg[0], var(EmptyList())));
        for (int i = 1; i < arg.size()-1; ++i)
            nowEval.append(arg[i]);
        nowEval.appendList(arg.back());
        Interpreter::run(nowEval, env, eval);
        return 1;
    }

    //Library Procedure

    if (procName == "equal?")
    {
        exceptionArgNum(2, 2, "equal?");

        eval = var(arg[0] == arg[1]);
        return 1;
    }

    if (procName == "zero?")
    {
        exceptionArgNum(1, 1, "zero?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name() && *(arg[i].tid) != typeid(RtlComplex).name() && *(arg[i].tid) != typeid(Complex).name())
                throw Exception::errorArgType(arg[i].typeName(), "zero?", i);

        eval = var(arg[0] == var(mpq_class(0)));
        return 1;
    }

    if (procName == "positive?")
    {
        exceptionArgNum(1, 1, "positive?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name())
                throw Exception::errorArgType(arg[i].typeName(), "positive?", i);

        eval = var(arg[0] > var(mpq_class(0)));
        return 1;
    }

    if (procName == "negative?")
    {
        exceptionArgNum(1, 1, "negative?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name())
                throw Exception::errorArgType(arg[i].typeName(), "negative?", i);

        eval = var(arg[0] < var(mpq_class(0)));
        return 1;
    }

    if (procName == "odd?")
    {
        exceptionArgNum(1, 1, "odd?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name())
                throw Exception::errorArgType(arg[i].typeName(), "odd?", i);
            else if (arg[i].operator mpq_class().get_den() != 1)
                throw Exception::errorArgType("rational number", "odd?", i);

        eval = var(! mpz_divisible_2exp_p(arg[0].operator mpq_class().get_num().get_mpz_t(), 1));
        return 1;
    }

    if (procName == "even?")
    {
        exceptionArgNum(1, 1, "even?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name())
                throw Exception::errorArgType(arg[i].typeName(), "even?", i);
            else if (arg[i].operator mpq_class().get_den() != 1)
                throw Exception::errorArgType("rational number", "even?", i);

        eval = var(bool(mpz_divisible_2exp_p(arg[0].operator mpq_class().get_num().get_mpz_t(), 1)));
        return 1;
    }

    if (procName == "max")
    {
        exceptionArgNumLower(1, "max");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name())
                throw Exception::errorArgType(arg[i].typeName(), "max", i);

        eval = arg[0];
        for (int i = 1; i < arg.size(); ++i)
            if (arg[i] > eval)
                eval = arg[i];

        return 1;
    }

    if (procName == "min")
    {
        exceptionArgNumLower(1, "min");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name())
                throw Exception::errorArgType(arg[i].typeName(), "min", i);

        eval = arg[0];
        for (int i = 1; i < arg.size(); ++i)
            if (arg[i] < eval)
                eval = arg[i];

        return 1;
    }

    if (procName == "abs")
    {
        exceptionArgNumLower(1, "abs");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name() && *(arg[i].tid) != typeid(mpf_class).name())
                throw Exception::errorArgType(arg[i].typeName(), "abs", i);

        eval = abs(arg[0]);
        return 1;
    }

    if (procName == "gcd")
    {
        exceptionArgNumLower(0, "gcd");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name())
                throw Exception::errorArgType(arg[i].typeName(), "gcd", i);
            else if (arg[i].operator mpq_class().get_den() != 1)
                throw Exception::errorArgType("rational number", "gcd", i);

        if (arg.size() == 0)
            eval = var(mpq_class(0));
        else
        {
            mpz_t t;
            mpz_init_set(t, arg[0].operator mpq_class().get_num().get_mpz_t());
            for (int i = 1; i < arg.size(); ++i)
                mpz_gcd(t, t, arg[i].operator mpq_class().get_num().get_mpz_t());
            eval = var(mpq_class(mpz_class(t)));
            mpz_clear(t);
        }
        return 1;
    }

    if (procName == "lcm")
    {
        exceptionArgNumLower(0, "lcm");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(mpq_class).name())
                throw Exception::errorArgType(arg[i].typeName(), "lcm", i);
            else if (arg[i].operator mpq_class().get_den() != 1)
                throw Exception::errorArgType("rational number", "lcm", i);

        if (arg.size() == 0)
            eval = var(mpq_class(1));
        else
        {
            mpz_t t;
            mpz_init_set(t, arg[0].operator mpq_class().get_num().get_mpz_t());
            for (int i = 1; i < arg.size(); ++i)
                mpz_lcm(t, t, arg[i].operator mpq_class().get_num().get_mpz_t());
            eval = var(mpq_class(mpz_class(t)));
            mpz_clear(t);
        }
        return 1;
    }

    if (procName == "not")
    {
        exceptionArgNum(1, 1, "lcm");

        eval = var(! arg[0].operator bool());
        return 1;
    }

    if (procName == "boolean?")
    {
        exceptionArgNum(1, 1, "lcm");

        eval = var(*(arg[0].tid) == typeid(bool).name());
        return 1;
    }

    if (procName == "null?")
    {
        exceptionArgNum(1, 1, "null?");

        eval = var(*(arg[0].tid) == typeid(EmptyList).name());
        return 1;
    }

    if (procName == "list?")
    {
        exceptionArgNum(1, 1, "list?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(Pair).name() && *(arg[i].tid) != typeid(EmptyList).name())
                throw Exception::errorArgType(arg[i].typeName(), "list?", i);

        var* ptr = &arg[0];
        for (; *(ptr -> tid) == typeid(Pair).name(); ptr = &(((Pair *)(ptr -> val)) -> second));
        eval = var(*(ptr -> tid) == typeid(EmptyList).name());
        return 1;
    }

    if (procName == "list")
    {
        exceptionArgNumLower(0, "list");

        eval = var(EmptyList());
        for (int i = arg.size()-1; i >= 0; --i)
            eval = var(Pair(arg[i], eval));
        return 1;
    }

    if (procName == "length")
    {
        exceptionArgNum(1, 1, "length");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(Pair).name())
                throw Exception::errorArgType(arg[i].typeName(), "length", i);

        var* ptr = &arg[0];
        int cnt = 0;
        for (; *(ptr -> tid) == typeid(Pair).name(); ptr = &(((Pair *)(ptr -> val)) -> second))
            ++cnt;
        if (*(ptr -> tid) == typeid(EmptyList).name())
            eval = var(mpq_class(cnt));
        else
            throw Exception::errorArgType("improper list", "length", 1);
        return 1;
    }

    if (procName == "append")
    {
        exceptionArgNumLower(1, "append");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(Pair).name() && *(arg[i].tid) != typeid(EmptyList).name())
                throw Exception::errorArgType(arg[i].typeName(), "append", i);

        eval = arg[0];
        for (int i = 1; i < arg.size(); ++i)
            eval.appendList(arg[i]);
        return 1;
    }

    if (procName == "reverse")
    {
        exceptionArgNum(1, 1, "reverse");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(Pair).name())
                throw Exception::errorArgType(arg[i].typeName(), "reverse", i);

        vector<var> elemList;
        var* ptr = &arg[0];
        int cnt = 0;
        elemList.clear();
        for (; *(ptr -> tid) == typeid(Pair).name(); ptr = &(((Pair *)(ptr -> val)) -> second))
            elemList.push_back(((Pair *)(ptr -> val)) -> first);
        if (*(ptr -> tid) == typeid(EmptyList).name())
        {
            eval = var(EmptyList());
            for (auto iter = elemList.begin(); iter != elemList.end(); ++iter)
                eval = var(Pair(*iter, eval));
        }
        else
            throw Exception::errorArgType("improper list", "reverse", 1);
        return 1;
    }

    if (procName == "list-tail")
    {
        exceptionArgNum(2, 2, "list-tail");

        if (*(arg[0].tid) != typeid(Pair).name() && *(arg[0].tid) != typeid(EmptyList).name())
            throw Exception::errorArgType(arg[0].typeName(), "list-tail", 0);

        if (*(arg[1].tid) != typeid(mpq_class).name())
            throw Exception::errorArgType(arg[1].typeName(), "list-tail", 1);
        else if (arg[1].operator mpq_class().get_den() != 1)
            throw Exception::errorArgType("rational number", "list-tail", 1);
        else if (arg[1].operator mpq_class().get_num() < 0)
            throw Exception::errorArgType("negative integer", "list-tail", 1);

        var* ptr = &arg[0];
        int cnt = arg[1].operator mpq_class().get_num().get_si();
        for (; *(ptr -> tid) == typeid(Pair).name() && cnt; ptr = &(((Pair *)(ptr -> val)) -> second))
            --cnt;
        if (cnt)
            throw Exception::errorArgValue(arg[1].getStr(), "list-tail", 1);

        eval = *ptr;
        return 1;
    }

    if (procName == "list-ref")
    {
        exceptionArgNum(2, 2, "list-ref");

        if (*(arg[0].tid) != typeid(Pair).name() && *(arg[0].tid) != typeid(EmptyList).name())
            throw Exception::errorArgType(arg[0].typeName(), "list-ref", 0);

        if (*(arg[1].tid) != typeid(mpq_class).name())
            throw Exception::errorArgType(arg[1].typeName(), "list-ref", 1);
        else if (arg[1].operator mpq_class().get_den() != 1)
            throw Exception::errorArgType("rational number", "list-ref", 1);
        else if (arg[1].operator mpq_class().get_num() < 0)
            throw Exception::errorArgType("negative integer", "list-ref", 1);

        var* ptr = &arg[0];
        int cnt = arg[1].operator mpq_class().get_num().get_si();
        for (; *(ptr -> tid) == typeid(Pair).name() && cnt; ptr = &(((Pair *)(ptr -> val)) -> second))
            --cnt;
        if (cnt)
            throw Exception::errorArgValue(arg[1].getStr(), "list-ref", 1);

        eval = ptr -> operator Pair().second;
        return 1;
    }

    if (procName == "memq")
    {
        exceptionArgNum(2, 2, "memq");

        if (*(arg[1].tid) != typeid(Pair).name() && *(arg[1].tid) != typeid(EmptyList).name())
            throw Exception::errorArgType(arg[1].typeName(), "memq", 1);

        var* ptr = &arg[1];
        for (; *(ptr -> tid) == typeid(Pair).name() && ptr -> val != arg[0].val; ptr = &(((Pair *)(ptr -> val)) -> second));

        if (*(ptr -> tid) == typeid(EmptyList()).name())
            eval = var(false);
        else if (*(ptr -> tid) == typeid(Pair()).name())
            eval = *ptr;
        else
            throw Exception::errorArgType("improper list", "memq", 1);
        return 1;
    }

    if (procName == "memv")
    {
        exceptionArgNum(2, 2, "memv");

        if (*(arg[1].tid) != typeid(Pair).name() && *(arg[1].tid) != typeid(EmptyList).name())
            throw Exception::errorArgType(arg[1].typeName(), "memv", 1);

        var* ptr = &arg[1];
        for (; *(ptr -> tid) == typeid(Pair).name(); ptr = &(((Pair *)(ptr -> val)) -> second))
            if (*(arg[0].tid) == *(ptr -> tid) && (*(arg[0].tid) == typeid(Pair).name() || *(arg[0].tid) == typeid(string).name() || *(arg[0].tid) == typeid(Vector).name()))
            {
                if (arg[0].val == ptr -> val)
                    break;
            }
            else
                if (arg[0] == *ptr)
                    break;

        if (*(ptr -> tid) == typeid(EmptyList()).name())
            eval = var(false);
        else if (*(ptr -> tid) == typeid(Pair()).name())
            eval = *ptr;
        else
            throw Exception::errorArgType("improper list", "memv", 1);
        return 1;
    }

    if (procName == "member")
    {
        exceptionArgNum(2, 2, "member");

        if (*(arg[1].tid) != typeid(Pair).name() && *(arg[1].tid) != typeid(EmptyList).name())
            throw Exception::errorArgType(arg[1].typeName(), "member", 1);

        var* ptr = &arg[1];
        for (; *(ptr -> tid) == typeid(Pair).name() && *ptr != arg[0]; ptr = &(((Pair *)(ptr -> val)) -> second));

        if (*(ptr -> tid) == typeid(EmptyList()).name())
            eval = var(false);
        else if (*(ptr -> tid) == typeid(Pair()).name())
            eval = *ptr;
        else
            throw Exception::errorArgType("improper list", "member", 1);
        return 1;
    }

    if (procName == "char-ci=?")
    {
        exceptionArgNum(2, 2, "char-ci=?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(char).name())
                throw Exception::errorArgType(arg[i].typeName(), "char-ci=?", i);

        eval = var(tolower(arg[0].operator char()) == tolower(arg[1].operator char()));
        return 1;
    }

    if (procName == "char-ci<?")
    {
        exceptionArgNum(2, 2, "char-ci<?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(char).name())
                throw Exception::errorArgType(arg[i].typeName(), "char-ci<?", i);

        eval = var(tolower(arg[0].operator char()) < tolower(arg[1].operator char()));
        return 1;
    }

    if (procName == "char-ci>?")
    {
        exceptionArgNum(2, 2, "char-ci>?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(char).name())
                throw Exception::errorArgType(arg[i].typeName(), "char-ci>?", i);

        eval = var(tolower(arg[0].operator char()) > tolower(arg[1].operator char()));
        return 1;
    }

    if (procName == "char-ci<=?")
    {
        exceptionArgNum(2, 2, "char-ci<=?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(char).name())
                throw Exception::errorArgType(arg[i].typeName(), "char-ci<=?", i);

        eval = var(tolower(arg[0].operator char()) <= tolower(arg[1].operator char()));
        return 1;
    }

    if (procName == "char-ci>=?")
    {
        exceptionArgNum(2, 2, "char-ci>=?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(char).name())
                throw Exception::errorArgType(arg[i].typeName(), "char-ci>=?", i);

        eval = var(tolower(arg[0].operator char()) >= tolower(arg[1].operator char()));
        return 1;
    }

    if (procName == "char-alphabetic?")
    {
        exceptionArgNum(1, 1, "char-alphabetic?");

        if (*(arg[0].tid) != typeid(char).name())
            throw Exception::errorArgType(arg[0].typeName(), "char-alphabetic?", 0);

        eval = var((bool)isalpha(arg[0].operator char()));
        return 1;
    }

    if (procName == "char-numeric?")
    {
        exceptionArgNum(1, 1, "char-numeric?");

        if (*(arg[0].tid) != typeid(char).name())
            throw Exception::errorArgType(arg[0].typeName(), "char-numeric??", 0);

        eval = var((bool)isdigit(arg[0].operator char()));
        return 1;
    }

    if (procName == "char-whitespace?")
    {
        exceptionArgNum(1, 1, "char-whitespace?");

        if (*(arg[0].tid) != typeid(char).name())
            throw Exception::errorArgType(arg[0].typeName(), "char-whitespace?", 0);

        eval = var((bool)isspace(arg[0].operator char()));
        return 1;
    }

    if (procName == "char-upper-case?")
    {
        exceptionArgNum(1, 1, "char-upper-case?");

        if (*(arg[0].tid) != typeid(char).name())
            throw Exception::errorArgType(arg[0].typeName(), "char-upper-case?", 0);

        eval = var((bool)isupper(arg[0].operator char()));
        return 1;
    }

    if (procName == "char-lower-case?")
    {
        exceptionArgNum(1, 1, "char-lower-case?");

        if (*(arg[0].tid) != typeid(char).name())
            throw Exception::errorArgType(arg[0].typeName(), "char-lower-case?", 0);

        eval = var((bool)islower(arg[0].operator char()));
        return 1;
    }

    if (procName == "char-upcase")
    {
        exceptionArgNum(1, 1, "char-upcase");

        if (*(arg[0].tid) != typeid(char).name())
            throw Exception::errorArgType(arg[0].typeName(), "char-upcase", 0);

        eval = var((char)toupper(arg[0].operator char()));
        return 1;
    }

    if (procName == "char-downcase")
    {
        exceptionArgNum(1, 1, "char-downcase");

        if (*(arg[0].tid) != typeid(char).name())
            throw Exception::errorArgType(arg[0].typeName(), "char-downcase", 0);

        eval = var((char)tolower(arg[0].operator char()));
        return 1;
    }

    if (procName == "string")
    {
        exceptionArgNumLower(0, "string");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(char).name())
                throw Exception::errorArgType(arg[i].typeName(), "string", i);

        string res = "";
        for (auto iter = arg.begin(); iter != arg.end(); res += (iter++) -> operator char());
        eval = var(res);
        return 1;
    }

    if (procName == "string=?")
    {
        exceptionArgNum(2, 2, "string=?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(string).name())
                throw Exception::errorArgType(arg[i].typeName(), "string=?", i);

        eval = var(arg[0].operator string() == arg[1].operator string());
        return 1;
    }

    if (procName == "string-ci=?")
    {
        exceptionArgNum(2, 2, "string-ci=?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(string).name())
                throw Exception::errorArgType(arg[i].typeName(), "string-ci=?", i);

        eval = var(Interpreter::lowercase(arg[0].operator string()) == Interpreter::lowercase(arg[1].operator string()));
        return 1;
    }

    if (procName == "string<?")
    {
        exceptionArgNum(2, 2, "string<?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(string).name())
                throw Exception::errorArgType(arg[i].typeName(), "string<?", i);

        eval = var(arg[0].operator string() < arg[1].operator string());
        return 1;
    }

    if (procName == "string>?")
    {
        exceptionArgNum(2, 2, "string>?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(string).name())
                throw Exception::errorArgType(arg[i].typeName(), "string>?", i);

        eval = var(arg[0].operator string() > arg[1].operator string());
        return 1;
    }

    if (procName == "string<=?")
    {
        exceptionArgNum(2, 2, "string<=?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(string).name())
                throw Exception::errorArgType(arg[i].typeName(), "string<=?", i);

        eval = var(arg[0].operator string() <= arg[1].operator string());
        return 1;
    }

    if (procName == "string>=?")
    {
        exceptionArgNum(2, 2, "string>=?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(string).name())
                throw Exception::errorArgType(arg[i].typeName(), "string>=?", i);

        eval = var(arg[0].operator string() >= arg[1].operator string());
        return 1;
    }

    if (procName == "string-ci<?")
    {
        exceptionArgNum(2, 2, "string-ci<?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(string).name())
                throw Exception::errorArgType(arg[i].typeName(), "string-ci<?", i);

        eval = var(Interpreter::lowercase(arg[0].operator string()) < Interpreter::lowercase(arg[1].operator string()));
        return 1;
    }

    if (procName == "string-ci>?")
    {
        exceptionArgNum(2, 2, "string-ci>?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(string).name())
                throw Exception::errorArgType(arg[i].typeName(), "string-ci>?", i);

        eval = var(Interpreter::lowercase(arg[0].operator string()) > Interpreter::lowercase(arg[1].operator string()));
        return 1;
    }

    if (procName == "string-ci<=?")
    {
        exceptionArgNum(2, 2, "string-ci<=?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(string).name())
                throw Exception::errorArgType(arg[i].typeName(), "string-ci<=?", i);

        eval = var(Interpreter::lowercase(arg[0].operator string()) <= Interpreter::lowercase(arg[1].operator string()));
        return 1;
    }

    if (procName == "string-ci>=?")
    {
        exceptionArgNum(2, 2, "string-ci>=?");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(string).name())
                throw Exception::errorArgType(arg[i].typeName(), "string-ci>=?", i);

        eval = var(Interpreter::lowercase(arg[0].operator string()) >= Interpreter::lowercase(arg[1].operator string()));
        return 1;
    }

    if (procName == "substring")
    {
        exceptionArgNum(3, 3, "substring");

        if (*(arg[0].tid) != typeid(string).name())
            throw Exception::errorArgType(arg[0].typeName(), "substring", 0);

        if (*(arg[1].tid) != typeid(mpq_class).name())
            throw Exception::errorArgType(arg[1].typeName(), "substring", 1);
        else if (arg[1].operator mpq_class().get_den() != 1)
            throw Exception::errorArgType("rational number", "substring", 1);
        else if (arg[1].operator mpq_class().get_num() < 0)
            throw Exception::errorArgType("negative number", "substring", 1);

        if (*(arg[2].tid) != typeid(mpq_class).name())
            throw Exception::errorArgType(arg[2].typeName(), "substring", 2);
        else if (arg[2].operator mpq_class().get_den() != 1)
            throw Exception::errorArgType("rational number", "substring", 2);
        else if (arg[2].operator mpq_class().get_num() < 0)
            throw Exception::errorArgType("negative number", "substring", 2);

        int L = arg[1].operator mpq_class().get_num().get_si();
        int R = arg[2].operator mpq_class().get_num().get_si();

        if (L > R)
        {
            ostringstream buffer;
            buffer << "substring : Index invaild. The start index cannot greater than the end index" << endl;
            buffer << "\tReceive: " << L << " " << R;
            throw Exception::errorInvaildCall(buffer.str());
        }

        if (R > arg[0].operator string().size())
            throw Exception::errorArgValue(arg[2].getStr(), "substring", 2);

        eval = var(arg[0].operator string().substr(L, R-L));
        return 1;
    }

    if (procName == "string-append")
    {
        exceptionArgNumLower(0, "string-append");

        for (int i = 0; i < arg.size(); ++i)
            if (*(arg[i].tid) != typeid(string).name())
                throw Exception::errorArgType(arg[i].typeName(), "string-append", i);

        string res = "";
        for (auto iter = arg.begin(); iter != arg.end(); res += (iter++) -> operator string());
        eval = var(res);
        return 1;
    }

    if (procName == "string->list")
    {
        exceptionArgNum(1, 1, "string->list");

        if (*(arg[0].tid) != typeid(string).name())
            throw Exception::errorArgType(arg[0].typeName(), "string->list", 0);

        eval = var(EmptyList());
        string s = arg[0].operator string();
        for (int i = 0; s[i]; eval.append(var(s[i++])));
        return 1;
    }

    if (procName == "list->string")
    {
        exceptionArgNum(1, 1, "list->string");

        if (*(arg[0].tid) != typeid(Pair).name())
            throw Exception::errorArgType(arg[0].typeName(), "list->string", 0);

        string res = "";
        var* ptr = &arg[0];
        for (; *(ptr -> tid) == typeid(Pair).name(); ptr = &(((Pair *)(ptr -> val)) -> second))
            if (*(((Pair *)(ptr -> val)) -> first.tid) == typeid(string).name())
                res += ((Pair *)(ptr -> val)) -> first.operator string();
            else
                throw Exception::errorArgType("list with non-string type", "list->string", 1);

        if (*(ptr -> tid) != typeid(EmptyList()).name())
            throw Exception::errorArgType("improper list", "list->string", 1);
        eval = var(res);
        return 1;
    }

    if (procName == "string-copy")
    {
        exceptionArgNum(1, 1, "string-copy");

        if (*(arg[0].tid) != typeid(string).name())
            throw Exception::errorArgType(arg[0].typeName(), "string-copy", 0);

        eval = arg[0].copy();
        return 1;
    }

    if (procName == "string-fill!")
    {
        exceptionArgNum(2, 2, "string-fill!");

        if (*(arg[0].tid) != typeid(string).name())
            throw Exception::errorArgType(arg[0].typeName(), "string-fill!", 0);

        if (*(arg[1].tid) != typeid(char).name())
            throw Exception::errorArgType(arg[1].typeName(), "string-fill!", 1);

        for (int i = 0; (*(string *)(arg[0].val))[i]; (*(string *)(arg[0].val))[i] = arg[1].operator char());
        eval = var();
        return 1;
    }

    if (procName == "vector")
    {
        exceptionArgNumLower(0, "vector");

        eval = var(Vector());
        for (auto iter = arg.begin(); iter != arg.end(); eval.vecPushBack(*(iter++)));
        return 1;
    }

    if (procName == "vector-fill!")
    {
        exceptionArgNum(2, 2, "vector-fill!");

        if (*(arg[0].tid) != typeid(Vector).name())
            throw Exception::errorArgType(arg[0].typeName(), "vector-fill!", 0);

        for (auto iter = ((Vector *)(arg[0].val)) -> begin(); iter != ((Vector *)(arg[0].val)) -> end(); ++iter)
            *iter = arg[1];
        eval = var();
        return 1;
    }

    if (procName == "display")
    {
        exceptionArgNum(1, 1, "display");

        cout << arg[0].getStr(DISPLAY_REPRESENT_STYLE) << flush;
        eval = var();
        return 1;
    }

    if (procName == "newline")
    {
        exceptionArgNum(0, 0, "newline");

        cout << endl;
        eval = var();
        return 1;
    }

    if (procName == "load")
    {
        exceptionArgNum(1, 1, "load");

        if (*(arg[0].tid) != typeid(string).name())
            throw Exception::errorArgType(arg[0].typeName(), "load", 0);

        string fileName = arg[0].operator string();

        ifstream fin(fileName.c_str());
        string codeBuffer = "";
        for (string buffer; getline(fin, buffer); codeBuffer += buffer+"\n");
        auto hIpt = new Interpreter;
        istringstream codeStream(codeBuffer);
        hIpt -> entry(codeStream, env);
        delete hIpt;
        fin.close();
    }

    return 0;
}

