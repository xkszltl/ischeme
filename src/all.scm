(define nil '())
(define (force func) (func))
; this is comment
 
( define ( caar pair ) ( car ( car pair ) ) )
( define ( cadr pair ) ( car ( cdr pair ) ) )
( define ( cdar pair ) ( cdr ( car pair ) ) )
( define ( cddr pair ) ( cdr ( cdr pair ) ) )
( define ( caaar pair ) ( car ( car ( car pair ) ) ) )
( define ( caadr pair ) ( car ( car ( cdr pair ) ) ) )
( define ( cadar pair ) ( car ( cdr ( car pair ) ) ) )
( define ( caddr pair ) ( car ( cdr ( cdr pair ) ) ) )
( define ( cdaar pair ) ( cdr ( car ( car pair ) ) ) )
( define ( cdadr pair ) ( cdr ( car ( cdr pair ) ) ) )
( define ( cddar pair ) ( cdr ( cdr ( car pair ) ) ) )
( define ( cdddr pair ) ( cdr ( cdr ( cdr pair ) ) ) )
( define ( caaaar pair ) ( car ( car ( car ( car pair ) ) ) ) )
( define ( caaadr pair ) ( car ( car ( car ( cdr pair ) ) ) ) )
( define ( caadar pair ) ( car ( car ( cdr ( car pair ) ) ) ) )
( define ( caaddr pair ) ( car ( car ( cdr ( cdr pair ) ) ) ) )
( define ( cadaar pair ) ( car ( cdr ( car ( car pair ) ) ) ) )
( define ( cadadr pair ) ( car ( cdr ( car ( cdr pair ) ) ) ) )
( define ( caddar pair ) ( car ( cdr ( cdr ( car pair ) ) ) ) )
( define ( cadddr pair ) ( car ( cdr ( cdr ( cdr pair ) ) ) ) )
( define ( cdaaar pair ) ( cdr ( car ( car ( car pair ) ) ) ) )
( define ( cdaadr pair ) ( cdr ( car ( car ( cdr pair ) ) ) ) )
( define ( cdadar pair ) ( cdr ( car ( cdr ( car pair ) ) ) ) )
( define ( cdaddr pair ) ( cdr ( car ( cdr ( cdr pair ) ) ) ) )
( define ( cddaar pair ) ( cdr ( cdr ( car ( car pair ) ) ) ) )
( define ( cddadr pair ) ( cdr ( cdr ( car ( cdr pair ) ) ) ) )
( define ( cdddar pair ) ( cdr ( cdr ( cdr ( car pair ) ) ) ) )
( define ( cddddr pair ) ( cdr ( cdr ( cdr ( cdr pair ) ) ) ) )
 
( define ( id obj ) obj )
( define ( flip func ) ( lambda ( arg1 arg2 ) ( func arg2 arg1 ) ) )
( define ( curry func arg1 ) ( lambda ( arg ) ( func arg1 arg ) ) )
 
 
( define ( compose f g ) ( lambda ( arg ) ( f ( g arg ) ) ) )
 
( define ( foldl func accum lst )
( if ( null? lst )
accum
( foldl func ( func accum ( car lst ) ) ( cdr lst ) ) ) )
 
( define ( foldr func accum lst )
( if ( null? lst )
accum
( func ( car lst ) ( foldr func accum ( cdr lst ) ) ) ) )
 
( define ( unfold func init pred)
( if (pred init )
( cons init '( ) )
( cons init ( unfold func ( func init ) pred) ) ) )
( define fold foldl )
( define reduce fold )
( define (mem-helper pred op ) ( lambda ( acc next ) ( if (and (not acc ) (pred
( op next ) ) ) next acc ) ) )
( define ( assq obj alist ) ( fold (mem-helper ( curry eq? obj ) car ) #f
alist ) )
( define ( assv obj alist ) ( fold (mem-helper ( curry eqv? obj ) car ) #f
alist ) )
( define ( assoc obj alist ) ( fold (mem-helper ( curry equal? obj ) car ) #f alist ) )
( define ( map func lst ) ( foldr ( lambda ( x y ) ( cons ( func x ) y ) ) '() lst ) )
( define ( filter pred lst ) ( foldr ( lambda ( x y ) ( if (pred x ) ( cons x y ) y ) ) '( ) lst ) )
 
( define (sum . lst ) ( fold + 0 lst ) )
( define (product . lst ) ( fold * 1 lst ) )
( define (binaryOr x y) (or x y))
( define (eagerOr . lst ) ( fold binaryOr #f lst ) )
( define (any? pred . lst ) ( apply eagerOr (map pred lst ) ) )
;-- test maker guideline: use dirichlet to compose 2 mixture sets of pure numeric or pure boolean functions, next start random walking in this complete bipartite graph.
;-- numeric primitives: test for gap or strict equality to make it boolean
;-- fact
;-- gcd
;-- expmod
;-- square
;-- mean
;-- +
;-- -
;-- *
;-- /
;--
;-- boolean primitives: conditional branch to make it numeric.
;-- divides?
;-- prime?
;-- boolean?
;-- not
;-- any?
;--
;-- misc primitives: manual test
;-- fib, the stupidest tree recursion version
;-- myE
;-- square-root
;-- hanoi
;
;
;-------------- hello world stuff
;

(define (zipWith f x y) (if (or (null? x) (null? y))
                          (quote ())
                          (cons (f (car x) (car y))
                                (zipWith f (cdr x) (cdr y)))))
(define (fib n)
  (if (= n 0) 0
    (if (= n 1) 1
      (+ (fib (- n 1)) 
         (fib (- n 2))))))

;(fib 22) ;should be 17711
;(fib 30) ;should be 832040
;(fib 32) ;should be 2178309, this may need careful memory management.
;(= (- (fib 32) (fib 31)) (fib 30)) ;should be true

(define (square x) (* x x))

(define (fact n) (if (= n 1)
                   1
                   (* n (fact (- n 1)))))

(define (hanoi source dest helper disks)
  (if (> disks 0) (append (hanoi source helper dest (- disks 1))
                                           (list (list source dest))
                                                           (hanoi helper dest source (- disks 1)))
    '()))

;(hanoi 'a 'b 'c 3');should return ((a b) (a c) (b c) (a b) (c a) (c b) (a b))


;-------------- math related stuff
(define (mean . numbers)
    (/ (apply + numbers)
            (length numbers)))

(define (divides? a b) (= (modulo b a) 0))

(define (expmod base e m)
  (if (= e 0) 1
    (if (even? e)
      (modulo (square (expmod base (/ e 2) m))
              m)
      (modulo (* base (expmod base (- e 1) m))
              m))))

(define (prime? n) 
  (define (smallest-divisor n)
    (find-divisor n 2))
  (define (find-divisor m test-divisor)
    (if (> (square test-divisor) n) n
      (if (divides? test-divisor n) test-divisor
        (find-divisor n (+ 1 test-divisor)))))
  (= n (smallest-divisor n)))

(define (fastexp base e)
  (if (= e 0) 1
    (if (even? e)
        (square (fastexp base (/ e 2)))
        (* base (fastexp base (- e 1))))))

(define (computeE e k n m)
  (if (>= n m) e
    (computeE (+ e (/ m n)) (+ k 1) (* n k) m)))

(define myE (computeE (fastexp 10 100) 2 1 (fastexp 10 100))) ;this is a scaled version of E.

(prime? 11);should be true
(prime? 7) ;should be true

(define (newton guess function derivative epsilon)
      (define guess2 (- guess (/ (function guess) (derivative guess))))
          (if (< (abs (- guess guess2)) epsilon) guess2
                    (newton guess2 function derivative epsilon)))

(define (square-root a)
      (newton 1 (lambda (x) (- (* x x) a)) (lambda (x) (* 2 x)) 1e-8))

; TODO: more complicated lazy lists in Scheme? reference http://www.haskell.org/haskellwiki/Blow_your_mind
; 
;(define fibs (cons 0 (cons 1 (delay (zipWith + fibs (cdr fibs))))))

;purely numeric part
(define x1 (gcd (fastexp (square (expmod (* 2 (mean 8765 4321 9999 1)) 100 131)) 7) 3628800)) ;11543/2
(define x2 (mean 8765 4321 9999 (fastexp (gcd (square (expmod 1 100 131)) 3628800) 7))) ;1792
(define x3 (gcd (square (expmod (fastexp (fact (mean 1 11 3 13)) 7) 100 131)) 3628800)) ;81
(define x4 (gcd (* 2 x1) 7777))
(define x5 (square (- x3 82)))
(define x6 (gcd (square (expmod (fastexp (fact (mean 2 2 4 4)) 7) 100 131)) 3628800)) ;1
(define x0 (- x6 x5))
(define x7 (fastexp (gcd (expmod (fact (square (mean 2 4 9 1))) 100 131) 3628800) 7))

;purely boolean part TBA
(define t1 (= x7 x6))
(define t0 (not (= x6 x5)))

;TBA mixture of them
;
(display (+ x1 x2 x3 x4 x5 x6 x7))
(display t0)
(display t1)
(display (hanoi 'a 'b 'c 3))
;15309/2#f#t


; TBA: test.scm

