#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <ctime>
#include <cassert>
#include <cctype>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <string>
#include <vector>
#include <bitset>
#include <set>
#include <map>
#include <queue>
#include <list>

#include "Constant.h"
#include "Exception.h"
#include "Interpreter.h"
#include "Interactive.h"
#include "Environment.h"
#include "BuildIn.h"

using namespace std;

///@brief Usage information.
void usageInfo()
{
}

///@brief Load the library.

/// The given library will be loaded into the specific environment.
/// It is suggested that the standard library should be loaded into the build-in environment.
void loadLib(const string &fileName, Environment &env)
{
    ifstream fin(fileName.c_str());
    string codeBuffer = "";
    for (string buffer; getline(fin, buffer); codeBuffer += buffer+'\n');
    auto hIpt = new Interpreter;
    istringstream codeStream(codeBuffer);
    hIpt -> entry(codeStream, env);
    delete hIpt;
    fin.close();
}

///@brief Main function.
int main(int argc, char *argv[])
{
    vector<string> fileName;
    string libFileName = DEFAULT_LIB;
    Environment env;
    BuildIn::setEnv();

    for (int i = 1; i < argc; ++i)
        if (argv[i][0] != '-')
            fileName.push_back(string(argv[i]));

    loadLib(libFileName, BuildIn::buildinEnv);
    if (fileName.size())
    {
        for (int i = 0; i < fileName.size(); ++i)
        {
            ifstream fin(fileName[i].c_str());
            string codeBuffer = "";
            for (string buffer; getline(fin, buffer); codeBuffer += buffer+"\n");
            auto hIpt = new Interpreter;
            istringstream codeStream(codeBuffer);
            hIpt -> entry(codeStream, env);
            delete hIpt;
            fin.close();
        }
    }
    else
    {
        auto hInteractive = new Interactive;
        return hInteractive -> entry(env);
    }
    return 0;
}

