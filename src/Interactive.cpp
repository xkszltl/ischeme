#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <ctime>
#include <cassert>
#include <cctype>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <string>
#include <vector>
#include <bitset>
#include <set>
#include <map>
#include <queue>
#include <list>

#include "Constant.h"
#include "scmType.h"
#include "Environment.h"
#include "Exception.h"
#include "Interpreter.h"
#include "Interactive.h"

using namespace std;

///@brief Default constructor.
Interactive::Interactive()
{
}

///@brief Destructor.
Interactive::~Interactive()
{
}

///@brief Entry of an interactive interface.

/// An environment should be given and it will be modified by the program running in the interactive interface.
int Interactive::entry(Environment &env)
{
    auto hIpt = new Interpreter;
    istringstream codeStream;

    cout << endl;
    cout << string(32, '=') << " Welcome " << string(32, '=') << endl;
    cout << endl;
    cout << "You can use \"quit\" or Ctrl+C to quit from iScheme Interactive Interface." << endl;
    cout << endl;
    while (1)
    {
        cout << "iScheme> " << flush;

        string codeBuffer;
        getline(cin, codeBuffer);

        if (codeBuffer == "quit")
            break;

        codeStream.clear();
        codeStream.str(codeBuffer);

        hIpt -> entry(codeStream, env);
    }
    return STATUS_NORMAL;
}

