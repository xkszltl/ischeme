#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <ctime>
#include <cassert>
#include <cctype>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <string>
#include <vector>
#include <bitset>
#include <set>
#include <map>
#include <queue>
#include <list>
#include <limits>

#include "Exception.h"

using namespace std;

string Exception::internalError = "This is a internal exception which may caused by some unknown bugs. Please contact the developer for help.";

///@brief Default constructor.

/// Constructor with empty message.
Exception::Exception():msg("") {}

///@brief Constructor.

/// Construct from a specific message.
Exception::Exception(const string &_msg):msg(_msg) {}

///@brief Copy constructor.
Exception::Exception(const Exception &b):msg(b.msg) {}

///@brief Predefined type of exception

/// Return an exception caused by using wrong type of arguments when calling a function.
Exception Exception::errorArgType(const string &typeName, const string &func, const int &argIndex)
{
    ostringstream buffer;
    buffer << "Error: Invaild function call with illegal type of argument." << endl;
    buffer << "\t" << func << " : Argument " << argIndex << endl;
    buffer << "\tReceive: " << typeName;
    return Exception(buffer.str());
}

///@brief Predefined type of exception

/// Return an exception caused by passing unacceptable value to a function.
Exception Exception::errorArgValue(const string &value, const string &func, const int &argIndex)
{
    ostringstream buffer;
    buffer << "Error: Invaild function call with illegal value of argument." << endl;
    buffer << "\t" << func << " : Argument " << argIndex << endl;
    buffer << "\tReceive: " << value;
    return Exception(buffer.str());
}

///@brief Predefined type of exception

/// Return an exception caused by passing too few arguments to a function.
Exception Exception::errorArgNumLower(const int &argNum, const string &func)
{
    ostringstream buffer;
    buffer << "Error: Invaild function call with too few arguments." << endl;
    buffer << "\t" << func << " : " << endl;
    buffer << "\tReceive: " << argNum << " arguments.";
    return Exception(buffer.str());
}

///@brief Predefined type of exception

/// Return an exception caused by passing too many arguments to a function.
Exception Exception::errorArgNumUpper(const int &argNum, const string &func)
{
    ostringstream buffer;
    buffer << "Error: Invaild function call with too many arguments." << endl;
    buffer << "\t" << func << " : " << endl;
    buffer << "\tReceive: " << argNum << " arguments.";
    return Exception(buffer.str());
}

///@brief Predefined type of exception

/// Return an exception caused by using an out-of-range index.
Exception Exception::errorIndex(const int &index)
{
    ostringstream buffer;
    buffer << "Error: Index " << index << " is out of range.";
    return Exception(buffer.str());
}

///@brief Predefined type of exception

/// Return an exception caused by using an unknown identifier.
Exception Exception::errorUnknownId(const string &id)
{
    return Exception("Error: Unknown identifier \""+id+"\".");
}

///@brief Predefined type of exception

/// Return an exception caused by an invaild function call.
/// Details of the exception should be given.
Exception Exception::errorInvaildCall(const string &msg)
{
    return Exception("Error: Invaild function call.\n\t"+msg);
}

///@brief Predefined type of exception

/// Return an exception caused by trying to eval a list without a procedure as its first argument.
Exception Exception::errorCallWithoutProc(const string &typeName, const string &val)
{
    return Exception("Error: Invaild format of function call. Require a procedure instand of a "+typeName+"\n\tReceive: "+val);
}

///@brief Predefined type of exception

/// Return an exception caused by using an invaild radix which is not defined in R5RS.
Exception Exception::errorBase(const int &base)
{
    ostringstream buffer;
    buffer << "Error: " << base << " is not a vaild radix." << endl;
    buffer << "\tScheme with R5RS can only accept radix 2, 8, 10 and 16.";
    return Exception(buffer.str());
}

///@brief Predefined type of exception

/// Retuan an exception caused by using zero as the divisor.
Exception Exception::errorDivideByZero(const string &funcName, const string &optList)
{
    return Exception("Error: Divide by zero in "+funcName+".\n\tReceive: ("+funcName+" "+optList+").");
}

///@brief Predefined type of exception

/// Return an exception caused by trying to use an non-exist conversion.
Exception Exception::errorConvert(const string &typeNameA, const string &typeNameB)
{
    return Exception("Error: Cannot convert \""+typeNameA+"\" to \""+typeNameB+"\".\n\t"+internalError);
}

///@brief Predefined type of exception

/// Return an exception caused by using an invaild operator or function for a object.
Exception Exception::errorOperator(const string &typeName, const string &funcName)
{
    return Exception("Error: Cannot use "+funcName+" for \""+typeName+"\".\n\t"+internalError);
}

///@brief Predefined type of exception

/// Return an exception caused by using an invaild operator or function for two objects.
Exception Exception::errorOperator(const string &typeNameA, const string &typeNameB, const string &funcName)
{
    return Exception("Error: Cannot use "+funcName+" for \""+typeNameA+"\" and \""+typeNameB+"\".\n\t"+internalError);
}

///@brief Predefined type of exception

/// Return an exception caused by trying to set a variable with unknown identifier in current environment as its name.
Exception Exception::errorSetUnknownId(const string &id, const string &comment)
{
    return Exception("Error: Unknown identifier \""+id+"\" "+comment+"\n\t"+internalError);
}

