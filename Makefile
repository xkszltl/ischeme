DIR = $(shell pwd)
INCS_DIR = $(DIR)/include
SRCS_DIR = $(DIR)/src
DEPS_DIR = $(DIR)/dep
OBJS_DIR = $(DIR)/obj
TARGETS_DIR = $(DIR)/bin

.PHONY: all
all: 
	@make -C $(SRCS_DIR)

.PHONY: clean
clean:
	@make --no-print-directory -C $(SRCS_DIR)/ clean
